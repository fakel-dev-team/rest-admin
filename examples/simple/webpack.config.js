const path = require('path');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');
const safePostCssParser = require('postcss-safe-parser');
const ESLintPlugin = require('eslint-webpack-plugin');

const resolve = (pathName) => path.resolve(__dirname, pathName);

const isEnvDevelopment = process.env.NODE_MODE === 'development';
const isEnvProduction = process.env.NODE_MODE === 'production';

const getStyleLoaders = (preProcessor) => {
  const loaders = [
    'style-loader',
    isEnvProduction && {
      loader: MiniCssExtractPlugin.loader,
    },
    {
      loader: 'css-loader',
    },
    {
      loader: 'postcss-loader',
      options: {
        postcssOptions: {
          plugins: [['postcss-preset-env']],
        },
      },
    },
  ].filter(Boolean);

  if (preProcessor) {
    loaders.push(
      {
        loader: 'resolve-url-loader',
        options: {
          root: resolve('./src'),
        },
      },
      {
        loader: preProcessor,
      },
    );
  }
  return loaders;
};

module.exports = {
  mode: isEnvProduction ? 'production' : isEnvDevelopment && 'development',
  entry: './src/index.tsx',
  output: {
    path: resolve('./build'),
    filename: 'static/js/[name].[contenthash:8].js',
    chunkFilename: 'static/js/[name].[contenthash:8].chunk.js',
    publicPath: '/',
  },

  module: {
    strictExportPresence: true,
    rules: [
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset/resource',
        parser: {
          dataUrlCondition: {
            maxSize: 244,
          },
        },
      },
      {
        test: /\.(js|jsx|ts|tsx)$/,
        loader: require.resolve('babel-loader'),
        options: {
          customize: require.resolve('babel-preset-react-app/webpack-overrides'),
          presets: [[require.resolve('babel-preset-react-app')]],
          plugins: [
            [
              require.resolve('babel-plugin-named-asset-import'),
              {
                loaderMap: {
                  svg: {
                    ReactComponent: '@svgr/webpack?-svgo,+titleProp,+ref![path]',
                  },
                },
              },
            ],
          ].filter(Boolean),
          cacheDirectory: true,
          cacheCompression: false,
          compact: isEnvProduction,
        },
      },
      {
        test: /\.css$/,
        use: getStyleLoaders(),
      },
      {
        test: /\.(scss|sass)$/,
        use: getStyleLoaders('sass-loader'),
      },
    ],
  },

  devServer: {
    open: true,
    contentBase: path.resolve(__dirname, 'build'),
    compress: true,
    port: process.env.PORT || 3000,
    publicPath: '/',
    historyApiFallback: true,
  },

  resolve: {
    extensions: ['.js', '.ts', '.tsx', '.json', '.jsx'],
  },
  optimization: {
    minimize: isEnvProduction,
    minimizer: [
      new TerserPlugin({
        terserOptions: {
          parse: {
            ecma: 8,
          },
          compress: {
            ecma: 5,
            warnings: false,
            comparisons: false,
            inline: 2,
          },
          mangle: {
            safari10: true,
          },
          output: {
            ecma: 5,
            comments: false,
            ascii_only: true,
          },
        },
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          parser: safePostCssParser,
          map: false
            ? {
                inline: false,
                annotation: true,
              }
            : false,
        },
        cssProcessorPluginOptions: {
          preset: ['default', { minifyFontValues: { removeQuotes: false } }],
        },
      }),
    ],
    splitChunks: {
      chunks: 'all',
      name: '',
    },
  },

  plugins: [
    new ESLintPlugin({
      extensions: ['.ts', '.tsx', '.jsx', '.js'],
    }),
    new HtmlWebpackPlugin({
      inject: true,
      template: path.resolve('./public/index.html'),
      fix: true,
      context: 'examples/simple/src',
    }),
    new MiniCssExtractPlugin({
      filename: 'static/css/[name].[contenthash:8].css',
      chunkFilename: 'static/css/[name].[contenthash:8].chunk.css',
    }),
  ],
};
