import React from 'react';

import Space from 'antd/lib/space';

import {
  Create,
  SimpleForm,
  TextInput,
  ReferenceManyInput,
  SelectInput,
  GalleryInput,
  ColorInput,
  CodeInput,
  TranslatableTextInput,
  TranslatableCodeInput,
  ArrayInput,
  InputsIterator,
} from '@fakel-dev-team/rest-admin-core';
import { DataProviderT } from '@fakel-dev-team/rest-admin-simple-rest';

export const PostsCreate: React.FC = (props) => {
  const getImages = (dataProvider: DataProviderT, pagination) => {
    const { currentPage, perPage } = pagination;

    const TOTAL_IMAGES = 22;
    const images = Array(TOTAL_IMAGES)
      .fill(0)
      .map((_, index) => ({
        id: +new Date() + index,
        src:
          'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
      }));

    return {
      data: images.slice((currentPage - 1) * perPage, currentPage * perPage),
      errors: null,
      total: TOTAL_IMAGES,
    };
  };

  const uploadHandler = (file: File, dataProvider?: DataProviderT) => ({
    id: +new Date(),
    src:
      'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
  });

  return (
    <Create redirect="list">
      {(createProps) => (
        <SimpleForm
          {...createProps}
          initialValue={{
            title: '',
            body: '',
            images: '',
            color: '',
            code: '',
            array: [{ text1: '', text2: '', text3: '' }],
          }}
        >
          <Space direction="vertical">
            {/* <TextInput label="Title" name="title" placeholder="Enter title" />
              <TextInput label="Body" name="body" placeholder="Enter body" />
              <CodeInput label="Code" name="code" />
              <TranslatableCodeInput label="Code" name="code" />
              <TranslatableTextInput
                label="Code"
                name="code"
                placeholder="Placeholder"
              />
              <ColorInput label="Color" name="color" /> */}

            {/* <ReferenceManyInput
                name="author"
                reference="users"
                label="Author"
              >
                <SelectInput
                  name="author"
                  valuePropName="_id"
                  titlePropName="username"
                />
              </ReferenceManyInput> */}

            <ArrayInput name="array">
              <InputsIterator
                style={{ width: '80vw' }}
                addButton={(arrayHelpers) => <div>Button</div>}
                title={<h1>Iterator title</h1>}
              >
                {({ name }) => (
                  <TextInput label="Text 1" name={`${name}.text1`} />
                )}
              </InputsIterator>
            </ArrayInput>

            {/* <GalleryInput
                buttonText="Загрузить изображения"
                label="Images"
                name="images"
                getImages={getImages}
                uploadHandler={uploadHandler}
              /> */}
          </Space>
        </SimpleForm>
      )}
    </Create>
  );
};
