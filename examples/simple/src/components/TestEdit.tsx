import React from 'react';

import { Button, Space } from 'antd';

import { useParams } from 'react-router-dom';

import { Edit, SimpleForm, TextInput, CodeInput } from '@fakel-dev-team/rest-admin-core';

export const PostEdit: React.FC = (props) => (
  <Edit {...props} redirect="list">
    {(editProps) => (
      <SimpleForm {...editProps}>
        <Space direction="horizontal" align="start">
          <Space direction="vertical">
            <TextInput label="Title" name="title" placeholder="Enter title" />
            {/* <TextInput label="Body" name="body" placeholder="Enter body" /> */}
            <CodeInput name="body" />
          </Space>
        </Space>
      </SimpleForm>
    )}
  </Edit>
);
