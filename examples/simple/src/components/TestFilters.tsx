import React from 'react';

import { Filters, TextInput } from '@fakel-dev-team/rest-admin-core';

export const TestFilters = () => (
  <Filters>
    <TextInput name="title" placeholder="Title" />
    <TextInput name="body" placeholder="Body" />
  </Filters>
);
