import React from 'react';

import Button from 'antd/lib/button';

import { useHistory } from 'react-router-dom';

import { List, TextField, ColumnT, DateField } from '@fakel-dev-team/rest-admin-core/';

export const UsersView: React.FC = (props) => {
  const history = useHistory();

  const columns: ColumnT[] = [
    {
      title: 'ID',
      source: '_id',
      Field: TextField,
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: 'Username',
      source: 'username',
      Field: TextField,
    },
    {
      title: 'Created At',
      source: 'createdAt',
      Field: DateField,
      sortDirections: ['descend', 'ascend'],
    },
  ];

  return (
    <List
      view="edit"
      {...props}
      columns={columns}
      actions={
        <>
          <Button
            onClick={() => history.push('/posts/create')}
            type="primary"
            style={{ marginBottom: 20 }}
          >
            Create
          </Button>
        </>
      }
    />
  );
};
