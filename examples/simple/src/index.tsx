import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

import { Admin, Resource } from '@fakel-dev-team/rest-admin-core';
import { createSimpleRest } from '@fakel-dev-team/rest-admin-simple-rest';
import { createAuthProvider } from '@fakel-dev-team/rest-admin-simple-auth';

import { PostsView } from './components/PostsView';
import { UsersView } from './components/UsersView';
import { PostEdit } from './components/TestEdit';

import './index.scss';

const dataProvider = createSimpleRest({ apiBaseUrl: 'http://localhost:3000' });
const authProvider = createAuthProvider({ baseURL: 'http://localhost:3000' });

const App = () => (
  <Admin dataProvider={dataProvider} authProvider={authProvider} logoTitle="Admin Custom Logo">
    <Resource
      name="posts"
      list={PostsView}
      edit={PostEdit}
      options={{
        label: 'Posts',
      }}
    />
    <Resource
      name="users"
      list={UsersView}
      options={{
        label: 'User',
      }}
    />
  </Admin>
);

render(
  <Router>
    <App />
  </Router>,
  document.getElementById('root'),
);
