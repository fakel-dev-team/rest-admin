import { useStore } from "./useStore";
export var useListStore = function () {
    return useStore("listStore");
};
