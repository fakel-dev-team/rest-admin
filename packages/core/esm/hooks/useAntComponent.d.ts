import { FormikProps, FieldInputProps, FieldMetaProps } from "formik";
declare type UseAntComponentT = {
    Component: any;
    ComponentChildren?: (value: any) => any;
    form: FormikProps<any>;
    field: FieldInputProps<any>;
    meta?: FieldMetaProps<any>;
    props?: any;
};
export declare const useAntComponent: (params: UseAntComponentT) => JSX.Element;
export {};
