import { useStore } from "./useStore";
export var useResourceStore = function () {
    var resourceStore = useStore("resourceStore");
    return resourceStore;
};
