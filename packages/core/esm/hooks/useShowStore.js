import { useStore } from "./useStore";
export var useShowStore = function () {
    return useStore("showStore");
};
