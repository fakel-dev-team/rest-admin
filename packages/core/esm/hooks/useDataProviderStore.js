import { useStore } from "./useStore";
export var useDataProviderStore = function () {
    var dataProviderStore = useStore("dataProviderStore");
    return dataProviderStore;
};
