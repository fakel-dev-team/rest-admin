import { notification } from "antd";
export var useNotification = function (params) {
    var message = params.message, description = params.description, _a = params.type, type = _a === void 0 ? "info" : _a;
    var openNotification = function () {
        notification[type]({
            message: message,
            description: description,
            duration: 0,
        });
    };
    return openNotification;
};
