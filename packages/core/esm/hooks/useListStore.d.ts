import { ListStore } from "../stores/ListStore";
export declare const useListStore: () => ListStore;
