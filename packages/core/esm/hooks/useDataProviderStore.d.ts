import { DataProviderStore } from "../stores/DataProviderStore";
export declare const useDataProviderStore: () => DataProviderStore;
