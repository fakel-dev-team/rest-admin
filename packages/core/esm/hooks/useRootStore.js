import { useContext } from "react";
import { AdminContext } from "../components/StoreProvider";
export var useRootStore = function () {
    var adminStore = useContext(AdminContext).adminStore;
    return adminStore;
};
