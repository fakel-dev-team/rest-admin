import { selectStore } from "../selectors/selectorStore";
import { useContext } from "react";
import { AdminContext } from "../components/StoreProvider";
export var useStore = function (name) {
    var adminStore = useContext(AdminContext).adminStore;
    var store = selectStore(adminStore, name);
    return store;
};
