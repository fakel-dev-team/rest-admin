import { useStore } from "./useStore";
export var useUrlStore = function () {
    return useStore("urlStore");
};
