import { useStore } from "./useStore";
export var useFiltersStore = function () {
    return useStore("filtersStore");
};
