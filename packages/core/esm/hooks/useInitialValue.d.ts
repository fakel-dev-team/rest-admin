export declare const useInitialValue: (id: string) => {
    initialValue: undefined;
    loading: boolean;
};
