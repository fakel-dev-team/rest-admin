import { FiltersStore } from "../stores/FiltersStore";
export declare const useFiltersStore: () => FiltersStore;
