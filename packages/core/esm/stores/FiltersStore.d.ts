export declare type Filter = {
    source: string;
    value: string;
    label: string;
    DisplayFilterComponent: any;
};
export declare class FiltersStore {
    filters: Filter[];
    displayFilters: Array<string>;
    constructor();
    pushDisplayFilter: (filter: any) => void;
    pushFilter(filter: Filter): void;
    getFilter(source: any): Filter;
    isFilterRegistred(source: string): boolean;
    setFilterValue(source: string, value: string): void;
}
