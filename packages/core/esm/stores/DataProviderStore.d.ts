import type { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
export declare class DataProviderStore {
    private provider;
    constructor(dataProvider: DataProviderT);
    get dataProvider(): DataProviderT;
}
