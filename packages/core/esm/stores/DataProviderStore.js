import { makeAutoObservable } from "mobx";
var DataProviderStore = /** @class */ (function () {
    function DataProviderStore(dataProvider) {
        makeAutoObservable(this);
        this.provider = dataProvider;
    }
    Object.defineProperty(DataProviderStore.prototype, "dataProvider", {
        get: function () {
            return this.provider;
        },
        enumerable: false,
        configurable: true
    });
    return DataProviderStore;
}());
export { DataProviderStore };
