var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { observable, action, makeObservable, } from "mobx";
var FiltersStore = /** @class */ (function () {
    function FiltersStore() {
        var _this = this;
        this.filters = [];
        this.displayFilters = [];
        this.pushDisplayFilter = function (filter) {
            _this.displayFilters.push(filter);
        };
        makeObservable(this, {
            filters: observable,
            displayFilters: observable,
        });
    }
    FiltersStore.prototype.pushFilter = function (filter) {
        if (!this.isFilterRegistred(filter.source)) {
            this.filters.push(filter);
        }
    };
    FiltersStore.prototype.getFilter = function (source) {
        return this.filters.find(function (filter) { return filter.source === source; });
    };
    FiltersStore.prototype.isFilterRegistred = function (source) {
        return !!this.getFilter(source);
    };
    FiltersStore.prototype.setFilterValue = function (source, value) {
        if (!this.isFilterRegistred(source)) {
            throw new Error("Filter not registred!");
        }
        var filter = this.getFilter(source);
        filter.value = value;
    };
    __decorate([
        action("push display filter"),
        __metadata("design:type", Object)
    ], FiltersStore.prototype, "pushDisplayFilter", void 0);
    __decorate([
        action("push filter"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], FiltersStore.prototype, "pushFilter", null);
    __decorate([
        action("get filter"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], FiltersStore.prototype, "getFilter", null);
    __decorate([
        action("set filter value"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String, String]),
        __metadata("design:returntype", void 0)
    ], FiltersStore.prototype, "setFilterValue", null);
    return FiltersStore;
}());
export { FiltersStore };
