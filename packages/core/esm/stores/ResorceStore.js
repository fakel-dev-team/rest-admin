var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { computed, observable, action, makeObservable, } from "mobx";
var ResourceStore = /** @class */ (function () {
    function ResourceStore() {
        this.resources = [];
        this.currentResource = "";
        makeObservable(this, {
            resources: observable,
            currentResource: observable,
            setCurrentResource: action,
            pushResource: action,
            getResource: action,
            getCurrentResource: action,
            isRegistred: action
        });
        this.resources = [];
    }
    ResourceStore.prototype.setCurrentResource = function (currentResource) {
        this.currentResource = currentResource;
    };
    ResourceStore.prototype.pushResource = function (resource) {
        if (!this.isRegistred(resource.name)) {
            this.resources.push(resource);
        }
    };
    ResourceStore.prototype.getResource = function (name) {
        return this.resources.find(function (resource) { return resource.name === name; });
    };
    ResourceStore.prototype.getCurrentResource = function () {
        var _this = this;
        if (this.resources) {
            return this.resources.find(function (resource) { return resource.name === _this.currentResource; }) || this.resources[0];
        }
    };
    ResourceStore.prototype.isRegistred = function (name) {
        return !!this.getResource(name);
    };
    __decorate([
        action("set current resourse"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], ResourceStore.prototype, "setCurrentResource", null);
    __decorate([
        action("push resource"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], ResourceStore.prototype, "pushResource", null);
    __decorate([
        computed({ name: "get resource" }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], ResourceStore.prototype, "getResource", null);
    __decorate([
        computed({ name: "get current resource" }),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], ResourceStore.prototype, "getCurrentResource", null);
    __decorate([
        action('get is registred'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], ResourceStore.prototype, "isRegistred", null);
    return ResourceStore;
}());
export { ResourceStore };
