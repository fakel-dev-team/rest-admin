var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { serialize, getURLParameters, deleteURLParameter } from "../utils/url";
import { makeAutoObservable, action } from "mobx";
var URLStore = /** @class */ (function () {
    function URLStore() {
        makeAutoObservable(this);
        this._url = "";
        this._params = null;
    }
    URLStore.prototype.setUrl = function (url) {
        this._url = url;
    };
    URLStore.prototype.serializeParams = function (params) {
        this._params = params;
        return serialize(params);
    };
    URLStore.prototype.deserializeParams = function (paramName) {
        if (!this._url) {
            throw new Error("URL is not defined!");
        }
        this._params = getURLParameters(this._url, paramName);
        return this._params;
    };
    URLStore.prototype.deleteUrlParams = function (params) {
        var _a = deleteURLParameter(this._url, params), path = _a.path, updatedParams = _a.params;
        var pathname = this._url.split("?filter=", 2)[0];
        this._params = updatedParams;
        this._url = pathname + path;
        return path;
    };
    Object.defineProperty(URLStore.prototype, "url", {
        get: function () {
            return this._url;
        },
        set: function (value) {
            this._url = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(URLStore.prototype, "params", {
        get: function () {
            return this._params;
        },
        set: function (value) {
            this._params = value;
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        action("set url"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "setUrl", null);
    __decorate([
        action("serialize params"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "serializeParams", null);
    __decorate([
        action("deserialize params"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "deserializeParams", null);
    __decorate([
        action("delete params from url"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "deleteUrlParams", null);
    return URLStore;
}());
export { URLStore };
