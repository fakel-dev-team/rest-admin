import { AuthStore } from "./AuthStore";
import { AuthProviderStore } from "./AuthProviderStore";
import { ImagesStore } from "./ImagesStore";
import { ShowStore } from "./ShowStore";
import { URLStore } from "./UrlStore";
import { ListStore } from "./ListStore";
import { ResourceStore } from "./ResorceStore";
import { FiltersStore } from "./FiltersStore";
import { DataProviderStore } from "./DataProviderStore";
import { makeAutoObservable } from "mobx";
var AdminStore = /** @class */ (function () {
    function AdminStore(dataProvider, authProvider) {
        makeAutoObservable(this);
        this._dataProviderStore = new DataProviderStore(dataProvider);
        this._authProviderStore = new AuthProviderStore(authProvider);
        this._resourceStore = new ResourceStore();
        this._filtersStore = new FiltersStore();
        this._listStore = new ListStore();
        this._urlStore = new URLStore();
        this._showStore = new ShowStore();
        this._imagesStore = new ImagesStore();
        this._authStore = new AuthStore();
    }
    Object.defineProperty(AdminStore.prototype, "showStore", {
        get: function () {
            return this._showStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "authProviderStore", {
        get: function () {
            return this._authProviderStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "authStore", {
        get: function () {
            return this._authStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "imagesStore", {
        get: function () {
            return this._imagesStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "urlStore", {
        get: function () {
            return this._urlStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "listStore", {
        get: function () {
            return this._listStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "filtersStore", {
        get: function () {
            return this._filtersStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "dataProviderStore", {
        get: function () {
            return this._dataProviderStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "resourceStore", {
        get: function () {
            return this._resourceStore;
        },
        enumerable: false,
        configurable: true
    });
    return AdminStore;
}());
export { AdminStore };
