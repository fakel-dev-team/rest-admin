export declare class URLStore {
    private _url;
    private _params;
    constructor();
    setUrl(url: string): void;
    serializeParams(params: any): string;
    deserializeParams(paramName: string): any;
    deleteUrlParams(params: string): string;
    get url(): string;
    set url(value: string);
    get params(): any;
    set params(value: any);
}
