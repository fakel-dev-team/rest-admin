import get from "lodash.get";
export var mapRecordsToOptions = function (records, source, titlePropName) {
    return records.map(function (record, index) {
        var value = get(record, source);
        var title = get(record, titlePropName);
        return {
            key: record.id || index,
            value: value,
            title: title,
        };
    });
};
