import React from "react";
export var getSorter = function (source) {
    return function (a, b) {
        var aValue = "" + a[source];
        var bValue = "" + b[source];
        return aValue.localeCompare(bValue);
    };
};
export var mapToAntColumn = function (columns) {
    return columns.map(function (Column, index) { return ({
        title: Column.title,
        dataIndex: Column.source,
        key: Column.id || index,
        sortDirections: Column.sortDirections ? Column.sortDirections : null,
        sorter: Column.sortDirections
            ? Column.sorter || getSorter(Column.source)
            : null,
        render: function (_, record, index) {
            return (React.createElement(Column.Field, { key: Column.source + "-" + index, record: record, source: Column.source, reference: Column.reference, link: Column.link }, Column.FieldChildren ? Column.FieldChildren() : null));
        },
    }); });
};
