import { OptionT } from "../@types";
export declare const mapRecordsToOptions: (records: any, source: string, titlePropName: string) => OptionT[];
