import { FILTER_PARAMS_NAME } from "../constants/url";
export var deleteURLParameter = function (url, parameter) {
    var params = getURLParameters(url, parameter);
    delete params[parameter];
    if (!Object.keys(params).length) {
        return {
            path: "",
            params: {},
        };
    }
    if (Object.keys(params).length >= 1) {
        return { path: "?" + FILTER_PARAMS_NAME + "=" + serialize(params), params: params };
    }
};
export var getURLParameters = function (url, paramName) {
    if (url) {
        var result_1 = {};
        var urlParams = decodeURI(url.split("?")[1]);
        if (urlParams) {
            var sURLVariables = urlParams.split("&");
            sURLVariables.forEach(function (el) {
                var sParameterName = el.split("=");
                result_1[sParameterName[0]] = sParameterName[1];
            });
        }
        return result_1;
    }
};
export var serialize = function (params) {
    var str = [];
    for (var param in params)
        if (params.hasOwnProperty(param)) {
            if (params[param]) {
                str.push(param + "=" + params[param]);
            }
        }
    return str.join("&");
};
