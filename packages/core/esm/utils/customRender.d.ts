export declare const customRender: (ui: any) => {
    asFragment: () => DocumentFragment;
    container: Element;
};
