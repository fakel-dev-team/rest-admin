import React from "react";
import { render } from "@testing-library/react";
import StoreProvider from "../components/StoreProvider";
import { mockAdminStore } from "../mock";
export var customRender = function (ui) {
    var _a = render(React.createElement(StoreProvider, { adminStore: mockAdminStore }, ui)), container = _a.container, asFragment = _a.asFragment;
    return { asFragment: asFragment, container: container };
};
