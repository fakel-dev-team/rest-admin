export declare type ClassPropsT<T> = {
    [K in keyof T]: T[K] extends Function ? never : K;
}[keyof T];
export declare const selectStore: <SourceStoreT>(store: SourceStoreT, name: ClassPropsT<SourceStoreT>) => SourceStoreT[ClassPropsT<SourceStoreT>];
