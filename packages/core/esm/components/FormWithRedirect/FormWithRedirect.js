import { useState } from "react";
import { useResourceStore } from "../../hooks/useResourceStore";
var FormWithRedirect = function (_a) {
    var to = _a.to, children = _a.children;
    var resourceStore = useResourceStore();
    var _b = useState("/" + resourceStore.currentResource + "/" + to || ""), redirect = _b[0], setRedirect = _b[1];
    var saveRedirect = function () {
        setRedirect("/" + resourceStore.currentResource + "/" + to);
    };
    return children({ redirect: redirect, saveRedirect: saveRedirect });
};
export default FormWithRedirect;
