import React from "react";
import Menu from "antd/lib/menu";
import { observer } from "mobx-react";
import { Link } from "react-router-dom";
import { useResourceStore } from "../../hooks/useResourceStore";
var isDefinedOptions = function (resource) { return !!resource.options; };
var getRenderView = function (resource) {
    if (isDefinedOptions(resource)) {
        return resource.options.renderViewOnMenuClicked || "list";
    }
    return "list";
};
var getLinkUrl = function (resource) {
    var DEFAULT_URL = "/" + resource.name + "/" + getRenderView(resource);
    if (isDefinedOptions(resource)) {
        return resource.options.linkUrl || DEFAULT_URL;
    }
    return DEFAULT_URL;
};
var getLabel = function (resource) {
    if (isDefinedOptions(resource)) {
        return resource.options.label || resource.name;
    }
    return resource.name;
};
var AppMenu = observer(function () {
    var resourceStore = useResourceStore();
    var resources = resourceStore.resources;
    return resources && resources.length ? (React.createElement(Menu, { mode: "inline", style: { height: "100%", borderRight: 0 } }, resources.map(function (resource, index) {
        return (React.createElement(Menu.Item, { icon: resource.options ? resource.options.icon : null, key: index },
            React.createElement(Link, { to: getLinkUrl(resource) }, getLabel(resource))));
    }))) : null;
});
export default AppMenu;
