import React, { useEffect } from "react";
import { Switch, useLocation } from "react-router-dom";
import { observer } from "mobx-react";
import { useResourceStore } from "../../hooks/useResourceStore";
import ResourceWithLayout from "./ResourceWithLayout";
var ResourceRouter = observer(function (props) {
    var resourceStore = useResourceStore();
    var currentLocation = useLocation();
    var currentResource = resourceStore.getCurrentResource() || resourceStore.resources[0];
    useEffect(function () {
        resourceStore.setCurrentResource(currentLocation.pathname.split("/", 2)[1]);
    }, [currentLocation.pathname]);
    return currentResource ? (React.createElement(Switch, { key: currentResource.name },
        React.createElement(ResourceWithLayout, { exact: true, path: "/" + currentResource.name + "/create", view: currentResource.create }),
        React.createElement(ResourceWithLayout, { exact: true, path: "/" + currentResource.name + "/list", view: currentResource.list }),
        React.createElement(ResourceWithLayout, { exact: true, path: "/" + currentResource.name + "/show/:id", view: currentResource.show }),
        React.createElement(ResourceWithLayout, { exact: true, path: "/" + currentResource.name + "/edit/:id", view: currentResource.edit }))) : null;
});
export default ResourceRouter;
