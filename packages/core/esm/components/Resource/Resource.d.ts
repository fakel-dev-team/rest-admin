import React from "react";
import { ResourceOptions } from "../../stores/ResorceStore";
declare type ResourceProps = {
    name: string;
    options?: ResourceOptions;
    create?: React.ComponentType<any>;
    list?: React.ComponentType<any>;
    edit?: React.ComponentType<any>;
    show?: React.ComponentType<any>;
};
declare const Resource: React.FC<ResourceProps>;
export default Resource;
