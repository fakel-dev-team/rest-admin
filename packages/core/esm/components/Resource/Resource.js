import { useEffect } from "react";
import { observer } from "mobx-react";
import { useResourceStore } from "../../hooks/useResourceStore";
var Resource = observer(function (resource) {
    var resourceStore = useResourceStore();
    useEffect(function () {
        resourceStore.pushResource(resource);
    }, []);
    return null;
});
export default Resource;
