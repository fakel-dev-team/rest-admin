import React from "react";
import Result from "antd/lib/result";
import Button from "antd/lib/button";
var ErrorFallback = function (_a) {
    var message = _a.message, description = _a.description, resetErrorBoundary = _a.resetErrorBoundary;
    return (React.createElement("div", { style: {
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
        } },
        React.createElement(Result, { status: "error", title: "Something went wrong:", subTitle: message, extra: [
                React.createElement(Button, { key: "reset", onClick: resetErrorBoundary }, "Reset"),
            ] })));
};
export default ErrorFallback;
