import React, { useEffect, useState } from "react";
import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useShowStore } from "../../hooks/useShowStore";
import { useParams } from "react-router-dom";
import { observer } from "mobx-react";
import LoadingOutlined from "@ant-design/icons/lib/icons/LoadingOutlined";
import Row from "antd/lib/row";
import Typography from "antd/lib/typography";
import Space from "antd/lib/space";
import Spin from "antd/lib/spin";
import ShowActions from "./ShowActions";
var Title = Typography.Title;
var Show = observer(function (_a) {
    var title = _a.title, actions = _a.actions, children = _a.children;
    var dataProviderStore = useDataProviderStore();
    var resourceStore = useResourceStore();
    var showStore = useShowStore();
    var _b = useState(false), loading = _b[0], setLoading = _b[1];
    var id = useParams().id;
    useEffect(function () {
        setLoading(true);
        if (resourceStore.currentResource) {
            showStore.getData(dataProviderStore.dataProvider, resourceStore.currentResource, { id: id });
            setLoading(false);
        }
    }, [resourceStore.currentResource]);
    return loading ? (React.createElement("div", { style: {
            width: "100%",
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
        } },
        React.createElement(Spin, { indicator: React.createElement(LoadingOutlined, { style: { fontSize: 24 } }) }))) : (showStore.data && (React.createElement(React.Fragment, null,
        React.createElement(Row, { justify: "space-between" },
            typeof title === "function" ? (title(showStore.data)) : (React.createElement(Title, { level: 3 }, title)),
            React.createElement(ShowActions, { id: id }, actions)),
        React.createElement(Space, { direction: "vertical" }, React.Children.map(children, function (child) {
            return React.cloneElement(child, {
                records: [showStore.data],
            });
        })))));
});
export default Show;
