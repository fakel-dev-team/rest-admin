import React from "react";
declare type ShowActionsProps = {
    id: string;
};
declare const ShowActions: React.FC<ShowActionsProps>;
export default ShowActions;
