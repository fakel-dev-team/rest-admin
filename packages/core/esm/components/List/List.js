import React, { useEffect } from "react";
import { ListView, DefaultListActions } from "./index";
import { observer } from "mobx-react";
import { toJS } from "mobx";
import Space from "antd/lib/space";
import Col from "antd/lib/col";
import Row from "antd/lib/row";
import { useListStore } from "../../hooks/useListStore";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useFiltersStore } from "../../hooks/useFiltersStore";
import { mapToAntColumn } from "../../utils/List";
var List = observer(function (_a) {
    var columns = _a.columns, filters = _a.filters, actions = _a.actions, perPage = _a.perPage, view = _a.view;
    var dataProviderStore = useDataProviderStore();
    var resourceStore = useResourceStore();
    var listStore = useListStore();
    var filtersStore = useFiltersStore();
    var tableColumns = mapToAntColumn(columns);
    useEffect(function () {
        listStore.getData(dataProviderStore.dataProvider, resourceStore.currentResource);
    }, [resourceStore.currentResource, filtersStore.displayFilters]);
    return (React.createElement(Space, { direction: "vertical" },
        React.createElement(Row, { justify: "space-between", align: "top" },
            React.createElement(Col, { span: "4" },
                actions || React.createElement(DefaultListActions, null),
                " "),
            React.createElement(Col, { style: { display: "flex", justifyContent: "flex-end" }, span: "20" }, filters)),
        React.createElement(ListView, { loading: toJS(listStore.loading), view: view, listStore: listStore, dataSource: toJS(listStore.dataSource), ids: listStore.ids, perPage: perPage || 20, resource: resourceStore.currentResource, columns: tableColumns })));
});
export default List;
