import React from "react";
import { ListStore } from "../../stores";
declare type ListViewProps = {
    dataSource: any;
    ids: number[];
    columns: any;
    resource: string;
    perPage?: number;
    page?: number;
    listStore: ListStore;
    loading?: boolean;
    view?: "edit" | "show";
};
declare const ListView: React.FC<ListViewProps>;
export default ListView;
