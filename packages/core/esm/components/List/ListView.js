var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import Table from "antd/lib/table";
import { ListSelection } from "./index";
import { observer } from "mobx-react";
import { useDataProviderStore } from "../../hooks";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useListStore } from "../../hooks/useListStore";
var ListView = observer(function (props) {
    var dataSource = props.dataSource, columns = props.columns, resource = props.resource, perPage = props.perPage, page = props.page, loading = props.loading, view = props.view;
    var dataProviderStore = useDataProviderStore();
    var listStore = useListStore();
    var resourceStore = useResourceStore();
    var history = useHistory();
    var _a = useState([]), selectedRows = _a[0], setSelectedRows = _a[1];
    var onSelect = function (selectedRowsIds) {
        setSelectedRows(__spreadArrays(selectedRowsIds));
    };
    var onRow = function (record) {
        if (!record.id) {
            console.warn("WARNING: Record must have an id parameter");
        }
        return {
            onClick: function () {
                history.push("/" + resource + "/" + (view ? view : "edit") + "/" + record.id);
            },
        };
    };
    var onPageChange = function (page, pageSize) {
        listStore.getData(dataProviderStore.dataProvider, resourceStore.currentResource, { page: page, pageSize: pageSize });
    };
    return (React.createElement(React.Fragment, null,
        selectedRows && selectedRows.length ? (React.createElement(ListSelection, { dataSource: dataSource, setSelectedRows: setSelectedRows, selectedRows: selectedRows })) : null,
        React.createElement(Table, { loading: loading, style: { minWidth: "80vw" }, size: "middle", onRow: onRow, rowSelection: {
                type: "checkbox",
                selectedRowKeys: selectedRows,
                onChange: onSelect,
            }, dataSource: dataSource, columns: columns, pagination: {
                showSizeChanger: true,
                total: listStore.total,
                defaultPageSize: perPage,
                defaultCurrent: page,
                onChange: onPageChange,
            } })));
});
export default ListView;
