import React from "react";
import Select from "antd/lib/select";
import CoreInput from "./Input";
import { mapRecordsToOptions } from "../../utils/Select";
var Option = Select.Option;
var SelectInput = function (props) {
    var options;
    if (!props.options) {
        options = mapRecordsToOptions(props.record, props.valuePropName, props.titlePropName);
    }
    var filter = function (input, option) {
        return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
    };
    var sort = function (optionA, optionB) {
        return optionA.children
            .toLowerCase()
            .localeCompare(optionB.children.toLowerCase());
    };
    return (React.createElement(CoreInput, { label: props.label, style: { minWidth: 200 }, name: props.name, placeholder: props.placeholder || "Search", options: props.options || options, component: Select, componentChildren: function (options) {
            return options.map(function (option, index) { return (React.createElement(Option, { key: option.key || index, value: option.value }, option.title)); });
        }, showSearch: true, optionFilterProp: "children", filterOption: props.filter || filter, filterSort: props.sort || sort }));
};
export default SelectInput;
