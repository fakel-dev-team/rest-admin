var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import { Field } from "formik";
import { useAntComponent } from "../../hooks/useAntComponent";
import Typography from "antd/lib/typography";
import Form from "antd/lib/form";
var Text = Typography.Text;
var CoreInput = function (props) {
    return (React.createElement(Field, { name: props.name }, function (_a) {
        var form = _a.form, field = _a.field, meta = _a.meta;
        return props.component ? (useAntComponent({
            Component: props.component,
            ComponentChildren: props.componentChildren,
            form: form,
            field: field,
            meta: meta,
            props: props,
        })) : (React.createElement(Form.Item, __assign({ initialValue: props.defaultValue, validateStatus: meta.touched && meta.error ? "error" : "success" }, props), props.children(form, field, meta)));
    }));
};
export default CoreInput;
