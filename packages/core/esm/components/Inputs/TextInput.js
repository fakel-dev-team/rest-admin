var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import Input from "antd/lib/input";
import CoreInput from "./Input";
var InputField = function (props) {
    return (React.createElement(CoreInput, __assign({}, props, { style: { width: "100%" }, component: Input })));
};
export default InputField;
