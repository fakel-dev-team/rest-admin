import React from "react";
import { FieldConfig, FormikProps, FieldProps } from "formik";
export interface InputProps extends FieldConfig {
    component?: any;
    componentChildren?: (value: any) => any;
    children?: (form: FormikProps<any>, field: FieldProps, meta: any) => any;
}
declare const CoreInput: React.FC<InputProps & any>;
export default CoreInput;
