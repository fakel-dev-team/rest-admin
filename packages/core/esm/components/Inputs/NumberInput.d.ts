import React from "react";
import { FormItemProps } from "antd/lib/form";
import { InputProps } from "./Input";
interface NumberInputProps extends InputProps {
    min?: number;
    max?: number;
    defaultValue?: number;
}
declare const NumberInput: React.FC<NumberInputProps & FormItemProps>;
export default NumberInput;
