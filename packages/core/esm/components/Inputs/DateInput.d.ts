import React from "react";
import { FormItemProps } from "antd/lib/form";
import { InputProps } from "./Input";
interface DateInputProps extends InputProps {
}
declare const DateInput: React.FC<DateInputProps & FormItemProps>;
export default DateInput;
