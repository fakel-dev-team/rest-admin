import React from "react";
export declare type ReferenceInputProps = {
    source: string;
    reference: string;
    label?: string;
    record?: any;
};
declare const ReferenceInput: React.FC<ReferenceInputProps>;
export default ReferenceInput;
