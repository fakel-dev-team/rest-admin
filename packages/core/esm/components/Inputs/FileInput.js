var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import Upload from "antd/lib/upload";
import CoreInput from "./Input";
var FileInput = function (props) {
    return (React.createElement(CoreInput, __assign({}, props), function (form, field, meta) {
        return React.createElement(Upload, __assign({}, props), props.button);
    }));
};
export default FileInput;
