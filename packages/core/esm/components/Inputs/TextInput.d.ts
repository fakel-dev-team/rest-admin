import React from "react";
import { InputProps } from "./Input";
import { AntFieldProps } from "../../@types";
interface InputFieldProps extends InputProps {
}
declare const InputField: React.FC<InputFieldProps & AntFieldProps>;
export default InputField;
