var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import Switch from "antd/lib/switch";
import CoreInput from "./Input";
var SwitchInput = function (props) {
    return React.createElement(CoreInput, __assign({}, props, { component: Switch }));
};
export default SwitchInput;
