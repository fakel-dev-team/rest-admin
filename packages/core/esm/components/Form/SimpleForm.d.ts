import React from "react";
declare type SimpleFormInterface = {
    initialValue: any;
    handleSubmit?: (...any: any[]) => any;
    actions?: React.ReactNode;
    buttonText?: string;
};
declare const SimpleForm: React.FC<SimpleFormInterface>;
export default SimpleForm;
