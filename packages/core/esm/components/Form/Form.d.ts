import React from "react";
import { FormikErrors, FormikValues, FormikHelpers } from "formik";
export interface AppFormProps {
    id?: string;
    name?: string;
    validate?: (values: FormikValues, props: any) => FormikErrors<any> | Promise<any>;
    validationSchema?: any;
    initialValue?: any;
    handleSubmit?: (values: any, formikHelpers: FormikHelpers<any>) => void;
}
declare const AppForm: React.FC<AppFormProps>;
export default AppForm;
