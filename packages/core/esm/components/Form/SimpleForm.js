var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import { Form } from "./index";
import Space from "antd/lib/space";
import { Button } from "antd";
var SimpleForm = function (props) {
    return props.initialValue ? (React.createElement(Form, __assign({ handleSubmit: props.handleSubmit, initialValue: props.initialValue }, props),
        React.createElement(Space, { direction: "vertical" },
            props.children,
            props.actions || (React.createElement(Button, { type: "primary", htmlType: "submit" }, props.buttonText || "Save"))))) : null;
};
export default SimpleForm;
