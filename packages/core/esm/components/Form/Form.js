import React from "react";
import { Formik, } from "formik";
import Form from 'antd/lib/form';
var AppForm = function (props) {
    var id = props.id, validate = props.validate, initialValue = props.initialValue, validationSchema = props.validationSchema, children = props.children, handleSubmit = props.handleSubmit;
    return (React.createElement(Formik, { validationSchema: validationSchema, initialValues: initialValue, onSubmit: handleSubmit }, function (formikProps) {
        return (React.createElement(Form, { id: id, layout: "vertical", onFinish: formikProps.handleSubmit }, children));
    }));
};
export default AppForm;
