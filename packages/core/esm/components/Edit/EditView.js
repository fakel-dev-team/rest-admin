import React from "react";
import Space from "antd/lib/space";
import Typography from "antd/lib/typography";
var Title = Typography.Title;
var EditView = function (_a) {
    var title = _a.title, children = _a.children;
    var defaultTitle = "Edit form";
    return (React.createElement(Space, { direction: "vertical" },
        React.createElement(Title, { level: 4 }, title || defaultTitle),
        children));
};
export default EditView;
