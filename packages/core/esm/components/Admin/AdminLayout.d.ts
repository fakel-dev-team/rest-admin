import React from "react";
export declare type AdminLayoutProps = {
    logoTitle?: string;
    userView?: React.ReactNode;
};
declare const AdminLayout: React.FC<AdminLayoutProps>;
export default AdminLayout;
