import React from 'react';
import { HashRouter as Router } from 'react-router-dom';
import { AdminStoreProvider, AdminRouter } from './index';
var Admin = function (_a) {
    var dataProvider = _a.dataProvider, children = _a.children, authProvider = _a.authProvider, userView = _a.userView, logoTitle = _a.logoTitle, defaultResource = _a.defaultResource;
    return (React.createElement(Router, { hashType: "slash" },
        React.createElement(AdminStoreProvider, { authProvider: authProvider, dataProvider: dataProvider },
            children,
            React.createElement(AdminRouter, null))));
};
export default Admin;
