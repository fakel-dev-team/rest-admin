import React from "react";
import { AdminStore } from "../../stores/AdminStore";
import StoreProvider from "../StoreProvider";
var AdminStoreProvider = function (_a) {
    var dataProvider = _a.dataProvider, authProvider = _a.authProvider, children = _a.children;
    var adminStore = new AdminStore(dataProvider, authProvider);
    return React.createElement(StoreProvider, { adminStore: adminStore }, children);
};
export default AdminStoreProvider;
