import React from "react";
import Layout from "antd/lib/layout";
import Typography from "antd/lib/typography";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import { Menu } from "../index";
import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from "../ErrorFallback/ErrorFallback";
import { observer } from "mobx-react";
import { useAuthStore } from "../../hooks/useAuthStore";
var Header = Layout.Header, Content = Layout.Content, Sider = Layout.Sider;
var Title = Typography.Title, AntLink = Typography.Link;
var AdminLayout = observer(function (_a) {
    var children = _a.children, logoTitle = _a.logoTitle, userView = _a.userView;
    // const { user } = useGetMe();
    // console.log(user);
    var authStore = useAuthStore();
    return (React.createElement(Layout, { style: { minHeight: "100vh" } },
        React.createElement(Header, null,
            React.createElement(Row, { justify: "space-between", align: "middle" },
                React.createElement(Col, { span: 6 },
                    React.createElement(AntLink, { href: "/" },
                        React.createElement(Title, { className: "header__logo", level: 3, style: { color: "#fff", marginBottom: 0 } }, logoTitle || "Admin"))))),
        React.createElement(Layout, null,
            React.createElement(Sider, { width: 200, className: "site-layout-background" },
                React.createElement(Menu, null)),
            React.createElement(Layout, { style: { padding: "20px" } },
                React.createElement(ErrorBoundary, { fallbackRender: function (_a) {
                        var error = _a.error, resetErrorBoundary = _a.resetErrorBoundary;
                        return (React.createElement(ErrorFallback, { message: error.message, resetErrorBoundary: resetErrorBoundary }));
                    } },
                    React.createElement(Content, { className: "site-layout-background" }, children))))));
});
export default AdminLayout;
