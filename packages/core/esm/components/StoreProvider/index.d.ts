import React from "react";
import { AdminStore } from "../../stores/AdminStore";
declare type StoreProviderProps = {
    adminStore: AdminStore;
};
export declare const AdminContext: React.Context<StoreProviderProps>;
declare const StoreProvider: React.FC<StoreProviderProps>;
export default StoreProvider;
