import React, { createContext } from "react";
export var AdminContext = createContext({
    adminStore: null,
});
var StoreProvider = function (_a) {
    var adminStore = _a.adminStore, children = _a.children;
    return (React.createElement(AdminContext.Provider, { value: { adminStore: adminStore } }, children));
};
export default StoreProvider;
