import React from "react";
import Space from "antd/lib/space";
import Typography from "antd/lib/typography";
import Row from "antd/lib/row";
var Title = Typography.Title;
var CreateView = function (_a) {
    var actions = _a.actions, title = _a.title, children = _a.children;
    var defaultTitle = "Create form";
    return (React.createElement(Space, { direction: "vertical" },
        React.createElement(Row, { justify: "space-between", align: "middle" },
            React.createElement(Title, { level: 4 }, title || defaultTitle),
            actions),
        children));
};
export default CreateView;
