import React from "react";
import { RedirectTo } from "../../@types";
/**
 * @component Create
 * @props
 *  title? - string (Title for create form)
 *  children - (props: any) => any
 *  redirect? - "list" | "show" | "create" | "edit"
 *
 * @example
 * <Create redirect="list">
      {(createProps) => {
        return (
          <SimpleForm
            {...createProps}
            initialValue={{
              username: "",
              email: "",
            }}
          >
           <TextInput label="Username" name="username" />
            <TextInput
              label="Email"
              name="email"
              placeholder="Enter email"
            />
          </SimpleForm>
        );
      }}
    </Create>
 * */
declare type CreateProps = {
    title?: string;
    children: (props?: any) => any;
    redirect?: RedirectTo;
    saveRedirect?: (...any: any[]) => void;
    onSuccessfulSubmit?: () => void;
    onFailedSubmit?: (error?: any) => void;
    handleSubmit?: (value: any, actions: any) => any;
};
declare const Create: React.FC<CreateProps>;
export default Create;
