var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React from "react";
import Col from "antd/lib/col";
import Button from "antd/lib/button";
import CloseOutlined from "@ant-design/icons/lib/icons/CloseOutlined";
var DisplayFilter = function (_a) {
    var displayFilter = _a.displayFilter, setDisplayFilters = _a.setDisplayFilters, setFilters = _a.setFilters, filtersStore = _a.filtersStore;
    var handleDeleteButton = function (displayFilter, source) {
        setDisplayFilters(function (displayFilters) {
            return displayFilters.filter(function (_displayFilter) { return _displayFilter !== displayFilter; });
        });
        setFilters(function (filters) {
            var isExist = filters.find(function (filter) { return filter.source === source; });
            if (!isExist) {
                return __spreadArrays(filters, [filtersStore.getFilter(source)]);
            }
            return filters;
        });
    };
    return (React.createElement(Col, { span: "8", style: {
            display: "flex",
            alignItems: "center",
        } },
        React.createElement(Button, { onClick: function () {
                return handleDeleteButton(displayFilter, displayFilter.props.name);
            }, size: "small", type: "ghost", shape: "circle", style: { marginRight: 10 }, icon: React.createElement(CloseOutlined, { style: { fontSize: 12 } }) }),
        displayFilter));
};
export default DisplayFilter;
