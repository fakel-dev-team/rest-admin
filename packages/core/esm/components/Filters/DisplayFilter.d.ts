import React from "react";
import { FiltersStore } from "../../stores";
/**
 * @component DisplayFilter
 * @props
 *  displayFilter - React.Node
 *  setDisplayFilter - @function (displayFilter) => void
 *  setFilters - @function (filters) => void
 *
 * @example
 *  <Filters>
 *    const [displayFilters, setDisplayFilters] = useState([])
 *    const [filters, setFilters] = useState([])
 *
 *    return (
 *      displayFilters.map(displayFilter =>
 *        <DisplayFilter
 *          displayFilter={displayFilter}
 *          filters={filters}
 *          setFilters={setFilters}
 *          setDisplayFilters={setDisplayFilters}
 *        />)
 *    )
 *  </Filters>
 *
 * */
declare type DisplayFiltersProps = {
    displayFilter: any;
    setDisplayFilters: (displayFilters: any) => any;
    setFilters: (filters: any) => any;
    filtersStore: FiltersStore;
};
declare const DisplayFilter: React.FC<DisplayFiltersProps>;
export default DisplayFilter;
