var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
import React, { useEffect, useState } from "react";
import Space from "antd/lib/space";
import Button from "antd/lib/button";
import Dropdown from "antd/lib/dropdown";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
/**
 * @hook_stores - hooks for getting stores
 */
import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useFiltersStore } from "../../hooks/useFiltersStore";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useListStore } from "../../hooks/useListStore";
import { Form } from "../Form";
import DisplayFilter from "./DisplayFilter";
import FiltersMenu from "./FiltersMenu";
import { observer } from "mobx-react";
/**
 * @function getDefaultFilters
 * @params
 *  filterStore - type FilterStore
 *  defaultFilters - Array of string
 *
 * @description
 *  takes filters from filterStore and returns only those that are in the default filters
 * */
var getDefaultFilters = function (filtersStore, defaultFilters) {
    return defaultFilters && defaultFilters.length
        ? defaultFilters.map(function (defaultFilter) {
            return filtersStore.getFilter(defaultFilter).DisplayFilterComponent;
        })
        : [];
};
/**
 * @component Filters
 * @props
 *  children - React.Nodes
 *  always_visible - boolean (if true - necessary default filters)
 *  defaultFilters - Array of string
 *
 * @example
 *  <Filters>
 *    <TextInput name="title" placeholder="Title" />
      <TextInput name="body" placeholder="Body" />
 *  </Filters>
 * */
var Filters = observer(function (_a) {
    var children = _a.children, always_visible = _a.always_visible, defaultFilters = _a.defaultFilters;
    var dataProviderStore = useDataProviderStore();
    var listStore = useListStore();
    var resourceStore = useResourceStore();
    var filtersStore = useFiltersStore();
    var _b = useState([]), filters = _b[0], setFilters = _b[1];
    var _c = useState([]), displayFilters = _c[0], setDisplayFilters = _c[1];
    var handleSubmit = function (values) { return __awaiter(void 0, void 0, void 0, function () {
        var valuesKeys;
        return __generator(this, function (_a) {
            listStore.getData(dataProviderStore.dataProvider, resourceStore.currentResource, { filters: values });
            valuesKeys = Object.keys(values);
            valuesKeys.map(function (valueKey) {
                return filtersStore.setFilterValue(valueKey, values[valueKey]);
            });
            return [2 /*return*/];
        });
    }); };
    var _filters = React.Children.map(children, function (child) { return ({
        source: child.props.name,
        value: "",
        label: child.props.label || child.props.name,
        DisplayFilterComponent: child,
    }); });
    var mapfiltersToInitialValue = function () {
        var initialValues = {};
        filtersStore.filters.forEach(function (filter) {
            var _a;
            initialValues = __assign(__assign({}, initialValues), (_a = {}, _a[filter.source] = "", _a));
        });
        return initialValues;
    };
    useEffect(function () {
        filtersStore.filters = _filters;
        if (always_visible) {
            setDisplayFilters(getDefaultFilters(filtersStore, defaultFilters));
        }
    }, []);
    useEffect(function () {
        if (always_visible) {
            setFilters(filtersStore.filters.filter(function (filter) { return !defaultFilters.includes(filter.source); }));
        }
        else {
            setFilters(filtersStore.filters);
        }
    }, [filtersStore.filters]);
    useEffect(function () {
        filtersStore.displayFilters = displayFilters;
    }, [displayFilters]);
    return (React.createElement(Space, null,
        React.createElement(Form, { handleSubmit: handleSubmit, initialValue: mapfiltersToInitialValue() }, displayFilters && displayFilters.length ? (React.createElement(React.Fragment, null,
            React.createElement(Space, null, displayFilters.map(function (displayFilter, index) { return (React.createElement(DisplayFilter, { key: "displayFilter-" + index, displayFilter: displayFilter, setDisplayFilters: setDisplayFilters, setFilters: setFilters, filtersStore: filtersStore })); })),
            React.createElement(Button, { style: { marginLeft: 10 }, type: "primary", htmlType: "submit" }, "Filter"))) : null),
        React.createElement(Dropdown, { trigger: ["click"], overlay: React.createElement(FiltersMenu, { setDisplayFilters: setDisplayFilters, setFilters: setFilters, filters: filters }) },
            React.createElement(Button, null,
                "Add Filter ",
                React.createElement(PlusOutlined, null)))));
});
export default Filters;
