var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
import React from "react";
import Menu from "antd/lib/menu";
import { observer } from "mobx-react";
var FiltersMenu = observer(function (_a) {
    var setDisplayFilters = _a.setDisplayFilters, setFilters = _a.setFilters, filters = _a.filters;
    var handleMenuClick = function (filter) {
        var displayFilter = filter.DisplayFilterComponent;
        setDisplayFilters(function (displayFilters) {
            var isExist = displayFilters.find(function (dF) { return dF === displayFilter; });
            if (!isExist) {
                return __spreadArrays(displayFilters, [displayFilter]);
            }
            return displayFilters;
        });
        setFilters(function (filters) { return filters.filter(function (_filter) { return _filter !== filter; }); });
    };
    return (React.createElement(Menu, null, filters.map(function (filter, index) { return (React.createElement(Menu.Item, { onClick: function () { return handleMenuClick(filter); }, key: index }, filter.label)); })));
});
export default FiltersMenu;
