import React from "react";
import { Filter } from "../../stores";
/**
 * @component FiltersMenu
 * @props
 *  setDisplayFilters - (displayFilters) => void
 *  setFilters - (filters) => void
 *  filters - Array of Filter
 *
 * @example
 *  <FiltersMenu
      setDisplayFilters={setDisplayFilters}
      setFilters={setFilters}
      filters={filters}
    />
 * */
declare type FiltersMenuProps = {
    setDisplayFilters: (displayFilters: any) => any;
    setFilters: (filters: any) => any;
    filters: Filter[];
};
declare const FiltersMenu: React.FC<FiltersMenuProps>;
export default FiltersMenu;
