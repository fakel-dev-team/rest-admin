import React from "react";
declare type FiltersProps = {
    always_visible?: boolean;
    defaultFilters?: string[];
};
/**
 * @component Filters
 * @props
 *  children - React.Nodes
 *  always_visible - boolean (if true - necessary default filters)
 *  defaultFilters - Array of string
 *
 * @example
 *  <Filters>
 *    <TextInput name="title" placeholder="Title" />
      <TextInput name="body" placeholder="Body" />
 *  </Filters>
 * */
declare const Filters: React.FC<FiltersProps>;
export default Filters;
