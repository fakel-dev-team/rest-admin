import get from "lodash.get";
var Field = function (_a) {
    var record = _a.record, source = _a.source, children = _a.children;
    var value = get(record, source);
    return children(value);
};
export default Field;
