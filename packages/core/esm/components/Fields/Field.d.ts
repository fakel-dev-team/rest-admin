import React from "react";
/**
 * @component Field
 * @props
 *  record - any (record from data source)
 *  source - string (name property in record)
 *  children - (value: any) => any
 *
 * @example
 * const record = {id: 1, name: "Name"}
 * const source = "name"
 * <Field source={source} record={record}>
      {(value) => (value ? <CheckOutlined /> : <CloseOutlined />)}
    </Field>
 * */
declare type FieldProps = {
    record: any;
    source: string;
    children: (value: any) => any;
};
declare const Field: React.FC<FieldProps>;
export default Field;
