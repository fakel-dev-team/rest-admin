import React from "react";
import Typography from "antd/lib/typography";
import Field from "./Field";
var Text = Typography.Text;
var useLinkStyle = {
    color: "#1890ff",
};
var TextField = function (_a) {
    var record = _a.record, source = _a.source, style = _a.style, _b = _a.isLink, isLink = _b === void 0 ? false : _b, label = _a.label;
    return (React.createElement(Field, { record: record, source: source }, function (value) { return (React.createElement(React.Fragment, null,
        label && (React.createElement(React.Fragment, null,
            React.createElement(Text, { type: "secondary" }, label),
            React.createElement("br", null))),
        React.createElement(Text, { style: Object.assign({}, style, isLink ? useLinkStyle : null) }, value))); }));
};
export default TextField;
