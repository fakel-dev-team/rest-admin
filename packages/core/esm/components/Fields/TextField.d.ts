import React from "react";
import { FieldProps } from "../../@types";
declare type TextFieldProps = {
    style?: React.CSSProperties;
    isLink?: boolean;
};
declare const TextField: React.FC<FieldProps & TextFieldProps>;
export default TextField;
