var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import { observer } from "mobx-react";
import Space from "antd/lib/space";
var FieldList = observer(function (props) {
    return (React.createElement(Space, { direction: props.direction || "vertical" }, props.records &&
        props.records.map(function (record) {
            return React.Children.map(props.children, function (child) {
                return React.cloneElement(child, __assign(__assign({}, props), { record: record }));
            });
        })));
});
export default FieldList;
