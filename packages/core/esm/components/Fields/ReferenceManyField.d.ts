import React from "react";
import { LinkT } from "../../@types";
export declare type ReferenceManyFieldProps<RecordT = any> = {
    name: string;
    reference: string;
    link?: LinkT;
    record?: RecordT;
};
declare function ReferenceManyField<RecordT>({ name, reference, link, children, record, }: React.PropsWithChildren<ReferenceManyFieldProps<RecordT>>): JSX.Element;
export default ReferenceManyField;
