import React from "react";
import { FieldProps } from "../../@types";
declare type TagField = {
    color?: string;
    icon?: string;
    visible?: boolean;
};
declare const TagField: React.FC<FieldProps & TagField>;
export default TagField;
