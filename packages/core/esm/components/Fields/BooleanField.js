import React from "react";
import Field from "./Field";
import CheckOutlined from "@ant-design/icons/lib/icons/CheckOutlined";
import CloseOutlined from "@ant-design/icons/lib/icons/CloseOutlined";
var BooleanField = function (props) {
    var source = props.source, record = props.record;
    return (React.createElement(Field, { source: source, record: record }, function (value) { return (value ? React.createElement(CheckOutlined, null) : React.createElement(CloseOutlined, null)); }));
};
export default BooleanField;
