var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
import React from "react";
import Tag from "antd/lib/tag";
import Field from "./Field";
var TagField = function (props) {
    var source = props.source, record = props.record;
    return (React.createElement(Field, { source: source, record: record }, function (value) { return (React.createElement(Tag, __assign({}, props, { color: "default" }), value)); }));
};
export default TagField;
