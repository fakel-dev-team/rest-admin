import React from "react";
import type { FieldProps, LinkT } from "../../@types";
export declare type ReferenceFieldProps = {
    reference: string;
    link: LinkT | false;
    children: (props?: any) => any;
};
export declare const createLink: (link: LinkT, reference: string, source: any) => string;
declare const ReferenceField: React.FC<FieldProps & ReferenceFieldProps>;
export default ReferenceField;
