"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.DataProviderStore = void 0;
var mobx_1 = require("mobx");
var DataProviderStore = /** @class */ (function () {
    function DataProviderStore(dataProvider) {
        mobx_1.makeAutoObservable(this);
        this.provider = dataProvider;
    }
    Object.defineProperty(DataProviderStore.prototype, "dataProvider", {
        get: function () {
            return this.provider;
        },
        enumerable: false,
        configurable: true
    });
    return DataProviderStore;
}());
exports.DataProviderStore = DataProviderStore;
