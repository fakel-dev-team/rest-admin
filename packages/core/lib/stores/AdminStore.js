"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminStore = void 0;
var AuthStore_1 = require("./AuthStore");
var AuthProviderStore_1 = require("./AuthProviderStore");
var ImagesStore_1 = require("./ImagesStore");
var ShowStore_1 = require("./ShowStore");
var UrlStore_1 = require("./UrlStore");
var ListStore_1 = require("./ListStore");
var ResorceStore_1 = require("./ResorceStore");
var FiltersStore_1 = require("./FiltersStore");
var DataProviderStore_1 = require("./DataProviderStore");
var mobx_1 = require("mobx");
var AdminStore = /** @class */ (function () {
    function AdminStore(dataProvider, authProvider) {
        mobx_1.makeAutoObservable(this);
        this._dataProviderStore = new DataProviderStore_1.DataProviderStore(dataProvider);
        this._authProviderStore = new AuthProviderStore_1.AuthProviderStore(authProvider);
        this._resourceStore = new ResorceStore_1.ResourceStore();
        this._filtersStore = new FiltersStore_1.FiltersStore();
        this._listStore = new ListStore_1.ListStore();
        this._urlStore = new UrlStore_1.URLStore();
        this._showStore = new ShowStore_1.ShowStore();
        this._imagesStore = new ImagesStore_1.ImagesStore();
        this._authStore = new AuthStore_1.AuthStore();
    }
    Object.defineProperty(AdminStore.prototype, "showStore", {
        get: function () {
            return this._showStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "authProviderStore", {
        get: function () {
            return this._authProviderStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "authStore", {
        get: function () {
            return this._authStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "imagesStore", {
        get: function () {
            return this._imagesStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "urlStore", {
        get: function () {
            return this._urlStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "listStore", {
        get: function () {
            return this._listStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "filtersStore", {
        get: function () {
            return this._filtersStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "dataProviderStore", {
        get: function () {
            return this._dataProviderStore;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(AdminStore.prototype, "resourceStore", {
        get: function () {
            return this._resourceStore;
        },
        enumerable: false,
        configurable: true
    });
    return AdminStore;
}());
exports.AdminStore = AdminStore;
