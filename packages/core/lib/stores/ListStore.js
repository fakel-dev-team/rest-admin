"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ListStore = void 0;
var mobx_1 = require("mobx");
var ListStore = /** @class */ (function () {
    function ListStore() {
        var _this = this;
        this.dataSource = [];
        this.ids = [];
        this.total = 0;
        this.loading = false;
        this.setDataSource = function (value) { return (_this.dataSource = value); };
        this.setTotal = function (value) { return (_this.total = value); };
        this.setDataSourceIds = function (value) { return (_this.ids = value); };
        mobx_1.makeObservable(this, {
            dataSource: mobx_1.observable,
            ids: mobx_1.observable,
            total: mobx_1.observable,
            loading: mobx_1.observable,
        });
    }
    ListStore.prototype.setLoading = function (loading) {
        this.loading = loading;
    };
    ListStore.prototype.deleteSelectedRecords = function (ids) {
        console.log(ids);
        this.dataSource = this.dataSource.filter(function (record, index) {
            return !ids.includes(record.id);
        });
    };
    ListStore.prototype.deleteRecords = function (dataProvider, resource, ids) {
        return __awaiter(this, void 0, void 0, function () {
            var updatedData;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(ids);
                        this.deleteSelectedRecords(ids);
                        return [4 /*yield*/, dataProvider.delete(resource, {
                                id: ids,
                            })];
                    case 1:
                        updatedData = (_a.sent()).data;
                        this.dataSource = updatedData;
                        return [2 /*return*/];
                }
            });
        });
    };
    ListStore.prototype.getData = function (dataProvider, resource, params) {
        if (params === void 0) { params = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var _a, records, total, ids, dataWithKey;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        this.setLoading(true);
                        return [4 /*yield*/, dataProvider.getList(resource, params)];
                    case 1:
                        _a = _b.sent(), records = _a.data, total = _a.total;
                        ids = records.map(function (_, index) { return index; });
                        dataWithKey = records.map(function (dataItem, index) { return (__assign(__assign({}, dataItem), { key: dataItem.id || index })); });
                        this.setDataSource(dataWithKey);
                        this.setDataSourceIds(ids);
                        this.setTotal(total);
                        this.setLoading(false);
                        return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        mobx_1.action("set loading"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Boolean]),
        __metadata("design:returntype", void 0)
    ], ListStore.prototype, "setLoading", null);
    __decorate([
        mobx_1.action("delete selected records"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Array]),
        __metadata("design:returntype", void 0)
    ], ListStore.prototype, "deleteSelectedRecords", null);
    __decorate([
        mobx_1.action("delete records"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, String, Array]),
        __metadata("design:returntype", Promise)
    ], ListStore.prototype, "deleteRecords", null);
    __decorate([
        mobx_1.action("get list data"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object, String, Object]),
        __metadata("design:returntype", Promise)
    ], ListStore.prototype, "getData", null);
    __decorate([
        mobx_1.action("set data source"),
        __metadata("design:type", Object)
    ], ListStore.prototype, "setDataSource", void 0);
    __decorate([
        mobx_1.action("set total"),
        __metadata("design:type", Object)
    ], ListStore.prototype, "setTotal", void 0);
    __decorate([
        mobx_1.action("set data source ids"),
        __metadata("design:type", Object)
    ], ListStore.prototype, "setDataSourceIds", void 0);
    return ListStore;
}());
exports.ListStore = ListStore;
