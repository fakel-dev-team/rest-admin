import type { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
export declare class ShowStore {
    private _data;
    constructor();
    getData(dataProvider: DataProviderT, resource: string, params?: {}): Promise<void>;
    set data(value: any);
    get data(): any;
}
