import { LinkT } from "./../@types/index";
import type React from "react";
export declare type ResourceOptions = {
    label?: string;
    icon?: React.ReactNode;
    isReference?: boolean;
    renderViewOnMenuClicked?: LinkT;
    linkUrl?: string;
};
export declare type ResourceT = {
    name: string;
    create?: React.ComponentType<any>;
    edit?: React.ComponentType<any>;
    list?: React.ComponentType<any>;
    show?: React.ComponentType<any>;
    options?: ResourceOptions;
};
export declare class ResourceStore {
    resources: ResourceT[];
    currentResource: string;
    constructor();
    setCurrentResource(currentResource: string): void;
    pushResource(resource: ResourceT): void;
    getResource(name: string): ResourceT;
    getCurrentResource(): ResourceT;
    isRegistred(name: string): boolean;
}
