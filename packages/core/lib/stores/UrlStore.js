"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.URLStore = void 0;
var url_1 = require("../utils/url");
var mobx_1 = require("mobx");
var URLStore = /** @class */ (function () {
    function URLStore() {
        mobx_1.makeAutoObservable(this);
        this._url = "";
        this._params = null;
    }
    URLStore.prototype.setUrl = function (url) {
        this._url = url;
    };
    URLStore.prototype.serializeParams = function (params) {
        this._params = params;
        return url_1.serialize(params);
    };
    URLStore.prototype.deserializeParams = function (paramName) {
        if (!this._url) {
            throw new Error("URL is not defined!");
        }
        this._params = url_1.getURLParameters(this._url, paramName);
        return this._params;
    };
    URLStore.prototype.deleteUrlParams = function (params) {
        var _a = url_1.deleteURLParameter(this._url, params), path = _a.path, updatedParams = _a.params;
        var pathname = this._url.split("?filter=", 2)[0];
        this._params = updatedParams;
        this._url = pathname + path;
        return path;
    };
    Object.defineProperty(URLStore.prototype, "url", {
        get: function () {
            return this._url;
        },
        set: function (value) {
            this._url = value;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(URLStore.prototype, "params", {
        get: function () {
            return this._params;
        },
        set: function (value) {
            this._params = value;
        },
        enumerable: false,
        configurable: true
    });
    __decorate([
        mobx_1.action("set url"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "setUrl", null);
    __decorate([
        mobx_1.action("serialize params"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Object]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "serializeParams", null);
    __decorate([
        mobx_1.action("deserialize params"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "deserializeParams", null);
    __decorate([
        mobx_1.action("delete params from url"),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [String]),
        __metadata("design:returntype", void 0)
    ], URLStore.prototype, "deleteUrlParams", null);
    return URLStore;
}());
exports.URLStore = URLStore;
