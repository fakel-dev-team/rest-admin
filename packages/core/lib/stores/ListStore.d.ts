import type { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
export declare class ListStore {
    dataSource: Array<any>;
    ids: number[];
    total: number;
    loading: boolean;
    constructor();
    setLoading(loading: boolean): void;
    deleteSelectedRecords(ids: any[]): void;
    deleteRecords(dataProvider: DataProviderT, resource: string, ids: any[]): Promise<void>;
    getData(dataProvider: DataProviderT, resource: string, params?: any): Promise<void>;
    setDataSource: (value: any) => any;
    setTotal: (value: any) => any;
    setDataSourceIds: (value: any) => any;
}
