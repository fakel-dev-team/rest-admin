import { IconType } from "antd/lib/notification";
declare type useNotificationParamsT = {
    message: string;
    description?: string;
    type?: IconType;
    duration?: number;
};
export declare const useNotification: (params: useNotificationParamsT) => () => void;
export {};
