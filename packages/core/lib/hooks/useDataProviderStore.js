"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useDataProviderStore = void 0;
var useStore_1 = require("./useStore");
exports.useDataProviderStore = function () {
    var dataProviderStore = useStore_1.useStore("dataProviderStore");
    return dataProviderStore;
};
