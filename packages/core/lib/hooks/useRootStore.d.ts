import { AdminStore } from "../stores/AdminStore";
export declare const useRootStore: () => AdminStore;
