import { AdminStore } from "../stores/AdminStore";
import { ClassPropsT } from "../selectors/selectorStore";
export declare const useStore: (name: ClassPropsT<AdminStore>) => import("../stores/AuthStore").AuthStore | import("../stores/AuthProviderStore").AuthProviderStore | import("..").ImagesStore | import("..").ShowStore | import("..").URLStore | import("..").ResourceStore | import("..").ListStore | import("..").FiltersStore | import("..").DataProviderStore;
