"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useResourceStore = void 0;
var useStore_1 = require("./useStore");
exports.useResourceStore = function () {
    var resourceStore = useStore_1.useStore("resourceStore");
    return resourceStore;
};
