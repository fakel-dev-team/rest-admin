"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useRootStore = void 0;
var react_1 = require("react");
var StoreProvider_1 = require("../components/StoreProvider");
exports.useRootStore = function () {
    var adminStore = react_1.useContext(StoreProvider_1.AdminContext).adminStore;
    return adminStore;
};
