"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useNotification = void 0;
var antd_1 = require("antd");
exports.useNotification = function (params) {
    var message = params.message, description = params.description, _a = params.type, type = _a === void 0 ? "info" : _a;
    var openNotification = function () {
        antd_1.notification[type]({
            message: message,
            description: description,
            duration: 0,
        });
    };
    return openNotification;
};
