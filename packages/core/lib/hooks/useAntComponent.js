"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) {
            if (e.indexOf(p[i]) < 0 && Object.prototype.propertyIsEnumerable.call(s, p[i]))
                t[p[i]] = s[p[i]];
        }
    return t;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.useAntComponent = void 0;
var react_1 = __importDefault(require("react"));
var formik_1 = require("formik");
var antd_1 = require("antd");
var Text = antd_1.Typography.Text;
exports.useAntComponent = function (params) {
    var Component = params.Component, form = params.form, field = params.field, meta = params.meta;
    var _a = params.props, component = _a.component, componentChildren = _a.componentChildren, options = _a.options, radioButtons = _a.radioButtons, isLink = _a.isLink, showSearch = _a.showSearch, filterSort = _a.filterSort, filterOption = _a.filterOption, optionFilterProp = _a.optionFilterProp, props = __rest(_a, ["component", "componentChildren", "options", "radioButtons", "isLink", "showSearch", "filterSort", "filterOption", "optionFilterProp"]);
    var onChange = function (event) {
        form.setFieldValue(field.name, event.target ? event.target.value : event);
    };
    return (react_1.default.createElement(antd_1.Form.Item, __assign({ initialValue: props.defaultValue, validateStatus: meta.touched && meta.error ? "error" : "success" }, props), componentChildren ? (react_1.default.createElement(Component, __assign({}, props, field, { onBlur: field.onBlur, onChange: props.onChange || onChange }),
        react_1.default.createElement(react_1.default.Fragment, null, componentChildren
            ? componentChildren(options || radioButtons)
            : null))) : (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(Component, __assign({}, props, field, { onBlur: field.onBlur, onChange: props.onChange || onChange })),
        react_1.default.createElement(formik_1.ErrorMessage, { name: props.name }, function (errorMessage) { return react_1.default.createElement(Text, { type: "danger" }, errorMessage); })))));
};
