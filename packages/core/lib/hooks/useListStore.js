"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useListStore = void 0;
var useStore_1 = require("./useStore");
exports.useListStore = function () {
    return useStore_1.useStore("listStore");
};
