"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useStore = void 0;
var selectorStore_1 = require("../selectors/selectorStore");
var react_1 = require("react");
var StoreProvider_1 = require("../components/StoreProvider");
exports.useStore = function (name) {
    var adminStore = react_1.useContext(StoreProvider_1.AdminContext).adminStore;
    var store = selectorStore_1.selectStore(adminStore, name);
    return store;
};
