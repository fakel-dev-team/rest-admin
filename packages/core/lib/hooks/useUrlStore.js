"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useUrlStore = void 0;
var useStore_1 = require("./useStore");
exports.useUrlStore = function () {
    return useStore_1.useStore("urlStore");
};
