"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useShowStore = void 0;
var useStore_1 = require("./useStore");
exports.useShowStore = function () {
    return useStore_1.useStore("showStore");
};
