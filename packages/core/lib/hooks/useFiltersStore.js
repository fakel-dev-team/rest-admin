"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.useFiltersStore = void 0;
var useStore_1 = require("./useStore");
exports.useFiltersStore = function () {
    return useStore_1.useStore("filtersStore");
};
