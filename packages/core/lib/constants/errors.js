"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.GET_MANY_IS_NOT_DEFINED = void 0;
exports.GET_MANY_IS_NOT_DEFINED = "Method getMany() in DataProvider is not defined";
