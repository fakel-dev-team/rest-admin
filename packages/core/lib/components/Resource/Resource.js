"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var mobx_react_1 = require("mobx-react");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var Resource = mobx_react_1.observer(function (resource) {
    var resourceStore = useResourceStore_1.useResourceStore();
    react_1.useEffect(function () {
        resourceStore.pushResource(resource);
    }, []);
    return null;
});
exports.default = Resource;
