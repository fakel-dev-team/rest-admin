"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var react_router_dom_1 = require("react-router-dom");
var mobx_react_1 = require("mobx-react");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var ResourceWithLayout_1 = __importDefault(require("./ResourceWithLayout"));
var ResourceRouter = mobx_react_1.observer(function (props) {
    var resourceStore = useResourceStore_1.useResourceStore();
    var currentLocation = react_router_dom_1.useLocation();
    var currentResource = resourceStore.getCurrentResource() || resourceStore.resources[0];
    react_1.useEffect(function () {
        resourceStore.setCurrentResource(currentLocation.pathname.split("/", 2)[1]);
    }, [currentLocation.pathname]);
    return currentResource ? (react_1.default.createElement(react_router_dom_1.Switch, { key: currentResource.name },
        react_1.default.createElement(ResourceWithLayout_1.default, { exact: true, path: "/" + currentResource.name + "/create", view: currentResource.create }),
        react_1.default.createElement(ResourceWithLayout_1.default, { exact: true, path: "/" + currentResource.name + "/list", view: currentResource.list }),
        react_1.default.createElement(ResourceWithLayout_1.default, { exact: true, path: "/" + currentResource.name + "/show/:id", view: currentResource.show }),
        react_1.default.createElement(ResourceWithLayout_1.default, { exact: true, path: "/" + currentResource.name + "/edit/:id", view: currentResource.edit }))) : null;
});
exports.default = ResourceRouter;
