import React from "react";
import { FormItemProps } from "antd/lib/form";
import { InputProps } from "./Input";
interface SwitchInputProps extends InputProps {
    defaultChecked?: boolean;
}
declare const SwitchInput: React.FC<SwitchInputProps & FormItemProps>;
export default SwitchInput;
