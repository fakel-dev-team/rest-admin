import React from "react";
import { AntFieldProps } from "../../@types";
import { InputProps } from "./Input";
interface AutoCompleteInputProps extends InputProps {
    valuePropName: string;
    titlePropName: string;
    reference: string;
}
declare const AutoCompleteInput: React.FC<AutoCompleteInputProps & AntFieldProps>;
export default AutoCompleteInput;
