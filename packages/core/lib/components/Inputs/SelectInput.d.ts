import React from "react";
import { FormItemProps } from "antd/lib/form";
import { InputProps } from "./Input";
import { OptionT } from "../../@types";
interface SelectInputProps extends InputProps {
    valuePropName?: string;
    titlePropName?: string;
    defaultValue?: string;
    record?: OptionT[];
    disabled?: boolean;
    loading?: boolean;
    mode?: "multiple" | "tags";
    placeholder?: string;
    filter?: (input: any, option: any) => boolean;
    sort?: (optionA: any, optionB: any) => any;
    options?: OptionT[];
}
declare const SelectInput: React.FC<SelectInputProps & FormItemProps>;
export default SelectInput;
