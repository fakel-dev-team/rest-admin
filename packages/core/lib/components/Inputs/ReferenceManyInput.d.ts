import React from "react";
export declare type ReferenceManyInputProps = {
    name: string;
    reference: string;
    label?: string;
    record?: any;
};
declare const ReferenceManyInput: React.FC<ReferenceManyInputProps>;
export default ReferenceManyInput;
