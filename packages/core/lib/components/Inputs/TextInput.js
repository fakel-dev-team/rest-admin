"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var input_1 = __importDefault(require("antd/lib/input"));
var Input_1 = __importDefault(require("./Input"));
var InputField = function (props) {
    return (react_1.default.createElement(Input_1.default, __assign({}, props, { style: { width: "100%" }, component: input_1.default })));
};
exports.default = InputField;
