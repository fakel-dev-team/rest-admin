"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var formik_1 = require("formik");
var useAntComponent_1 = require("../../hooks/useAntComponent");
var typography_1 = __importDefault(require("antd/lib/typography"));
var form_1 = __importDefault(require("antd/lib/form"));
var Text = typography_1.default.Text;
var CoreInput = function (props) {
    return (react_1.default.createElement(formik_1.Field, { name: props.name }, function (_a) {
        var form = _a.form, field = _a.field, meta = _a.meta;
        return props.component ? (useAntComponent_1.useAntComponent({
            Component: props.component,
            ComponentChildren: props.componentChildren,
            form: form,
            field: field,
            meta: meta,
            props: props,
        })) : (react_1.default.createElement(form_1.default.Item, __assign({ initialValue: props.defaultValue, validateStatus: meta.touched && meta.error ? "error" : "success" }, props), props.children(form, field, meta)));
    }));
};
exports.default = CoreInput;
