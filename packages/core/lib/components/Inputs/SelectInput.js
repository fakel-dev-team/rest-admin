"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var select_1 = __importDefault(require("antd/lib/select"));
var Input_1 = __importDefault(require("./Input"));
var Select_1 = require("../../utils/Select");
var Option = select_1.default.Option;
var SelectInput = function (props) {
    var options;
    if (!props.options) {
        options = Select_1.mapRecordsToOptions(props.record, props.valuePropName, props.titlePropName);
    }
    var filter = function (input, option) {
        return option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;
    };
    var sort = function (optionA, optionB) {
        return optionA.children
            .toLowerCase()
            .localeCompare(optionB.children.toLowerCase());
    };
    return (react_1.default.createElement(Input_1.default, { label: props.label, style: { minWidth: 200 }, name: props.name, placeholder: props.placeholder || "Search", options: props.options || options, component: select_1.default, componentChildren: function (options) {
            return options.map(function (option, index) { return (react_1.default.createElement(Option, { key: option.key || index, value: option.value }, option.title)); });
        }, showSearch: true, optionFilterProp: "children", filterOption: props.filter || filter, filterSort: props.sort || sort }));
};
exports.default = SelectInput;
