import React from "react";
import { FormItemProps } from "antd/lib/form";
import { InputProps } from "./Input";
export declare type RadioButtonT = {
    value: any;
    title: string;
    disable?: boolean;
    checked?: boolean;
};
interface RadioGroupInputProps extends InputProps {
    defaultValue?: string;
    radioButtons: RadioButtonT[];
    disabled?: boolean;
}
declare const RadioGroupInput: React.FC<RadioGroupInputProps & FormItemProps>;
export default RadioGroupInput;
