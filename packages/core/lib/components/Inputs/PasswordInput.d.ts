import React from "react";
import { FormItemProps } from "antd/lib/form";
import { InputProps } from "./Input";
interface PasswordInputProps extends InputProps {
    placeholder?: string;
}
declare const PasswordInput: React.FC<PasswordInputProps & FormItemProps>;
export default PasswordInput;
