import React from "react";
declare type EditViewProps = {
    title?: string;
};
declare const EditView: React.FC<EditViewProps>;
export default EditView;
