"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var space_1 = __importDefault(require("antd/lib/space"));
var typography_1 = __importDefault(require("antd/lib/typography"));
var Title = typography_1.default.Title;
var EditView = function (_a) {
    var title = _a.title, children = _a.children;
    var defaultTitle = "Edit form";
    return (react_1.default.createElement(space_1.default, { direction: "vertical" },
        react_1.default.createElement(Title, { level: 4 }, title || defaultTitle),
        children));
};
exports.default = EditView;
