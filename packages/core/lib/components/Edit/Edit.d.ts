import React from "react";
import { RedirectTo } from "../../@types";
/**
 * @component Create
 * @props
 *  id - string (id current resource)
 *  title? - string (Title for create form)
 *  children - (props: any) => any
 *  redirect? - "list" | "show" | "create" | "edit"
 *
 * @example
 * <Edit redirect="list">
      {(editProps) => {
        return (
          <SimpleForm
            {...createProps}
            initialValue={editProps.initialValue}
          >
            <TextInput label="Username" name="username" />
            <TextInput
              label="Email"
              name="email"
              placeholder="Enter email"
            />
          </SimpleForm>
        );
      }}
    </Edit>
 * */
declare type EditProps = {
    id?: string;
    title?: string;
    children: (props?: any) => any;
    redirect?: RedirectTo;
    saveRedirect?: (...any: any[]) => void;
    onSuccessfulSubmit?: () => void;
    onFailedSubmit?: (error?: any) => void;
    handleSubmit?: (value: any, actions: any) => any;
};
declare const Edit: React.FC<EditProps>;
export default Edit;
