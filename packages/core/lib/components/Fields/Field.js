"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var lodash_get_1 = __importDefault(require("lodash.get"));
var Field = function (_a) {
    var record = _a.record, source = _a.source, children = _a.children;
    var value = lodash_get_1.default(record, source);
    return children(value);
};
exports.default = Field;
