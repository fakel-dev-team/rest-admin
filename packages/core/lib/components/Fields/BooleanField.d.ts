import React from "react";
import { FieldProps } from "../../@types/index";
declare type TagField = {
    color?: string;
    icon?: string;
    visible?: boolean;
};
declare const BooleanField: React.FC<FieldProps & TagField>;
export default BooleanField;
