"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var tag_1 = __importDefault(require("antd/lib/tag"));
var Field_1 = __importDefault(require("./Field"));
var TagField = function (props) {
    var source = props.source, record = props.record;
    return (react_1.default.createElement(Field_1.default, { source: source, record: record }, function (value) { return (react_1.default.createElement(tag_1.default, __assign({}, props, { color: "default" }), value)); }));
};
exports.default = TagField;
