"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var mobx_react_1 = require("mobx-react");
var space_1 = __importDefault(require("antd/lib/space"));
var FieldList = mobx_react_1.observer(function (props) {
    return (react_1.default.createElement(space_1.default, { direction: props.direction || "vertical" }, props.records &&
        props.records.map(function (record) {
            return react_1.default.Children.map(props.children, function (child) {
                return react_1.default.cloneElement(child, __assign(__assign({}, props), { record: record }));
            });
        })));
});
exports.default = FieldList;
