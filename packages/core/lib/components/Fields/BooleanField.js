"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var Field_1 = __importDefault(require("./Field"));
var CheckOutlined_1 = __importDefault(require("@ant-design/icons/lib/icons/CheckOutlined"));
var CloseOutlined_1 = __importDefault(require("@ant-design/icons/lib/icons/CloseOutlined"));
var BooleanField = function (props) {
    var source = props.source, record = props.record;
    return (react_1.default.createElement(Field_1.default, { source: source, record: record }, function (value) { return (value ? react_1.default.createElement(CheckOutlined_1.default, null) : react_1.default.createElement(CloseOutlined_1.default, null)); }));
};
exports.default = BooleanField;
