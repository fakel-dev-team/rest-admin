"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var typography_1 = __importDefault(require("antd/lib/typography"));
var Field_1 = __importDefault(require("./Field"));
var Text = typography_1.default.Text;
var useLinkStyle = {
    color: "#1890ff",
};
var TextField = function (_a) {
    var record = _a.record, source = _a.source, style = _a.style, _b = _a.isLink, isLink = _b === void 0 ? false : _b, label = _a.label;
    return (react_1.default.createElement(Field_1.default, { record: record, source: source }, function (value) { return (react_1.default.createElement(react_1.default.Fragment, null,
        label && (react_1.default.createElement(react_1.default.Fragment, null,
            react_1.default.createElement(Text, { type: "secondary" }, label),
            react_1.default.createElement("br", null))),
        react_1.default.createElement(Text, { style: Object.assign({}, style, isLink ? useLinkStyle : null) }, value))); }));
};
exports.default = TextField;
