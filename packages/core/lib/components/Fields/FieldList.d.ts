import React from "react";
import { LinkT } from "../../@types";
declare type FieldListPropsT = {
    link?: LinkT;
    records?: any[];
    direction?: "vertical" | "horizontal";
};
declare const FieldList: React.FC<FieldListPropsT>;
export default FieldList;
