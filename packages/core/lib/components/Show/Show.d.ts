import React from "react";
declare type ShowProps = {
    title?: string | ((record: any) => JSX.Element);
    actions?: JSX.Element;
};
declare const Show: React.FC<ShowProps>;
export default Show;
