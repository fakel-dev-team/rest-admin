"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var useDataProviderStore_1 = require("../../hooks/useDataProviderStore");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var useShowStore_1 = require("../../hooks/useShowStore");
var react_router_dom_1 = require("react-router-dom");
var mobx_react_1 = require("mobx-react");
var LoadingOutlined_1 = __importDefault(require("@ant-design/icons/lib/icons/LoadingOutlined"));
var row_1 = __importDefault(require("antd/lib/row"));
var typography_1 = __importDefault(require("antd/lib/typography"));
var space_1 = __importDefault(require("antd/lib/space"));
var spin_1 = __importDefault(require("antd/lib/spin"));
var ShowActions_1 = __importDefault(require("./ShowActions"));
var Title = typography_1.default.Title;
var Show = mobx_react_1.observer(function (_a) {
    var title = _a.title, actions = _a.actions, children = _a.children;
    var dataProviderStore = useDataProviderStore_1.useDataProviderStore();
    var resourceStore = useResourceStore_1.useResourceStore();
    var showStore = useShowStore_1.useShowStore();
    var _b = react_1.useState(false), loading = _b[0], setLoading = _b[1];
    var id = react_router_dom_1.useParams().id;
    react_1.useEffect(function () {
        setLoading(true);
        if (resourceStore.currentResource) {
            showStore.getData(dataProviderStore.dataProvider, resourceStore.currentResource, { id: id });
            setLoading(false);
        }
    }, [resourceStore.currentResource]);
    return loading ? (react_1.default.createElement("div", { style: {
            width: "100%",
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
        } },
        react_1.default.createElement(spin_1.default, { indicator: react_1.default.createElement(LoadingOutlined_1.default, { style: { fontSize: 24 } }) }))) : (showStore.data && (react_1.default.createElement(react_1.default.Fragment, null,
        react_1.default.createElement(row_1.default, { justify: "space-between" },
            typeof title === "function" ? (title(showStore.data)) : (react_1.default.createElement(Title, { level: 3 }, title)),
            react_1.default.createElement(ShowActions_1.default, { id: id }, actions)),
        react_1.default.createElement(space_1.default, { direction: "vertical" }, react_1.default.Children.map(children, function (child) {
            return react_1.default.cloneElement(child, {
                records: [showStore.data],
            });
        })))));
});
exports.default = Show;
