import React from "react";
import { RedirectTo } from "../../@types";
declare type RedirectProps = {
    to: RedirectTo;
    children: (props?: any) => any;
};
declare const FormWithRedirect: React.FC<RedirectProps>;
export default FormWithRedirect;
