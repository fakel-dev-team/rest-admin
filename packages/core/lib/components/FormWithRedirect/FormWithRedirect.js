"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = require("react");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var FormWithRedirect = function (_a) {
    var to = _a.to, children = _a.children;
    var resourceStore = useResourceStore_1.useResourceStore();
    var _b = react_1.useState("/" + resourceStore.currentResource + "/" + to || ""), redirect = _b[0], setRedirect = _b[1];
    var saveRedirect = function () {
        setRedirect("/" + resourceStore.currentResource + "/" + to);
    };
    return children({ redirect: redirect, saveRedirect: saveRedirect });
};
exports.default = FormWithRedirect;
