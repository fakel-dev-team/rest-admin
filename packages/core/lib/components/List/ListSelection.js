"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var row_1 = __importDefault(require("antd/lib/row"));
var col_1 = __importDefault(require("antd/lib/col"));
var button_1 = __importDefault(require("antd/lib/button"));
var message_1 = __importDefault(require("antd/lib/message"));
var CloseOutlined_1 = __importDefault(require("@ant-design/icons/lib/icons/CloseOutlined"));
var DeleteOutlined_1 = __importDefault(require("@ant-design/icons/lib/icons/DeleteOutlined"));
var useDataProviderStore_1 = require("../../hooks/useDataProviderStore");
var useListStore_1 = require("../../hooks/useListStore");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var ListSelection = function (_a) {
    var selectedRows = _a.selectedRows, actions = _a.actions, setSelectedRows = _a.setSelectedRows, dataSource = _a.dataSource;
    var dataProviderStore = useDataProviderStore_1.useDataProviderStore();
    var resourceStore = useResourceStore_1.useResourceStore();
    var listStore = useListStore_1.useListStore();
    var onClose = function () {
        setSelectedRows([]);
    };
    var onDelete = function () { return __awaiter(void 0, void 0, void 0, function () {
        var ids;
        return __generator(this, function (_a) {
            ids = selectedRows.map(function (id) { return dataSource[id].id; });
            console.log(dataSource);
            listStore.deleteRecords(dataProviderStore.dataProvider, resourceStore.currentResource, ids);
            message_1.default.success("Упешно удалено!");
            setSelectedRows([]);
            return [2 /*return*/];
        });
    }); };
    return selectedRows ? (react_1.default.createElement(row_1.default, { align: "middle", justify: "space-between", style: {
            backgroundColor: "#fff",
            width: "100%",
            padding: 10,
        } },
        react_1.default.createElement(col_1.default, null,
            react_1.default.createElement(button_1.default, { onClick: onClose, icon: react_1.default.createElement(CloseOutlined_1.default, null) })),
        react_1.default.createElement(col_1.default, null,
            "Selected ",
            selectedRows.length,
            " items"),
        react_1.default.createElement(col_1.default, null, actions || (react_1.default.createElement(button_1.default, { onClick: onDelete, type: "primary", danger: true },
            "Delete ",
            react_1.default.createElement(DeleteOutlined_1.default, null)))))) : null;
};
exports.default = ListSelection;
