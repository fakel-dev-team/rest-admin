"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var index_1 = require("./index");
var mobx_react_1 = require("mobx-react");
var mobx_1 = require("mobx");
var space_1 = __importDefault(require("antd/lib/space"));
var col_1 = __importDefault(require("antd/lib/col"));
var row_1 = __importDefault(require("antd/lib/row"));
var useListStore_1 = require("../../hooks/useListStore");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var useDataProviderStore_1 = require("../../hooks/useDataProviderStore");
var useFiltersStore_1 = require("../../hooks/useFiltersStore");
var List_1 = require("../../utils/List");
var List = mobx_react_1.observer(function (_a) {
    var columns = _a.columns, filters = _a.filters, actions = _a.actions, perPage = _a.perPage, view = _a.view;
    var dataProviderStore = useDataProviderStore_1.useDataProviderStore();
    var resourceStore = useResourceStore_1.useResourceStore();
    var listStore = useListStore_1.useListStore();
    var filtersStore = useFiltersStore_1.useFiltersStore();
    var tableColumns = List_1.mapToAntColumn(columns);
    react_1.useEffect(function () {
        listStore.getData(dataProviderStore.dataProvider, resourceStore.currentResource);
    }, [resourceStore.currentResource, filtersStore.displayFilters]);
    return (react_1.default.createElement(space_1.default, { direction: "vertical" },
        react_1.default.createElement(row_1.default, { justify: "space-between", align: "top" },
            react_1.default.createElement(col_1.default, { span: "4" },
                actions || react_1.default.createElement(index_1.DefaultListActions, null),
                " "),
            react_1.default.createElement(col_1.default, { style: { display: "flex", justifyContent: "flex-end" }, span: "20" }, filters)),
        react_1.default.createElement(index_1.ListView, { loading: mobx_1.toJS(listStore.loading), view: view, listStore: listStore, dataSource: mobx_1.toJS(listStore.dataSource), ids: listStore.ids, perPage: perPage || 20, resource: resourceStore.currentResource, columns: tableColumns })));
});
exports.default = List;
