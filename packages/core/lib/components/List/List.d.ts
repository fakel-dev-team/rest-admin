import React from "react";
import { ColumnT } from "../../@types";
declare type ListProps = {
    columns: ColumnT[];
    title?: string;
    actions?: JSX.Element;
    filters?: JSX.Element;
    perPage?: number;
    view?: "edit" | "show";
};
declare const List: React.FC<ListProps>;
export default List;
