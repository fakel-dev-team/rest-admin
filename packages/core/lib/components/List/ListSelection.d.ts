import React from "react";
declare type ListSelectionProps = {
    selectedRows: number[];
    setSelectedRows: (...any: any[]) => any;
    actions?: React.ReactNode;
    dataSource: any;
};
declare const ListSelection: React.FC<ListSelectionProps>;
export default ListSelection;
