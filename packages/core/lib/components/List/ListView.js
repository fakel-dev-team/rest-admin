"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importStar(require("react"));
var react_router_dom_1 = require("react-router-dom");
var table_1 = __importDefault(require("antd/lib/table"));
var index_1 = require("./index");
var mobx_react_1 = require("mobx-react");
var hooks_1 = require("../../hooks");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var useListStore_1 = require("../../hooks/useListStore");
var ListView = mobx_react_1.observer(function (props) {
    var dataSource = props.dataSource, columns = props.columns, resource = props.resource, perPage = props.perPage, page = props.page, loading = props.loading, view = props.view;
    var dataProviderStore = hooks_1.useDataProviderStore();
    var listStore = useListStore_1.useListStore();
    var resourceStore = useResourceStore_1.useResourceStore();
    var history = react_router_dom_1.useHistory();
    var _a = react_1.useState([]), selectedRows = _a[0], setSelectedRows = _a[1];
    var onSelect = function (selectedRowsIds) {
        setSelectedRows(__spreadArrays(selectedRowsIds));
    };
    var onRow = function (record) {
        if (!record.id) {
            console.warn("WARNING: Record must have an id parameter");
        }
        return {
            onClick: function () {
                history.push("/" + resource + "/" + (view ? view : "edit") + "/" + record.id);
            },
        };
    };
    var onPageChange = function (page, pageSize) {
        listStore.getData(dataProviderStore.dataProvider, resourceStore.currentResource, { page: page, pageSize: pageSize });
    };
    return (react_1.default.createElement(react_1.default.Fragment, null,
        selectedRows && selectedRows.length ? (react_1.default.createElement(index_1.ListSelection, { dataSource: dataSource, setSelectedRows: setSelectedRows, selectedRows: selectedRows })) : null,
        react_1.default.createElement(table_1.default, { loading: loading, style: { minWidth: "80vw" }, size: "middle", onRow: onRow, rowSelection: {
                type: "checkbox",
                selectedRowKeys: selectedRows,
                onChange: onSelect,
            }, dataSource: dataSource, columns: columns, pagination: {
                showSizeChanger: true,
                total: listStore.total,
                defaultPageSize: perPage,
                defaultCurrent: page,
                onChange: onPageChange,
            } })));
});
exports.default = ListView;
