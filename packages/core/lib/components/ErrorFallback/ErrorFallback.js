"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var result_1 = __importDefault(require("antd/lib/result"));
var button_1 = __importDefault(require("antd/lib/button"));
var ErrorFallback = function (_a) {
    var message = _a.message, description = _a.description, resetErrorBoundary = _a.resetErrorBoundary;
    return (react_1.default.createElement("div", { style: {
            height: "100%",
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
        } },
        react_1.default.createElement(result_1.default, { status: "error", title: "Something went wrong:", subTitle: message, extra: [
                react_1.default.createElement(button_1.default, { key: "reset", onClick: resetErrorBoundary }, "Reset"),
            ] })));
};
exports.default = ErrorFallback;
