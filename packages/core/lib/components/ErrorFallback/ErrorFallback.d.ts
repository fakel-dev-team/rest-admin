import React from "react";
declare type ErrorFallbackProps = {
    message?: string;
    description?: string;
    resetErrorBoundary?: any;
};
declare const ErrorFallback: React.FC<ErrorFallbackProps>;
export default ErrorFallback;
