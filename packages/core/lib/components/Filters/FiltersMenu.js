"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var menu_1 = __importDefault(require("antd/lib/menu"));
var mobx_react_1 = require("mobx-react");
var FiltersMenu = mobx_react_1.observer(function (_a) {
    var setDisplayFilters = _a.setDisplayFilters, setFilters = _a.setFilters, filters = _a.filters;
    var handleMenuClick = function (filter) {
        var displayFilter = filter.DisplayFilterComponent;
        setDisplayFilters(function (displayFilters) {
            var isExist = displayFilters.find(function (dF) { return dF === displayFilter; });
            if (!isExist) {
                return __spreadArrays(displayFilters, [displayFilter]);
            }
            return displayFilters;
        });
        setFilters(function (filters) { return filters.filter(function (_filter) { return _filter !== filter; }); });
    };
    return (react_1.default.createElement(menu_1.default, null, filters.map(function (filter, index) { return (react_1.default.createElement(menu_1.default.Item, { onClick: function () { return handleMenuClick(filter); }, key: index }, filter.label)); })));
});
exports.default = FiltersMenu;
