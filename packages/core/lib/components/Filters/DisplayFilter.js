"use strict";
var __spreadArrays = (this && this.__spreadArrays) || function () {
    for (var s = 0, i = 0, il = arguments.length; i < il; i++) s += arguments[i].length;
    for (var r = Array(s), k = 0, i = 0; i < il; i++)
        for (var a = arguments[i], j = 0, jl = a.length; j < jl; j++, k++)
            r[k] = a[j];
    return r;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var col_1 = __importDefault(require("antd/lib/col"));
var button_1 = __importDefault(require("antd/lib/button"));
var CloseOutlined_1 = __importDefault(require("@ant-design/icons/lib/icons/CloseOutlined"));
var DisplayFilter = function (_a) {
    var displayFilter = _a.displayFilter, setDisplayFilters = _a.setDisplayFilters, setFilters = _a.setFilters, filtersStore = _a.filtersStore;
    var handleDeleteButton = function (displayFilter, source) {
        setDisplayFilters(function (displayFilters) {
            return displayFilters.filter(function (_displayFilter) { return _displayFilter !== displayFilter; });
        });
        setFilters(function (filters) {
            var isExist = filters.find(function (filter) { return filter.source === source; });
            if (!isExist) {
                return __spreadArrays(filters, [filtersStore.getFilter(source)]);
            }
            return filters;
        });
    };
    return (react_1.default.createElement(col_1.default, { span: "8", style: {
            display: "flex",
            alignItems: "center",
        } },
        react_1.default.createElement(button_1.default, { onClick: function () {
                return handleDeleteButton(displayFilter, displayFilter.props.name);
            }, size: "small", type: "ghost", shape: "circle", style: { marginRight: 10 }, icon: react_1.default.createElement(CloseOutlined_1.default, { style: { fontSize: 12 } }) }),
        displayFilter));
};
exports.default = DisplayFilter;
