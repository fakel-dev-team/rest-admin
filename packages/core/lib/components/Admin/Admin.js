"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_router_dom_1 = require("react-router-dom");
var index_1 = require("./index");
var Admin = function (_a) {
    var dataProvider = _a.dataProvider, children = _a.children, authProvider = _a.authProvider, userView = _a.userView, logoTitle = _a.logoTitle, defaultResource = _a.defaultResource;
    return (react_1.default.createElement(react_router_dom_1.HashRouter, { hashType: "slash" },
        react_1.default.createElement(index_1.AdminStoreProvider, { authProvider: authProvider, dataProvider: dataProvider },
            children,
            react_1.default.createElement(index_1.AdminRouter, null))));
};
exports.default = Admin;
