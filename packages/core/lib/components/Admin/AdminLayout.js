"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var layout_1 = __importDefault(require("antd/lib/layout"));
var typography_1 = __importDefault(require("antd/lib/typography"));
var row_1 = __importDefault(require("antd/lib/row"));
var col_1 = __importDefault(require("antd/lib/col"));
var index_1 = require("../index");
var react_error_boundary_1 = require("react-error-boundary");
var ErrorFallback_1 = __importDefault(require("../ErrorFallback/ErrorFallback"));
var mobx_react_1 = require("mobx-react");
var useAuthStore_1 = require("../../hooks/useAuthStore");
var Header = layout_1.default.Header, Content = layout_1.default.Content, Sider = layout_1.default.Sider;
var Title = typography_1.default.Title, AntLink = typography_1.default.Link;
var AdminLayout = mobx_react_1.observer(function (_a) {
    var children = _a.children, logoTitle = _a.logoTitle, userView = _a.userView;
    // const { user } = useGetMe();
    // console.log(user);
    var authStore = useAuthStore_1.useAuthStore();
    return (react_1.default.createElement(layout_1.default, { style: { minHeight: "100vh" } },
        react_1.default.createElement(Header, null,
            react_1.default.createElement(row_1.default, { justify: "space-between", align: "middle" },
                react_1.default.createElement(col_1.default, { span: 6 },
                    react_1.default.createElement(AntLink, { href: "/" },
                        react_1.default.createElement(Title, { className: "header__logo", level: 3, style: { color: "#fff", marginBottom: 0 } }, logoTitle || "Admin"))))),
        react_1.default.createElement(layout_1.default, null,
            react_1.default.createElement(Sider, { width: 200, className: "site-layout-background" },
                react_1.default.createElement(index_1.Menu, null)),
            react_1.default.createElement(layout_1.default, { style: { padding: "20px" } },
                react_1.default.createElement(react_error_boundary_1.ErrorBoundary, { fallbackRender: function (_a) {
                        var error = _a.error, resetErrorBoundary = _a.resetErrorBoundary;
                        return (react_1.default.createElement(ErrorFallback_1.default, { message: error.message, resetErrorBoundary: resetErrorBoundary }));
                    } },
                    react_1.default.createElement(Content, { className: "site-layout-background" }, children))))));
});
exports.default = AdminLayout;
