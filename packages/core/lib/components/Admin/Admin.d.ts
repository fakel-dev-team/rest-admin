import React from 'react';
import { DataProviderT } from '@fakel-dev-team/rest-admin-simple-rest';
import { AuthProvider } from '@fakel-dev-team/rest-admin-simple-auth';
/**
 * @component Admin
 * @props
 *  dataProvider - DataProviderT
 *
 * @example
 *  <Admin dataProvider={dataProvider}>
      <Resource
        name="posts"
        edit={PostsEdit}
        create={PostsCreate}
        list={PostView}
        show={PostShow}
        options={{ label: "Posts", icon: <BookOutlined /> }}
      />
      ...
    </Admin>
 * */
declare type AdminPropsT = {
    dataProvider: DataProviderT;
    authProvider?: AuthProvider;
    logoTitle?: string;
    userView?: React.ReactNode;
    defaultResource?: string;
};
declare const Admin: React.FC<AdminPropsT>;
export default Admin;
