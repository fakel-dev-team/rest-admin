import React from "react";
import { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { AuthProvider } from "@fakel-dev-team/rest-admin-simple-auth";
declare type AdminStoreProviderProps = {
    dataProvider: DataProviderT;
    authProvider?: AuthProvider;
};
declare const AdminStoreProvider: React.FC<AdminStoreProviderProps>;
export default AdminStoreProvider;
