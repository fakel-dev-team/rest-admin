"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var AdminStore_1 = require("../../stores/AdminStore");
var StoreProvider_1 = __importDefault(require("../StoreProvider"));
var AdminStoreProvider = function (_a) {
    var dataProvider = _a.dataProvider, authProvider = _a.authProvider, children = _a.children;
    var adminStore = new AdminStore_1.AdminStore(dataProvider, authProvider);
    return react_1.default.createElement(StoreProvider_1.default, { adminStore: adminStore }, children);
};
exports.default = AdminStoreProvider;
