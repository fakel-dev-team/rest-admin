"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var index_1 = require("./index");
var space_1 = __importDefault(require("antd/lib/space"));
var antd_1 = require("antd");
var SimpleForm = function (props) {
    return props.initialValue ? (react_1.default.createElement(index_1.Form, __assign({ handleSubmit: props.handleSubmit, initialValue: props.initialValue }, props),
        react_1.default.createElement(space_1.default, { direction: "vertical" },
            props.children,
            props.actions || (react_1.default.createElement(antd_1.Button, { type: "primary", htmlType: "submit" }, props.buttonText || "Save"))))) : null;
};
exports.default = SimpleForm;
