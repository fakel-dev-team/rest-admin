"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var formik_1 = require("formik");
var form_1 = __importDefault(require("antd/lib/form"));
var AppForm = function (props) {
    var id = props.id, validate = props.validate, initialValue = props.initialValue, validationSchema = props.validationSchema, children = props.children, handleSubmit = props.handleSubmit;
    return (react_1.default.createElement(formik_1.Formik, { validationSchema: validationSchema, initialValues: initialValue, onSubmit: handleSubmit }, function (formikProps) {
        return (react_1.default.createElement(form_1.default, { id: id, layout: "vertical", onFinish: formikProps.handleSubmit }, children));
    }));
};
exports.default = AppForm;
