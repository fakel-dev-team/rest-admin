"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var menu_1 = __importDefault(require("antd/lib/menu"));
var mobx_react_1 = require("mobx-react");
var react_router_dom_1 = require("react-router-dom");
var useResourceStore_1 = require("../../hooks/useResourceStore");
var isDefinedOptions = function (resource) { return !!resource.options; };
var getRenderView = function (resource) {
    if (isDefinedOptions(resource)) {
        return resource.options.renderViewOnMenuClicked || "list";
    }
    return "list";
};
var getLinkUrl = function (resource) {
    var DEFAULT_URL = "/" + resource.name + "/" + getRenderView(resource);
    if (isDefinedOptions(resource)) {
        return resource.options.linkUrl || DEFAULT_URL;
    }
    return DEFAULT_URL;
};
var getLabel = function (resource) {
    if (isDefinedOptions(resource)) {
        return resource.options.label || resource.name;
    }
    return resource.name;
};
var AppMenu = mobx_react_1.observer(function () {
    var resourceStore = useResourceStore_1.useResourceStore();
    var resources = resourceStore.resources;
    return resources && resources.length ? (react_1.default.createElement(menu_1.default, { mode: "inline", style: { height: "100%", borderRight: 0 } }, resources.map(function (resource, index) {
        return (react_1.default.createElement(menu_1.default.Item, { icon: resource.options ? resource.options.icon : null, key: index },
            react_1.default.createElement(react_router_dom_1.Link, { to: getLinkUrl(resource) }, getLabel(resource))));
    }))) : null;
});
exports.default = AppMenu;
