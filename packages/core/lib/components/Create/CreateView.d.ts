import React from "react";
declare type CreateViewProps = {
    actions?: JSX.Element;
    title?: string;
};
declare const CreateView: React.FC<CreateViewProps>;
export default CreateView;
