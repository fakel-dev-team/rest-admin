"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var space_1 = __importDefault(require("antd/lib/space"));
var typography_1 = __importDefault(require("antd/lib/typography"));
var row_1 = __importDefault(require("antd/lib/row"));
var Title = typography_1.default.Title;
var CreateView = function (_a) {
    var actions = _a.actions, title = _a.title, children = _a.children;
    var defaultTitle = "Create form";
    return (react_1.default.createElement(space_1.default, { direction: "vertical" },
        react_1.default.createElement(row_1.default, { justify: "space-between", align: "middle" },
            react_1.default.createElement(Title, { level: 4 }, title || defaultTitle),
            actions),
        children));
};
exports.default = CreateView;
