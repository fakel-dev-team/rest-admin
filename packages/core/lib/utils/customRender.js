"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.customRender = void 0;
var react_1 = __importDefault(require("react"));
var react_2 = require("@testing-library/react");
var StoreProvider_1 = __importDefault(require("../components/StoreProvider"));
var mock_1 = require("../mock");
exports.customRender = function (ui) {
    var _a = react_2.render(react_1.default.createElement(StoreProvider_1.default, { adminStore: mock_1.mockAdminStore }, ui)), container = _a.container, asFragment = _a.asFragment;
    return { asFragment: asFragment, container: container };
};
