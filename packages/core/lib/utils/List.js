"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapToAntColumn = exports.getSorter = void 0;
var react_1 = __importDefault(require("react"));
exports.getSorter = function (source) {
    return function (a, b) {
        var aValue = "" + a[source];
        var bValue = "" + b[source];
        return aValue.localeCompare(bValue);
    };
};
exports.mapToAntColumn = function (columns) {
    return columns.map(function (Column, index) { return ({
        title: Column.title,
        dataIndex: Column.source,
        key: Column.id || index,
        sortDirections: Column.sortDirections ? Column.sortDirections : null,
        sorter: Column.sortDirections
            ? Column.sorter || exports.getSorter(Column.source)
            : null,
        render: function (_, record, index) {
            return (react_1.default.createElement(Column.Field, { key: Column.source + "-" + index, record: record, source: Column.source, reference: Column.reference, link: Column.link }, Column.FieldChildren ? Column.FieldChildren() : null));
        },
    }); });
};
