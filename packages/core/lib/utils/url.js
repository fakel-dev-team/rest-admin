"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.serialize = exports.getURLParameters = exports.deleteURLParameter = void 0;
var url_1 = require("../constants/url");
exports.deleteURLParameter = function (url, parameter) {
    var params = exports.getURLParameters(url, parameter);
    delete params[parameter];
    if (!Object.keys(params).length) {
        return {
            path: "",
            params: {},
        };
    }
    if (Object.keys(params).length >= 1) {
        return { path: "?" + url_1.FILTER_PARAMS_NAME + "=" + exports.serialize(params), params: params };
    }
};
exports.getURLParameters = function (url, paramName) {
    if (url) {
        var result_1 = {};
        var urlParams = decodeURI(url.split("?")[1]);
        if (urlParams) {
            var sURLVariables = urlParams.split("&");
            sURLVariables.forEach(function (el) {
                var sParameterName = el.split("=");
                result_1[sParameterName[0]] = sParameterName[1];
            });
        }
        return result_1;
    }
};
exports.serialize = function (params) {
    var str = [];
    for (var param in params)
        if (params.hasOwnProperty(param)) {
            if (params[param]) {
                str.push(param + "=" + params[param]);
            }
        }
    return str.join("&");
};
