"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mapRecordsToOptions = void 0;
var lodash_get_1 = __importDefault(require("lodash.get"));
exports.mapRecordsToOptions = function (records, source, titlePropName) {
    return records.map(function (record, index) {
        var value = lodash_get_1.default(record, source);
        var title = lodash_get_1.default(record, titlePropName);
        return {
            key: record.id || index,
            value: value,
            title: title,
        };
    });
};
