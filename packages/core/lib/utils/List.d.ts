import { ColumnT } from "../@types";
import { ColumnType } from "antd/lib/table";
export declare const getSorter: (source: string) => (a: any, b: any) => number;
export declare const mapToAntColumn: (columns: ColumnT[]) => Array<ColumnType<any>>;
