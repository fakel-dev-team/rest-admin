import { ResourceStore } from "./../stores/ResorceStore";
import { ResourceT } from "../stores/ResorceStore";

describe("Resource Store", () => {
  let resourceStore: ResourceStore;

  beforeAll(() => {
    resourceStore = new ResourceStore();
  });

  afterAll(() => {
    resourceStore.resources = [];
  });

  test("should be push resource to array", () => {
    const resource: ResourceT = {
      name: "posts",
    };
    resourceStore.pushResource(resource);
    const resources = resourceStore.resources;
    expect(resources).toHaveLength(1);
  });

  test("getResource() should be return resource with specific name", () => {
    const name = "posts";
    const resource = resourceStore.getResource(name);
    expect(resource.name).toBe(name);
  });

  test("getResource() should be return undefiend", () => {
    const name = "tasks";
    const resource = resourceStore.getResource(name);
    expect(resource).toBeUndefined();
  });
});
