"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_hooks_1 = require("@testing-library/react-hooks");
var useRootStore_1 = require("../hooks/useRootStore");
var react_1 = __importDefault(require("react"));
var StoreProvider_1 = __importDefault(require("../components/StoreProvider"));
var DataProviderStore_1 = require("./../stores/DataProviderStore");
var adminStore_1 = require("../mock/adminStore");
var AdminStore_1 = require("../stores/AdminStore");
var renderWrapper = function () {
    var wrapper = function (_a) {
        var children = _a.children, adminStore = _a.adminStore;
        return (react_1.default.createElement(StoreProvider_1.default, { adminStore: adminStore }, children));
    };
    return wrapper;
};
var initialProps = {
    adminStore: adminStore_1.mockAdminStore,
};
var beforeUseRootStore = function () {
    var result = react_hooks_1.renderHook(function () { return useRootStore_1.useRootStore(); }, {
        wrapper: renderWrapper(),
        initialProps: initialProps,
    }).result;
    return result;
};
describe("useRootStore hook", function () {
    var result = null;
    beforeAll(function () {
        result = beforeUseRootStore();
    });
    afterAll(function () {
        cleanup();
    });
    test("should not be return null", function () {
        expect(result.current).not.toBeNull();
    });
    test("should be return instance of AdminStore", function () {
        expect(result.current).toBeInstanceOf(AdminStore_1.AdminStore);
    });
    test("should not be return instance of DataProviderStore", function () {
        expect(result.current).not.toBeInstanceOf(DataProviderStore_1.DataProviderStore);
    });
});
