"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_hooks_1 = require("@testing-library/react-hooks");
var useStore_1 = require("../hooks/useStore");
var StoreProvider_1 = __importDefault(require("./../components/StoreProvider"));
var DataProviderStore_1 = require("../stores/DataProviderStore");
var adminStore_1 = require("../mock/adminStore");
var renderWrapper = function () {
    var wrapper = function (_a) {
        var children = _a.children, adminStore = _a.adminStore;
        return (react_1.default.createElement(StoreProvider_1.default, { adminStore: adminStore }, children));
    };
    return wrapper;
};
var initialProps = {
    adminStore: adminStore_1.mockAdminStore,
};
function beforeUseStore(name) {
    var result = react_hooks_1.renderHook(function () { return useStore_1.useStore(name); }, {
        wrapper: renderWrapper(),
        initialProps: initialProps,
    }).result;
    return result;
}
describe("useStore hook", function () {
    afterAll(function () {
        react_hooks_1.cleanup();
    });
    test("should not be return null", function () {
        var result = beforeUseStore("dataProviderStore");
        expect(result.current).not.toBeNull();
    });
    test("should be return instance of DataProviderStore", function () {
        var result = beforeUseStore("dataProviderStore");
        expect(result.current).toBeInstanceOf(DataProviderStore_1.DataProviderStore);
    });
    test("should not be return instance of DataProviderStore", function () {
        var result = beforeUseStore("formStore");
        expect(result.current).not.toBeInstanceOf(DataProviderStore_1.DataProviderStore);
    });
});
