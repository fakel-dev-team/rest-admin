import React from "react";

import { renderHook, cleanup } from "@testing-library/react-hooks";
import { useStore } from "../hooks/useStore";

import StoreProvider from "./../components/StoreProvider";
import { DataProviderStore } from "../stores/DataProviderStore";

import { mockAdminStore } from "../mock/adminStore";

const renderWrapper = () => {
  const wrapper = ({ children, adminStore }) => (
    <StoreProvider adminStore={adminStore}>{children}</StoreProvider>
  );

  return wrapper;
};

const initialProps = {
  adminStore: mockAdminStore,
};

function beforeUseStore(name) {
  const { result } = renderHook(() => useStore(name), {
    wrapper: renderWrapper(),
    initialProps,
  });

  return result;
}

describe("useStore hook", () => {
  afterAll(() => {
    cleanup();
  });

  test("should not be return null", () => {
    const result = beforeUseStore("dataProviderStore");
    expect(result.current).not.toBeNull();
  });

  test("should be return instance of DataProviderStore", () => {
    const result = beforeUseStore("dataProviderStore");
    expect(result.current).toBeInstanceOf(DataProviderStore);
  });

  test("should not be return instance of DataProviderStore", () => {
    const result = beforeUseStore("formStore");
    expect(result.current).not.toBeInstanceOf(DataProviderStore);
  });
});
