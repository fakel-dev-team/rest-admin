import { renderHook } from "@testing-library/react-hooks";
import { useRootStore } from "../hooks/useRootStore";

import React from "react";
import StoreProvider from "../components/StoreProvider";
import { DataProviderStore } from "./../stores/DataProviderStore";
import { mockAdminStore } from "../mock/adminStore";

import { AdminStore } from "../stores/AdminStore";

const renderWrapper = () => {
  const wrapper = ({ children, adminStore }) => (
    <StoreProvider adminStore={adminStore}>{children}</StoreProvider>
  );

  return wrapper;
};

const initialProps = {
  adminStore: mockAdminStore,
};

const beforeUseRootStore = () => {
  const { result } = renderHook(() => useRootStore(), {
    wrapper: renderWrapper(),
    initialProps,
  });

  return result;
};

describe("useRootStore hook", () => {
  let result = null;

  beforeAll(() => {
    result = beforeUseRootStore();
  });

  afterAll(() => {
    cleanup();
  });

  test("should not be return null", () => {
    expect(result.current).not.toBeNull();
  });
  test("should be return instance of AdminStore", () => {
    expect(result.current).toBeInstanceOf(AdminStore);
  });
  test("should not be return instance of DataProviderStore", () => {
    expect(result.current).not.toBeInstanceOf(DataProviderStore);
  });
});
