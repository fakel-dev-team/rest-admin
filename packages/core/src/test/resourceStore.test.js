import { ResourceStore } from "../stores/ResorceStore";
import CreateView from "../components/PostView";
import { cleanup } from "@testing-library/react";

describe("Resource Store", () => {
  let resourceStore;

  beforeAll(() => {
    resourceStore = new ResourceStore();
  });

  afterAll(() => {
    resourceStore.resources = [];
    cleanup();
  });

  test("should be push resource to array", () => {
    const resource = {
      name: "posts",
      create: CreateView,
    };
    resourceStore.pushResource(resource);
    const resources = resourceStore.resources;
    expect(resources).toHaveLength(1);
  });

  test("getResource() should be return resource with specific name", () => {
    const name = "posts";
    const resource = resourceStore.getResource(name);
    expect(resource.name).toBe(name);
  });

  test("getResource() should be return undefiend", () => {
    const name = "tasks";
    const resource = resourceStore.getResource(name);
    expect(resource).toBeUndefined();
  });
});
