import { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { AdminStore } from "../stores/AdminStore";

export const mockDataProvider: DataProviderT = {
  getList: (resource) => {
    return Promise.resolve({ errors: null, total: 0, data: null });
  },
  getOne: (resource) => {
    return Promise.resolve({ errors: null, data: null });
  },
  create: (resource) => {
    return Promise.resolve({ errors: null, data: null });
  },
  update: (resource) => {
    return Promise.resolve({ errors: null, data: null });
  },
  delete: (resource) => {
    return Promise.resolve({ errors: null, data: null });
  },
};

export const mockAdminStore = new AdminStore(mockDataProvider);
