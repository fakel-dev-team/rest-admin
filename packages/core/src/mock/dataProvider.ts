import { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";

export const mockDataProvider = () => ({
  getList: (resource, params) => {},
  getOne: (resource, params) => {},
  create: (resource, params) => {},
  update: (resource, params) => {},
  delete: (resource, params) => {},
});
