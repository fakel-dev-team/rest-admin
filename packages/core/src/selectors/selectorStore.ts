export type ClassPropsT<T> = {
  [K in keyof T]: T[K] extends Function ? never : K;
}[keyof T];

export const selectStore = <SourceStoreT>(
  store: SourceStoreT,
  name: ClassPropsT<SourceStoreT>
) => {
  return store[name];
};
