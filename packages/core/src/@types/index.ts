import { FormItemProps } from "antd/lib/form";
import { InputProps } from "antd/lib/input";
import { SelectProps } from "antd/lib/select";
import { DatePickerProps } from "antd/lib/date-picker";
import { InputNumberProps } from "antd/lib/input-number";
import React, { Key } from "react";

export type LinkT = "edit" | "show" | "list" | "create";

export type FieldProps = {
  source: string;
  label?: string;
  record?: string;
  reference?: string;
};

import { FieldConfig } from "formik";

export interface FormFieldProps extends FieldConfig {
  component?: any;
}

export type RedirectTo = "create" | "list" | "edit" | "show";

export type AntFieldProps = FormItemProps<any> &
  InputProps &
  (SelectProps<any> | DatePickerProps | InputNumberProps);

type SortOrder = "descend" | "ascend";

export type ColumnT = {
  id?: string;
  title: string;
  source: string;
  Field: any;
  FieldChildren?: () => React.ReactNode;
  reference?: string;
  link?: string;
  sortDirections?: SortOrder[];
  sorter?: (a: any, b: any) => any;
};

export type OptionT = {
  key?: Key;
  disabled?: boolean;
  value: Key;
  title?: string;
  className?: string;
  style?: React.CSSProperties;
  label?: React.ReactNode;
};
