import type { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { makeAutoObservable } from "mobx";

export class DataProviderStore {
  private provider: DataProviderT;

  constructor(dataProvider: DataProviderT) {
    makeAutoObservable(this);
    this.provider = dataProvider;
  }

  get dataProvider() {
    return this.provider;
  }
}
