import { serialize, getURLParameters, deleteURLParameter } from "../utils/url";
import { makeAutoObservable, computed, observable, action } from "mobx";

import type { History } from "history";
import { useHistory } from "react-router-dom";

type LocationT = {
  key: string;
  pathname: string;
  search: string;
  hash: string;
};

export class URLStore {
  private _url: string;
  private _params: any;

  constructor() {
    makeAutoObservable(this);
    this._url = "";
    this._params = null;
  }

  @action("set url")
  public setUrl(url: string) {
    this._url = url;
  }

  @action("serialize params")
  public serializeParams(params: any) {
    this._params = params;
    return serialize(params);
  }

  @action("deserialize params")
  public deserializeParams(paramName: string) {
    if (!this._url) {
      throw new Error("URL is not defined!");
    }
    this._params = getURLParameters(this._url, paramName);
    return this._params;
  }

  @action("delete params from url")
  public deleteUrlParams(params: string) {
    const { path, params: updatedParams } = deleteURLParameter(
      this._url,
      params
    );

    const pathname = this._url.split("?filter=", 2)[0];

    this._params = updatedParams;
    this._url = pathname + path;

    return path;
  }

  get url() {
    return this._url;
  }

  set url(value: string) {
    this._url = value;
  }

  get params() {
    return this._params;
  }

  set params(value: any) {
    this._params = value;
  }
}
