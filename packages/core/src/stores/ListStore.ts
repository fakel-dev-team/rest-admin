import type { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { ResourceT } from "./ResorceStore";
import {
  makeAutoObservable,
  computed,
  observable,
  action,
  runInAction,
  makeObservable,
} from "mobx";

export class ListStore {
  dataSource: Array<any> = [];
  ids: number[] = [];
  total: number = 0;
  loading: boolean = false;

  constructor() {
    makeObservable(this, {
      dataSource: observable,
      ids: observable,
      total: observable,
      loading: observable,
    });
  }

  @action("set loading")
  public setLoading(loading: boolean) {
    this.loading = loading;
  }

  @action("delete selected records")
  public deleteSelectedRecords(ids: any[]) {
    console.log(ids);
    this.dataSource = this.dataSource.filter((record, index) => {
      return !ids.includes(record.id);
    });
  }

  @action("delete records")
  public async deleteRecords(
    dataProvider: DataProviderT,
    resource: string,
    ids: any[]
  ) {
    console.log(ids);
    this.deleteSelectedRecords(ids);

    const { data: updatedData } = await dataProvider.delete(resource, {
      id: ids,
    });

    this.dataSource = updatedData;
  }

  @action("get list data")
  public async getData(
    dataProvider: DataProviderT,
    resource: string,
    params: any = {}
  ) {
    this.setLoading(true);
    const { data: records, total } = await dataProvider.getList(
      resource,
      params
    );

    const ids = records.map((_, index) => index);
    const dataWithKey = records.map((dataItem, index) => ({
      ...dataItem,
      key: dataItem.id || index,
    }));
    this.setDataSource(dataWithKey);
    this.setDataSourceIds(ids);
    this.setTotal(total);
    this.setLoading(false);
  }

  @action("set data source")
  public setDataSource = (value) => (this.dataSource = value);

  @action("set total")
  public setTotal = (value) => (this.total = value);

  @action("set data source ids")
  public setDataSourceIds = (value) => (this.ids = value);
}
