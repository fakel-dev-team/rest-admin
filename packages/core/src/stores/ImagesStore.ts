import { observable, action, runInAction, makeObservable } from "mobx";

export type ImageT = {
  id?: string | number;
  src: string;
  name?: string;
  isSelected?: boolean;
};

export class ImagesStore {
  images: ImageT[] = [];
  total: number = 0;

  constructor() {
    makeObservable(this, {
      images: observable,
      total: observable,
    });
  }

  @action("set images")
  public setImages(images: ImageT[], total: number) {
    this.images = images.map((image) => ({ ...image, isSelected: false }));
    this.images.sort((a, b) => +a.isSelected - +b.isSelected);
    this.total = total;
  }

  @action("push image")
  public pushImage(image: ImageT) {
    this.images.push(image);
  }

  @action("set selected images")
  public setIsSelected(image: ImageT, isSelected: boolean) {
    this.images = this.images.map((img) => {
      if (img.id === image.id) {
        return {
          ...img,
          isSelected,
        };
      }

      return img;
    });
  }

  @action("clear selected images")
  public clearSelected() {
    this.images = this.images.map((image) => ({ ...image, isSelected: false }));
  }

  @action("select all")
  public toggleSelectAll() {
    this.images = this.images.map((image) => ({
      ...image,
      isSelected: !image.isSelected,
    }));
  }

  @action("set total")
  public setTotal(total: number) {
    this.total = total;
  }
}
