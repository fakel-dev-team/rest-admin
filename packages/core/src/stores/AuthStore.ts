import { action, makeObservable, observable, computed, runInAction, makeAutoObservable } from "mobx";
export class AuthStore {
  user: any = null;
  isAuth: boolean = null;

  constructor() {
    makeObservable(this, {
      user: observable,
      isAuth: observable,
      setUser: action,
      setIsAuth: action
    });
  }
 
  @action("set user")
  public setUser(user: any) {
    this.user = user;
  }

  @action("set isAuth")
  public setIsAuth(isAuth: boolean) {
    this.isAuth = isAuth;
  }
}
