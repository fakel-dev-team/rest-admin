import type { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { makeAutoObservable, runInAction } from "mobx";

export class ShowStore {
  private _data: any;
  constructor() {
    makeAutoObservable(this);
    this._data = null;
  }

  public async getData(
    dataProvider: DataProviderT,
    resource: string,
    params = {}
  ) {
    const { data } = await dataProvider.getOne(resource, params);

    runInAction(() => {
      this._data = data;
    });
  }

  set data(value: any) {
    this._data = value;
  }

  get data() {
    return this._data;
  }
}
