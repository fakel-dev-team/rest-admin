import {
  makeAutoObservable,
  computed,
  observable,
  action,
  autorun,
  makeObservable,
} from "mobx";

export type Filter = {
  source: string;
  value: string;
  label: string;
  DisplayFilterComponent: any;
};

export class FiltersStore {
  filters: Filter[] = [];
  displayFilters: Array<string> = [];
  constructor() {
    makeObservable(this, {
      filters: observable,
      displayFilters: observable,
    });
  }

  @action("push display filter")
  public pushDisplayFilter = (filter) => {
    this.displayFilters.push(filter);
  };

  @action("push filter")
  public pushFilter(filter: Filter) {
    if (!this.isFilterRegistred(filter.source)) {
      this.filters.push(filter);
    }
  }

  @action("get filter")
  public getFilter(source) {
    return this.filters.find((filter) => filter.source === source);
  }

  public isFilterRegistred(source: string) {
    return !!this.getFilter(source);
  }

  @action("set filter value")
  public setFilterValue(source: string, value: string) {
    if (!this.isFilterRegistred(source)) {
      throw new Error("Filter not registred!");
    }

    const filter = this.getFilter(source);
    filter.value = value;
  }
}
