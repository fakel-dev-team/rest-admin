import { AuthStore } from "./AuthStore";
import { AuthProviderStore } from "./AuthProviderStore";
import { ImagesStore } from "./ImagesStore";
import { ShowStore } from "./ShowStore";
import { URLStore } from "./UrlStore";
import { ListStore } from "./ListStore";
import { ResourceStore } from "./ResorceStore";
import { FiltersStore } from "./FiltersStore";

import type { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { AuthProvider } from "@fakel-dev-team/rest-admin-simple-auth";

import { DataProviderStore } from "./DataProviderStore";
import { makeAutoObservable } from "mobx";

export class AdminStore {
  private _dataProviderStore: DataProviderStore;
  private _authProviderStore: AuthProviderStore;
  private _resourceStore: ResourceStore;
  private _filtersStore: FiltersStore;
  private _listStore: ListStore;
  private _urlStore: URLStore;
  private _showStore: ShowStore;
  private _imagesStore: ImagesStore;
  private _authStore: AuthStore;

  constructor(dataProvider: DataProviderT, authProvider?: AuthProvider) {
    makeAutoObservable(this);
    this._dataProviderStore = new DataProviderStore(dataProvider);
    this._authProviderStore = new AuthProviderStore(authProvider);
    this._resourceStore = new ResourceStore();
    this._filtersStore = new FiltersStore();
    this._listStore = new ListStore();
    this._urlStore = new URLStore();
    this._showStore = new ShowStore();
    this._imagesStore = new ImagesStore();
    this._authStore = new AuthStore();
  }

  get showStore() {
    return this._showStore;
  }

  get authProviderStore() {
    return this._authProviderStore;
  }

  get authStore() {
    return this._authStore;
  }

  get imagesStore() {
    return this._imagesStore;
  }

  get urlStore() {
    return this._urlStore;
  }

  get listStore() {
    return this._listStore;
  }

  get filtersStore() {
    return this._filtersStore;
  }

  get dataProviderStore() {
    return this._dataProviderStore;
  }

  get resourceStore() {
    return this._resourceStore;
  }
}
