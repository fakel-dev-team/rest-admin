import { LinkT } from "./../@types/index";
import { RouteProps } from "react-router-dom";
import {
  makeAutoObservable,
  computed,
  observable,
  action,
  makeObservable,
} from "mobx";
import type React from "react";

export type ResourceOptions = {
  label?: string;
  icon?: React.ReactNode;
  isReference?: boolean;
  renderViewOnMenuClicked?: LinkT;
  linkUrl?: string;
};

export type ResourceT = {
  name: string;
  create?: React.ComponentType<any>;
  edit?: React.ComponentType<any>;
  list?: React.ComponentType<any>;
  show?: React.ComponentType<any>;
  options?: ResourceOptions;
};

export class ResourceStore {
  resources: ResourceT[] = [];
  currentResource: string = "";

  constructor() {
    makeObservable(this, {
      resources: observable,
      currentResource: observable,
      setCurrentResource: action,
      pushResource: action,
      getResource: action,
      getCurrentResource: action,
      isRegistred: action
    });
    this.resources = [];
  }

  @action("set current resourse")
  public setCurrentResource(currentResource: string) {
    this.currentResource = currentResource;
  }

  @action("push resource")
  public pushResource(resource: ResourceT) {
    if (!this.isRegistred(resource.name)) {
      this.resources.push(resource);
    }
  }

  @computed({ name: "get resource" })
  public getResource(name: string) {
    return this.resources.find((resource) => resource.name === name);
  }

  @computed({ name: "get current resource" })
  public getCurrentResource() {
    if(this.resources) {
      return this.resources.find((resource) => resource.name === this.currentResource) || this.resources[0];
    }
  }

  @action('get is registred')
  public isRegistred(name: string) {
    return !!this.getResource(name);
  }
}
