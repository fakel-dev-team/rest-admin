import { makeAutoObservable } from 'mobx';
import { AuthProvider } from '@fakel-dev-team/rest-admin-simple-auth';

export class AuthProviderStore {
	private provider: AuthProvider;

	constructor(authProvider: any) {
		makeAutoObservable(this);
		this.provider = authProvider;
	}

	get authProvider() {
		return this.provider;
	}
}
