import { FILTER_PARAMS_NAME } from "../constants/url";

export const deleteURLParameter = (url: string, parameter: string) => {
  const params = getURLParameters(url, parameter);
  delete params[parameter];

  if (!Object.keys(params).length) {
    return {
      path: "",
      params: {},
    };
  }

  if (Object.keys(params).length >= 1) {
    return { path: `?${FILTER_PARAMS_NAME}=${serialize(params)}`, params };
  }
};

export const getURLParameters = (url: string, paramName: string) => {
  if (url) {
    let result = {};

    const urlParams = decodeURI(url.split(`?`)[1]);
    if (urlParams) {
      const sURLVariables = urlParams.split("&");
      sURLVariables.forEach((el) => {
        let sParameterName = el.split("=");
        result[sParameterName[0]] = sParameterName[1];
      });
    }

    return result;
  }
};

export const serialize = (params) => {
  var str = [];
  for (var param in params)
    if (params.hasOwnProperty(param)) {
      if (params[param]) {
        str.push(param + "=" + params[param]);
      }
    }
  return str.join("&");
};
