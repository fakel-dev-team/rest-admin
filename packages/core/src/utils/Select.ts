import { OptionT } from "../@types";

import get from "lodash.get";

export const mapRecordsToOptions = (
  records: any,
  source: string,
  titlePropName: string
): OptionT[] => {
  return records.map((record, index) => {
    const value = get(record, source);
    const title = get(record, titlePropName);
    return {
      key: record.id || index,
      value,
      title,
    };
  });
};
