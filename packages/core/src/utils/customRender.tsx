import React from "react";
import { render } from "@testing-library/react";
import StoreProvider from "../components/StoreProvider";

import { mockAdminStore } from "../mock";

export const customRender = (ui) => {
  const { container, asFragment } = render(
    <StoreProvider adminStore={mockAdminStore}>{ui}</StoreProvider>
  );

  return { asFragment, container };
};
