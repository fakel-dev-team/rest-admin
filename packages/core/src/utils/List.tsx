import React from "react";

import { FieldProps } from "formik";
import { ColumnT } from "../@types";
import { ColumnType } from "antd/lib/table";

export const getSorter = (source: string) => {
  return (a, b) => {
    const aValue = `${a[source]}`;
    const bValue = `${b[source]}`;

    return aValue.localeCompare(bValue);
  };
};

export const mapToAntColumn = (columns: ColumnT[]): Array<ColumnType<any>> => {
  return columns.map((Column, index) => ({
    title: Column.title,
    dataIndex: Column.source,
    key: Column.id || index,
    sortDirections: Column.sortDirections ? Column.sortDirections : null,
    sorter: Column.sortDirections
      ? Column.sorter || getSorter(Column.source)
      : null,
    render: (_, record, index) => {
      return (
        <Column.Field<FieldProps>
          key={`${Column.source}-${index}`}
          record={record}
          source={Column.source}
          reference={Column.reference}
          link={Column.link}
        >
          {Column.FieldChildren ? Column.FieldChildren() : null}
        </Column.Field>
      );
    },
  }));
};
