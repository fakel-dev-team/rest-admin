import React from "react";

import {
  FormikProps,
  FieldInputProps,
  FieldMetaProps,
  ErrorMessage,
} from "formik";

import { Form, Typography, Select, Radio } from "antd";

const { Text } = Typography;

type UseAntComponentT = {
  Component: any;
  ComponentChildren?: (value: any) => any;
  form: FormikProps<any>;
  field: FieldInputProps<any>;
  meta?: FieldMetaProps<any>;
  props?: any;
};

export const useAntComponent = (params: UseAntComponentT) => {
  const { Component, form, field, meta } = params;
  const {
    component,
    componentChildren,
    options,
    radioButtons,
    isLink,
    showSearch,
    filterSort,
    filterOption,
    optionFilterProp,
    ...props
  } = params.props;

  const onChange = (event) => {
    form.setFieldValue(field.name, event.target ? event.target.value : event);
  };
  return (
    <Form.Item
      initialValue={props.defaultValue}
      validateStatus={meta.touched && meta.error ? "error" : "success"}
      {...props}
    >
      {componentChildren ? (
        <Component
          {...props}
          {...field}
          onBlur={field.onBlur}
          onChange={props.onChange || onChange}
        >
          <>
            {componentChildren
              ? componentChildren(options || radioButtons)
              : null}
          </>
        </Component>
      ) : (
        <>
          <Component
            {...props}
            {...field}
            onBlur={field.onBlur}
            onChange={props.onChange || onChange}
          />
          <ErrorMessage name={props.name}>
            {(errorMessage) => <Text type="danger">{errorMessage}</Text>}
          </ErrorMessage>
        </>
      )}
    </Form.Item>
  );
};
