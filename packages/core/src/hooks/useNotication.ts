import { notification } from "antd";
import { IconType } from "antd/lib/notification";

type useNotificationParamsT = {
  message: string;
  description?: string;
  type?: IconType;
  duration?: number;
};

export const useNotification = (params: useNotificationParamsT) => {
  const { message, description, type = "info" } = params;

  const openNotification = () => {
    notification[type]({
      message,
      description,
      duration: 0,
    });
  };

  return openNotification;
};
