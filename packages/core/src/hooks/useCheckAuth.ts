import { useAuthProviderStore } from "./useAuthProviderStore";
import { useAuthStore } from "./useAuthStore";

export const useCheckAuth = () => {
  const authProviderStore = useAuthProviderStore();
  const authStore = useAuthStore();

  try {
    authProviderStore.authProvider
      .checkAuth()
      .then((isAuthorized) => {
        authStore.setIsAuth(isAuthorized);
      })
      .catch((error) => {
        authStore.setIsAuth(false);
      });
    return { isAuthorized: authStore.isAuth };
  } catch (error) {}
};
