import { AuthStore } from './../stores/AuthStore';
import { useStore } from './useStore';

export const useAuthStore = () => {
	return useStore('authStore') as AuthStore;
};
