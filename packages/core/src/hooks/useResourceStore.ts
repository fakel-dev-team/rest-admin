import { ResourceStore } from "./../stores/ResorceStore";
import { useStore } from "./useStore";

export const useResourceStore = () => {
  const resourceStore = useStore("resourceStore") as ResourceStore;
  return resourceStore;
};
