import { ListStore } from "../stores/ListStore";
import { useStore } from "./useStore";

export const useListStore = () => {
  return useStore("listStore") as ListStore;
};
