import { useStore } from "./useStore";
import { FiltersStore } from "../stores/FiltersStore";

export const useFiltersStore = () => {
  return useStore("filtersStore") as FiltersStore;
};
