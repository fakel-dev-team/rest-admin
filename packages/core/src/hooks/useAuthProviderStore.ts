import { useStore } from './useStore';
import { AuthProviderStore } from '../stores/AuthProviderStore';

export const useAuthProviderStore = () => {
	const authProviderStore = useStore('authProviderStore') as AuthProviderStore;
	
	if (!authProviderStore.authProvider) {
		throw new Error(
			'Auth Provider not found in store. Define authProvider and pass into Admin props!',
		);
	}

	return authProviderStore;
};
