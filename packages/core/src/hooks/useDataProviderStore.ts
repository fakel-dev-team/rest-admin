import { useStore } from "./useStore";
import { DataProviderStore } from "../stores/DataProviderStore";

export const useDataProviderStore = () => {
  const dataProviderStore = useStore("dataProviderStore") as DataProviderStore;
  return dataProviderStore;
};
