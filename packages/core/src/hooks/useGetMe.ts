import { useAuthProviderStore } from "./useAuthProviderStore";
import { useAuthStore } from "./useAuthStore";

export const useGetMe = () => {
  const authProviderStore = useAuthProviderStore();
  const authStore = useAuthStore();

  try {
    authProviderStore.authProvider
      .getMe()
      .then((user) => {
        authStore.setUser(user);
      })
      .catch((error) => {
        console.log(error);
      });

    return { user: authStore.user };
  } catch (error) {}
};
