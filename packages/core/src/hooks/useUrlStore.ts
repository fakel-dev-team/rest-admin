import { URLStore } from "./../stores/UrlStore";
import { useStore } from "./useStore";

export const useUrlStore = () => {
  return useStore("urlStore") as URLStore;
};
