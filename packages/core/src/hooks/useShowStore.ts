import { ShowStore } from "./../stores/ShowStore";
import { useStore } from "./useStore";

export const useShowStore = () => {
  return useStore("showStore") as ShowStore;
};
