import { AdminStore } from "../stores/AdminStore";
import { ClassPropsT, selectStore } from "../selectors/selectorStore";
import { useContext } from "react";
import { AdminContext } from "../components/StoreProvider";

export const useStore = (name: ClassPropsT<AdminStore>) => {
  const { adminStore } = useContext(AdminContext);
  
  const store = selectStore(adminStore, name);
  return store;
};
