import { useState, useEffect } from "react";
import { useDataProviderStore } from "./useDataProviderStore";
import { useResourceStore } from "./useResourceStore";

export const useInitialValue = (id: string) => {
  const [initialValue, setInitialValue] = useState();
  const [loading, setLoading] = useState(false);

  const dataProviderStore = useDataProviderStore();
  const resourceStore = useResourceStore();

  const getInitialData = async () => {
    if (resourceStore.currentResource) {
      setLoading(true);
      const {
        data,
        errors,
      } = await dataProviderStore.dataProvider.getOne(
        resourceStore.currentResource,
        { id }
      );
      setInitialValue(data);
      setLoading(false);
    }
  };

  useEffect(() => {
    getInitialData();
  }, [id]);

  return { initialValue, loading };
};
