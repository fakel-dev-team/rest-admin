import { ImagesStore } from "./../stores/ImagesStore";
import { useStore } from "./useStore";

export const useImagesStore = () => {
  return useStore("imagesStore") as ImagesStore;
};
