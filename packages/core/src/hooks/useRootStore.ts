import { AdminStore } from "../stores/AdminStore";
import { useContext } from "react";
import { AdminContext } from "../components/StoreProvider";

export const useRootStore = (): AdminStore => {
  const { adminStore } = useContext(AdminContext);
  return adminStore;
};
