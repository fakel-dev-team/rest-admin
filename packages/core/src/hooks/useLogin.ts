import { useAuthProviderStore } from "./useAuthProviderStore";
import { useAuthStore } from "./useAuthStore";
import { useState } from "react";

export type LoginCredentials = {
  username: string;
  password: string;
};

export const useLogin = () => {
  const authProviderStore = useAuthProviderStore();
  const authStore = useAuthStore();

  const [loading, setLoading] = useState(false);

  return {
    login: async (credentials: LoginCredentials) => {
      try {
        setLoading(true);
        await authProviderStore.authProvider.login(credentials);
        authStore.setIsAuth(await authProviderStore.authProvider.checkAuth());
        authStore.setUser(await authProviderStore.authProvider.getMe());
        setLoading(false);
      } catch (error) {
        setLoading(false);
        console.error(error);
      }
    },
    logout: async () => {
      try {
        await authProviderStore.authProvider.logout();
        authStore.setIsAuth(false);
        authStore.setUser(null);
      } catch (error) {
        console.error(error);
      }
    },
    isAuthorized: authStore.isAuth,
    user: authStore.user,
    loading,
  };
};
