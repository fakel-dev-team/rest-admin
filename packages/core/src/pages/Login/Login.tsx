import React from "react";

import Space from "antd/lib/space";
import Card from "antd/lib/card";
import Button from "antd/lib/Button";

import { PasswordInput, SimpleForm, TextInput } from "../../components";
import { useLogin } from "../../hooks/useLogin";
import { Redirect, useHistory } from "react-router-dom";

export const LoginPage: React.FC = (props) => {
  const { login, isAuthorized, loading } = useLogin();

  const history = useHistory();

  const handleSubmit = (values) => {
    login(values).then(() => {
      if (isAuthorized) {
        history.push("/");
      }
    });
  };

  return !isAuthorized ? (
    <Space
      style={{
        width: "100vw",
        height: "100vh",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Card title="Login" style={{ width: "100%" }}>
        <SimpleForm
          buttonText="Login"
          handleSubmit={handleSubmit}
          initialValue={{ username: "", password: "" }}
          actions={
            <Button type="primary" htmlType="submit" loading={loading}>
              Login
            </Button>
          }
        >
          <TextInput name="username" placeholder="Username" />
          <PasswordInput name="password" placeholder="Password" />
        </SimpleForm>
      </Card>
    </Space>
  ) : (
    <Redirect to="/" />
  );
};
