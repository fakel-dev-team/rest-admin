import React from "react";

import Button from "antd/lib/button";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useHistory } from "react-router-dom";

type CreateButtonProps = {
  text?: string;
  style?: React.CSSProperties;
};

export const CreateButton: React.FC<CreateButtonProps> = ({ text, style }) => {
  const resourceStore = useResourceStore();
  const history = useHistory();

  const onClick = () => {
    history.push(`/${resourceStore.currentResource}/create`);
  };

  return (
    <Button style={style} type="primary" onClick={onClick}>
      {text || "Create"}
    </Button>
  );
};
