import React from 'react';

import Button, { ButtonType } from 'antd/lib/button';
import { useLogin } from '../../hooks/useLogin';
import { observer } from 'mobx-react';

type LoginButtonProps = {
	type?: ButtonType;
	text?: string;
	style?: React.CSSProperties;
};

export const LogoutButton: React.FC<LoginButtonProps> = observer(
	({ type, text, style }) => {
		const { logout } = useLogin();

		const onClick = () => {
			logout();
		};

		return (
			<Button style={style} type={type || 'link'} onClick={onClick}>
				{text || 'Logout'}
			</Button>
		);
	},
);
