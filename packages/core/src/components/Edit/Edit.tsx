import React from "react";

import { RedirectTo } from "../../@types";

import EditView from "./EditView";

import { useInitialValue } from "../../hooks";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useNotification } from "../../hooks/useNotication";
import { useDataProviderStore } from "../../hooks/useDataProviderStore";

import { useHistory, useParams } from "react-router-dom";
import { FormikHelpers } from "formik";
import Spin from "antd/lib/spin";
import Space from "antd/lib/space";
import LoadingOutlined from "@ant-design/icons/LoadingOutlined";

import { observer } from "mobx-react";

/**
 * @component Create
 * @props
 *  id - string (id current resource)
 *  title? - string (Title for create form)
 *  children - (props: any) => any
 *  redirect? - "list" | "show" | "create" | "edit"
 * 
 * @example
 * <Edit redirect="list">
      {(editProps) => {
        return (
          <SimpleForm
            {...createProps}
            initialValue={editProps.initialValue}
          >
            <TextInput label="Username" name="username" />
            <TextInput
              label="Email"
              name="email"
              placeholder="Enter email"
            />
          </SimpleForm>
        );
      }}
    </Edit>
 * */

type EditProps = {
  id?: string;
  title?: string;
  children: (props?: any) => any;
  redirect?: RedirectTo;
  saveRedirect?: (...any) => void;
  onSuccessfulSubmit?: () => void;
  onFailedSubmit?: (error?: any) => void;
  handleSubmit?: (value: any, actions: any) => any;
};

const Edit: React.FC<EditProps> = observer((props) => {
  const { title, children, redirect } = props;

  const { id } = useParams();

  const { initialValue, loading } = useInitialValue(props.id || id);

  const dataProviderStore = useDataProviderStore();
  const resourceStore = useResourceStore();
  const history = useHistory();

  const successfulNotification = useNotification({
    message: "Успешно отправлено",
  });
  const failedNotification = useNotification({
    type: "error",
    message: "Ошибка",
  });

  const handleSubmit = async (values: any, actions: FormikHelpers<any>) => {
    try {
      const response = await dataProviderStore.dataProvider.update(
        resourceStore.currentResource,
        values,
        { id }
      );

      props.onSuccessfulSubmit
        ? props.onSuccessfulSubmit()
        : successfulNotification();
      history.push(`/${resourceStore.currentResource}/${props.redirect}`);
    } catch (error) {
      props.onFailedSubmit ? props.onFailedSubmit() : failedNotification();
    }
  };

  return !loading ? (
    <EditView title={title}>
      {children({
        handleSubmit: props.handleSubmit || handleSubmit,
        initialValue,
      })}
    </EditView>
  ) : initialValue ? (
    <Space
      style={{
        height: "100vh",
        width: "100vw",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Spin indicator={<LoadingOutlined />} />
    </Space>
  ) : null;
});

export default Edit;
