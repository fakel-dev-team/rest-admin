import React from "react";

import Space from "antd/lib/space";
import Typography from "antd/lib/typography";

const { Title } = Typography;

type EditViewProps = {
  title?: string;
};

const EditView: React.FC<EditViewProps> = ({ title, children }) => {
  const defaultTitle = "Edit form";

  return (
    <Space direction="vertical">
      <Title level={4}>{title || defaultTitle}</Title>
      {children}
    </Space>
  );
};

export default EditView;
