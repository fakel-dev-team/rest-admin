"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_hooks_1 = require("@testing-library/react-hooks");
var react_2 = require("@testing-library/react");
var react_router_dom_1 = require("react-router-dom");
var customRender_1 = require("../../../utils/customRender");
var index_1 = require("../index");
var adminStore_1 = require("../../../mock/adminStore");
var StoreProvider_1 = __importDefault(require("../../StoreProvider"));
var user_event_1 = __importDefault(require("@testing-library/user-event"));
var useResourceStore_1 = require("../../../hooks/useResourceStore");
var react_router_dom_2 = require("react-router-dom");
var MockCreateView = function () { return react_1.default.createElement("div", null, "Create"); };
var mockResource = {
    name: "posts",
    create: MockCreateView,
};
var renderWrapper = function () {
    var wrapper = function (_a) {
        var children = _a.children;
        return (react_1.default.createElement(StoreProvider_1.default, { adminStore: adminStore_1.mockAdminStore }, children));
    };
    return wrapper;
};
describe("<Resource>", function () {
    afterEach(function () {
        react_hooks_1.cleanup();
    });
    test("should be save resource to store", function () {
        customRender_1.customRender(react_1.default.createElement(index_1.Resource, { name: mockResource.name, create: mockResource.create }));
        var result = react_hooks_1.renderHook(function () { return useResourceStore_1.useResourceStore(); }, {
            wrapper: renderWrapper(),
        }).result;
        var resources = result.current.resources;
        var resource = resources.find(function (resource) { return resource.name === mockResource.name; });
        expect(resources).toHaveLength(1);
        expect(resource).not.toBeUndefined();
    });
    test("should be open view after click on link", function () {
        var container = customRender_1.customRender(react_1.default.createElement(react_router_dom_2.BrowserRouter, null,
            react_1.default.createElement(index_1.Resource, { name: mockResource.name, create: mockResource.create }),
            react_1.default.createElement(react_router_dom_1.Link, { to: "/" + mockResource.name + "/create", id: "create" }, "Create"))).container;
        var link = container.querySelector("#create");
        user_event_1.default.click(link);
        react_2.waitFor(function () {
            expect(screen.getByText(/Create/i)).toBeInTheDocument();
        });
    });
});
