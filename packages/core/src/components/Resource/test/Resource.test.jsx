import React from "react";

import { renderHook, cleanup } from "@testing-library/react-hooks";
import { waitFor } from "@testing-library/react";

import { Link } from "react-router-dom";

import { customRender } from "../../../utils/customRender";
import { Resource } from "../index";

import { mockAdminStore } from "../../../mock/adminStore";
import StoreProvider from "../../StoreProvider";

import userEvent from "@testing-library/user-event";

import { useResourceStore } from "../../../hooks/useResourceStore";

import { BrowserRouter as Router } from "react-router-dom";

const MockCreateView = () => <div>Create</div>;

const mockResource = {
  name: "posts",
  create: MockCreateView,
};

const renderWrapper = () => {
  const wrapper = ({ children }) => (
    <StoreProvider adminStore={mockAdminStore}>{children}</StoreProvider>
  );
  return wrapper;
};

describe("<Resource>", () => {
  afterEach(() => {
    cleanup();
  });

  test("should be save resource to store", () => {
    customRender(
      <Resource name={mockResource.name} create={mockResource.create} />
    );

    const { result } = renderHook(() => useResourceStore(), {
      wrapper: renderWrapper(),
    });

    const resources = result.current.resources;
    const resource = resources.find(
      (resource) => resource.name === mockResource.name
    );
    expect(resources).toHaveLength(1);
    expect(resource).not.toBeUndefined();
  });

  test("should be open view after click on link", () => {
    const { container } = customRender(
      <Router>
        <Resource name={mockResource.name} create={mockResource.create} />
        <Link to={`/${mockResource.name}/create`} id="create">
          Create
        </Link>
      </Router>
    );

    const link = container.querySelector("#create");

    userEvent.click(link);

    waitFor(() => {
      expect(screen.getByText(/Create/i)).toBeInTheDocument();
    });
  });
});
