import React, { useEffect, useState } from "react";

import { Switch, Route, useLocation } from "react-router-dom";
import { observer } from "mobx-react";

import { useResourceStore } from "../../hooks/useResourceStore";

import ResourceWithLayout from "./ResourceWithLayout";

type ResourceRouterProps = {};

const ResourceRouter: React.FC<ResourceRouterProps> = observer((props) => {
  const resourceStore = useResourceStore();

  const currentLocation = useLocation();
  const currentResource =
    resourceStore.getCurrentResource() || resourceStore.resources[0];

  useEffect(() => {
    resourceStore.setCurrentResource(currentLocation.pathname.split("/", 2)[1]);
  }, [currentLocation.pathname]);

  return currentResource ? (
    <Switch key={currentResource.name}>
      <ResourceWithLayout
        exact
        path={`/${currentResource.name}/create`}
        view={currentResource.create}
      />
      <ResourceWithLayout
        exact
        path={`/${currentResource.name}/list`}
        view={currentResource.list}
      />
      <ResourceWithLayout
        exact
        path={`/${currentResource.name}/show/:id`}
        view={currentResource.show}
      />
      <ResourceWithLayout
        exact
        path={`/${currentResource.name}/edit/:id`}
        view={currentResource.edit}
      />
    </Switch>
  ) : null;
});

export default ResourceRouter;
