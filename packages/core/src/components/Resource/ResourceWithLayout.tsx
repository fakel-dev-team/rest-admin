import React from "react";
import { AdminLayout } from "../Admin";
import ProtectedRoute from "./ResourceProtectedRoute";

import { RouteProps } from "react-router-dom";
import { observer } from "mobx-react";

type ResourceWithLayoutProps = {
  view: any;
};

const ResourceWithLayout: React.FC<
  ResourceWithLayoutProps & RouteProps
> = observer(({ view, ...routeProps }) => {
  return (
    <ProtectedRoute
      {...routeProps}
      component={() => <AdminLayout>{React.createElement(view)}</AdminLayout>}
    />
  );
});

export default ResourceWithLayout;
