import React, { useEffect } from "react";
import { observer } from "mobx-react";

import { ResourceOptions } from "../../stores/ResorceStore";

import { useResourceStore } from "../../hooks/useResourceStore";
import ResourceRouter from "./ResourceRouter";

type ResourceProps = {
  name: string;
  options?: ResourceOptions;
  create?: React.ComponentType<any>;
  list?: React.ComponentType<any>;
  edit?: React.ComponentType<any>;
  show?: React.ComponentType<any>;
};

const Resource: React.FC<ResourceProps> = observer((resource) => {
  const resourceStore = useResourceStore();

  useEffect(() => {
    resourceStore.pushResource(resource);
  }, []);

  return null;
});

export default Resource;
