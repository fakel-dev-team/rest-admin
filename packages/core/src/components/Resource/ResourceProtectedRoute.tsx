import React from "react";
import { observer } from "mobx-react";
import { Redirect, Route } from "react-router-dom";
import { useAuthProviderStore } from "../../hooks/useAuthProviderStore";
import { useCheckAuth } from "../../hooks/useCheckAuth";

const ProtectedRoute = observer(({ component: Component, ...routeProps }) => {
  const { isAuthorized } = useCheckAuth();
  const authProviderStore = useAuthProviderStore();

  if (authProviderStore.authProvider) {
    return Component ? (
      <Route
        {...routeProps}
        render={(location) => {
          if (isAuthorized) return <Component />;
          else {
            return (
              <Redirect
                to={{ pathname: "/login", state: { from: location } }}
              />
            );
          }
        }}
      />
    ) : null;
  } else {
    return <Route {...routeProps} component={Component} />;
  }
});

export default ProtectedRoute;
