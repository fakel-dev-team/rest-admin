import React, { useState } from "react";
import { OptionT, AntFieldProps } from "../../@types";

import AutoComplete from "antd/lib/auto-complete";
import Select from "antd/lib/select";

import CoreInput, { InputProps } from "./Input";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";

import { mapRecordsToOptions } from "../../utils/Select";

import { GET_MANY_IS_NOT_DEFINED } from "../../constants/errors";

import debounce from "lodash.debounce";

interface AutoCompleteInputProps extends InputProps {
  valuePropName: string;
  titlePropName: string;
  reference: string;
}

const AutoCompleteInput: React.FC<AutoCompleteInputProps & AntFieldProps> = (
  props
) => {
  const { name, titlePropName, valuePropName } = props;
  const dataProviderStore = useDataProviderStore();

  const [options, setOptions] = useState<OptionT[]>([]);
  const [selectedOption, setSelectedOption] = useState<OptionT>(null);

  const fetch = async (searchText: string) => {
    if (!dataProviderStore.dataProvider.getMany) {
      throw new Error(GET_MANY_IS_NOT_DEFINED);
    }

    const { data } = await dataProviderStore.dataProvider.getMany(
      props.reference,
      {
        search: searchText,
      }
    );

    const options = mapRecordsToOptions(data, valuePropName, titlePropName);
    setOptions(options);
  };

  const onSelect = (value: number, option) => {
    if (option) {
      setSelectedOption(options[value - 1]);
    }
  };

  const onSearch = debounce((searchText: string) => {
    if (!searchText) {
      setOptions([]);
    } else {
      fetch(searchText);
    }
  }, 500);

  return (
    <CoreInput
      style={{ width: 200 }}
      options={options}
      {...props}
      onSelect={onSelect}
      onSearch={onSearch}
      component={AutoComplete}
      componentChildren={(options) =>
        options.map((option, index) => {
          return (
            <Select.Option key={option.key || index} value={option.value}>
              {option.title}
            </Select.Option>
          );
        })
      }
    ></CoreInput>
  );
};

export default AutoCompleteInput;
