import React from "react";

import Select from "antd/lib/select";
import { FormItemProps } from "antd/lib/form";

import CoreInput, { InputProps } from "./Input";

import { OptionT } from "../../@types";

import { mapRecordsToOptions } from "../../utils/Select";

const { Option } = Select;

interface SelectInputProps extends InputProps {
  valuePropName?: string;
  titlePropName?: string;
  defaultValue?: string;
  record?: OptionT[];
  disabled?: boolean;
  loading?: boolean;
  mode?: "multiple" | "tags";
  placeholder?: string;
  filter?: (input: any, option: any) => boolean;
  sort?: (optionA: any, optionB: any) => any;
  options?: OptionT[];
}

const SelectInput: React.FC<SelectInputProps & FormItemProps> = (props) => {
  let options;
  if (!props.options) {
    options = mapRecordsToOptions(
      props.record,
      props.valuePropName,
      props.titlePropName
    );
  }

  const filter = (input, option) =>
    option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;

  const sort = (optionA, optionB) =>
    optionA.children
      .toLowerCase()
      .localeCompare(optionB.children.toLowerCase());

  return (
    <CoreInput
      label={props.label}
      style={{ minWidth: 200 }}
      name={props.name}
      placeholder={props.placeholder || "Search"}
      options={props.options || options}
      component={Select}
      componentChildren={(options) =>
        options.map((option, index) => (
          <Option key={option.key || index} value={option.value}>
            {option.title}
          </Option>
        ))
      }
      showSearch
      optionFilterProp="children"
      filterOption={props.filter || filter}
      filterSort={props.sort || sort}
    />
  );
};

export default SelectInput;
