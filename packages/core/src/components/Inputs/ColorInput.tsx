import React, { useState } from "react";

import { FormItemProps } from "antd/lib/form";

import CoreInput, { InputProps } from "./Input";
import { ChromePicker } from "react-color";
import Space from "antd/lib/space";
import { TextInput } from ".";
import { FormikProps } from "formik";

interface ColorInputProps extends InputProps {}

const ColorInput: React.FC<ColorInputProps & FormItemProps> = (props) => {
  const [color, setColor] = useState("#000000");
  const [displayPicker, setDisplayPicker] = useState(false);

  const handleInputFocus = () => {
    setDisplayPicker(true);
  };

  const handleInputUnfocus = () => {
    setDisplayPicker(false);
  };

  const handleInputChange = (form: FormikProps<any>, event) => {
    const value = event.currentTarget.value;
    form.setFieldValue(props.name, value);
  };

  const handleChangeColor = (color) => {
    setColor(color.hex);
  };

  const handleColorChangeComplete = (form: FormikProps<any>, color: any) => {
    form.setFieldValue(props.name, color.hex);
  };

  return (
    <CoreInput {...props}>
      {(form, field) => {
        return (
          <div style={{ position: "relative" }}>
            <TextInput
              {...props}
              label=''
              onChange={(event) => handleInputChange(form, event)}
              onFocus={handleInputFocus}
            />
            {displayPicker ? (
              <div
                style={{
                  position: "absolute",
                  top: "110%",
                  left: 0,
                  zIndex: 2,
                }}>
                <div
                  onClick={handleInputUnfocus}
                  style={{
                    position: "fixed",
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                  }}></div>
                <ChromePicker
                  color={color}
                  onChange={handleChangeColor}
                  onChangeComplete={(color) =>
                    handleColorChangeComplete(form, color)
                  }
                />
              </div>
            ) : null}
          </div>
        );
      }}
    </CoreInput>
  );
};

export default ColorInput;
