import React from "react";

import CoreInput, { InputProps } from "./Input";

import { AntFieldProps } from "../../@types";

import AceEditor from "react-ace";
import "ace-builds/src-noconflict/mode-html";
import "ace-builds/src-noconflict/theme-monokai";
import { FormikProps, FieldProps } from "formik";

interface InputFieldProps extends InputProps {
  label?: string;
  placeholder?: string;
}

const CodeInput: React.FC<InputFieldProps & AntFieldProps> = (props) => {
  const handleChange = (form: FormikProps<any>, value: string) => {
    form.setFieldValue(props.name, value);
  };

  return (
    <CoreInput {...props} style={{ minWidth: 200 }}>
      {(form: FormikProps<any>, field) => {
        return (
          <AceEditor
            defaultValue={field.value}
            value={field.value}
            placeholder={props.placeholder || props.label}
            mode="html"
            style={{ width: "100%" }}
            maxLines={4}
            minLines={4}
            theme="monokai"
            name={props.name}
            onChange={(value) => handleChange(form, value)}
            fontSize={16}
            highlightActiveLine={true}
            setOptions={{
              enableBasicAutocompletion: false,
              enableLiveAutocompletion: false,
              enableSnippets: false,
              showLineNumbers: true,
              tabSize: 2,
            }}
          />
        );
      }}
    </CoreInput>
  );
};

export default CodeInput;
