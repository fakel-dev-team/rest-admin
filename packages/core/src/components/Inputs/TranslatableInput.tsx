import React, { useState } from "react";

import CoreInput, { InputProps } from "./Input";

import { AntFieldProps } from "../../@types";

import Tabs from "antd/lib/tabs";

const { TabPane } = Tabs;

type Locale = { [key: string]: string };

interface TranslatableInputProps {
  locales: Locale;
  children?: (activeLocale: string) => any;
}

const TranslatableInput: React.FC<TranslatableInputProps> = (props) => {
  const [activeLocale, setActiveLocale] = useState(props.locales[0]);

  const handleTabChange = (tab: string) => {
    setActiveLocale(tab);
  };

  const mapLocalesToTabs = () => {
    return Object.keys(props.locales).map((locale) => {
      return (
        <TabPane tab={props.locales[locale]} key={locale}>
          {props.children(locale)}
        </TabPane>
      );
    });
  };

  return (
    <Tabs type="card" onChange={handleTabChange}>
      {mapLocalesToTabs()}
    </Tabs>
  );
};

export default TranslatableInput;
