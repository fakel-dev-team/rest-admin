import React from "react";

import Switch from "antd/lib/switch";
import { FormItemProps } from "antd/lib/form";

import CoreInput, { InputProps } from "./Input";

interface SwitchInputProps extends InputProps {
  defaultChecked?: boolean;
}

const SwitchInput: React.FC<SwitchInputProps & FormItemProps> = (props) => {
  return <CoreInput {...props} component={Switch}></CoreInput>;
};

export default SwitchInput;
