import React from "react";

import { AntFieldProps } from "../../@types";

import TranslatableInput from "./TranslatableInput";
import CodeInput from "./CodeInput";

interface TranslatableCodeInputProps {
  label?: string;
  placeholde?: string;
}

const locales = { ru: "Русский", uk: "Украинский", en: "Английский" };

const TranslatableCodeInput: React.FC<
  TranslatableCodeInputProps & AntFieldProps
> = (props) => {
  return (
    <TranslatableInput locales={locales}>
      {(locale) => {
        return (
          <CodeInput name={`${props.name}.${locale}`} label={props.label} />
        );
      }}
    </TranslatableInput>
  );
};

export default TranslatableCodeInput;
