import React from "react";

import InputNumber from "antd/lib/input-number";
import { FormItemProps } from "antd/lib/form";

import CoreInput, { InputProps } from "./Input";

interface NumberInputProps extends InputProps {
  min?: number;
  max?: number;
  defaultValue?: number;
}

const NumberInput: React.FC<NumberInputProps & FormItemProps> = (props) => {
  return <CoreInput {...props} component={InputNumber}></CoreInput>;
};

export default NumberInput;
