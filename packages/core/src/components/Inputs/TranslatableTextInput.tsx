import React from "react";

import { AntFieldProps } from "../../@types";

import TranslatableInput from "./TranslatableInput";
import TextInput from "./TextInput";

interface TranslatableTextInputProps {
  label?: string;
  placeholder?: string;
}

const locales = { ru: "Русский", uk: "Украинский", en: "Английский" };

const TranslatableTextInput: React.FC<
  TranslatableTextInputProps & AntFieldProps
> = (props) => {
  return (
    <TranslatableInput locales={locales}>
      {(locale) => {
        return (
          <TextInput
            placeholder={props.placeholder}
            name={`${props.name}.${locale}`}
            label={props.label}
          />
        );
      }}
    </TranslatableInput>
  );
};

export default TranslatableTextInput;
