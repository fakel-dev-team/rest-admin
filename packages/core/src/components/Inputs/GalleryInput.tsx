import React, { useState } from "react";
import { AntFieldProps } from "../../@types";

import { ImageT } from "../../stores/ImagesStore";

import CoreInput, { InputProps } from "./Input";
import { GetImagesFunction, UploadHandler } from "../Gallery/Gallery";
import { Gallery, GallerySelectedImages } from "../Gallery";

import Space from "antd/lib/space";
import Button from "antd/lib/button";

import { FormikProps, FieldInputProps } from "formik";

interface GalleryInputdProps extends InputProps {
  resource?: string;
  getImages?: GetImagesFunction;
  defaultPerPage?: number;
  uploadHandler: UploadHandler;
  isMultiple?: boolean;
  buttonText?: string;
}

const GalleryInput: React.FC<GalleryInputdProps & AntFieldProps> = (props) => {
  const [isVisible, setVisible] = useState(false);

  const openModal = () => {
    setVisible(true);
  };

  const setFormValue = (form: FormikProps<any>, images: ImageT[]) => {
    form.setFieldValue(props.name, images);
  };

  return (
    <CoreInput {...props}>
      {(form: FormikProps<any>, field: FieldInputProps<any>) => {
        return (
          <Space direction="vertical" style={{ width: 300 }}>
            <Gallery
              isVisible={isVisible}
              setVisible={setVisible}
              onOk={(images: ImageT[]) => setFormValue(form, images)}
              buttonText={props.buttonText}
              resource={props.resource}
              uploadHandler={props.uploadHandler}
              getImages={props.getImages}
              defaultPerPage={props.defaultPerPage}
              isMultiple={props.isMultiple}
            />
            <Button onClick={openModal}>
              {props.buttonText || "Open modal"}
            </Button>
            <GallerySelectedImages
              openModal={openModal}
              images={form.values[field.name]}
            />
          </Space>
        );
      }}
    </CoreInput>
  );
};

export default GalleryInput;
