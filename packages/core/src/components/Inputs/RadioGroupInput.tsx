import React from "react";

import Radio from "antd/lib/radio";
import { FormItemProps } from "antd/lib/form";

import CoreInput, { InputProps } from "./Input";

const { Group: RadioGroup } = Radio;

export type RadioButtonT = {
  value: any;
  title: string;
  disable?: boolean;
  checked?: boolean;
};

interface RadioGroupInputProps extends InputProps {
  defaultValue?: string;
  radioButtons: RadioButtonT[];
  disabled?: boolean;
}

const RadioGroupInput: React.FC<RadioGroupInputProps & FormItemProps> = (
  props
) => {
  return <CoreInput {...props} component={RadioGroup}></CoreInput>;
};

export default RadioGroupInput;
