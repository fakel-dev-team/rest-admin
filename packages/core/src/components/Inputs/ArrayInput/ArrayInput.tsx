import React from "react";

import { FieldArray, FieldConfig } from "formik";

import { ArrayHelpers } from "formik/dist/FieldArray";

export interface ArrayInputProps extends FieldConfig {
  direction?: "vertical" | "horizontal";
  pushHandler?: (arrayHelpers: ArrayHelpers) => void;
  removeHandler?: (arrayHelpers: ArrayHelpers, index: any) => void;
}

const ArrayInput: React.FC<ArrayInputProps & any> = (props) => {
  const sourceName = props.name;

  return (
    <FieldArray name={sourceName}>
      {(arrayHelpers) => {
        const { values } = arrayHelpers.form;
        const records = values[sourceName];

        return React.cloneElement(React.Children.only(props.children), {
          records,
          arrayHelpers,
          arrayName: sourceName,
        });
      }}
    </FieldArray>
  );
};

export default ArrayInput;
