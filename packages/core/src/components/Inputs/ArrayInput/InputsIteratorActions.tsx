import React from "react";
import { ArrayHelpers } from "formik";

import DeleteOutlined from "@ant-design/icons/lib/icons/DeleteOutlined";
import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";
import UpOutlined from "@ant-design/icons/lib/icons/UpOutlined";
import DownOutlined from "@ant-design/icons/lib/icons/DownOutlined";

import Button from "antd/lib/button";
import Space from "antd/lib/space";

type InputsIteratorActionsProps = {
  direction?: "horizontal" | "vertical";
  arrayHelpers: ArrayHelpers;
  index: number;
  disabledUpArrow?: boolean;
  disabledDownArrow?: boolean;
  addButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  removeButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  moveUpButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  moveDownButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
};

const InputsIteratorActions: React.FC<InputsIteratorActionsProps> = ({
  arrayHelpers,
  index,
  disabledUpArrow,
  disabledDownArrow,
  addButton,
  removeButton,
  moveUpButton,
  moveDownButton,
  direction = "horizontal",
}) => {
  /**
   * @todo: show actions
   * */

  const _pushHandler = (arrayHelpers: ArrayHelpers) => {
    arrayHelpers.push({});
  };

  const _removeHandler = (arrayHelpers: ArrayHelpers, index: any) => {
    arrayHelpers.remove(index);
  };

  const _moveUpHandler = (arrayHelpers: ArrayHelpers, fromIndex: any) => {
    const toIndex = fromIndex - 1;
    arrayHelpers.swap(fromIndex, toIndex);
  };

  const _moveDownHandler = (arrayHelpers: ArrayHelpers, fromIndex: any) => {
    const toIndex = fromIndex + 1;
    arrayHelpers.swap(fromIndex, toIndex);
  };
  return (
    <Space direction={direction}>
      {addButton ? (
        addButton(arrayHelpers)
      ) : (
        <Button
          type="primary"
          onClick={() => _pushHandler(arrayHelpers)}
          icon={<PlusOutlined />}
        />
      )}
      {removeButton ? (
        removeButton(arrayHelpers)
      ) : (
        <Button
          type="primary"
          danger
          onClick={() => _removeHandler(arrayHelpers, index)}
          icon={<DeleteOutlined />}
        />
      )}
      {moveUpButton ? (
        moveUpButton(arrayHelpers)
      ) : (
        <Button
          disabled={disabledUpArrow}
          onClick={() => _moveUpHandler(arrayHelpers, index)}
          icon={<UpOutlined />}
        />
      )}
      {moveDownButton ? (
        moveDownButton(arrayHelpers)
      ) : (
        <Button
          disabled={disabledDownArrow}
          onClick={() => _moveDownHandler(arrayHelpers, index)}
          icon={<DownOutlined />}
        />
      )}
    </Space>
  );
};

export default InputsIteratorActions;
