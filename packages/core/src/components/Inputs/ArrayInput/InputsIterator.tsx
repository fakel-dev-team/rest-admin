import React from "react";

import { FieldConfig } from "formik";

import { ArrayHelpers } from "formik/dist/FieldArray";

import Col from "antd/lib/col";
import Row from "antd/lib/row";
import Card from "antd/lib/card";
import Space from "antd/lib/space";

import InputsIteratorActions from "./InputsIteratorActions";

type ActionsButtonType = "add" | "remove" | "up" | "down";
type ActionsPositionType = "header" | "body";

export interface InputsIterator extends FieldConfig {
  children?: (props) => any;
  direction?: "vertical" | "horizontal";
  records?: any;
  arrayHelpers?: ArrayHelpers;
  actions?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  addButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  removeButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  moveUpButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  moveDownButton?: (arrayHelpers: ArrayHelpers) => React.ReactNode;
  arrayName?: string;
  title?: string | React.ReactNode;
  showActions?: ActionsButtonType | ActionsButtonType[];
  actionPosition?: ActionsPositionType;
  customLayout?: (record: any, arrayName: string) => React.ReactNode;
  style?: React.CSSProperties;
}

const InputsIterator: React.FC<InputsIterator & any> = (props) => {
  const arrayName = props.arrayName;
  return (
    <Space style={props.style || { width: "50vw" }} direction="vertical">
      {props.records && props.customLayout
        ? props.customLayout(props, arrayName)
        : props.records.map((record, index) => {
            return (
              <Card
                style={{ width: "100%" }}
                extra={
                  (props.actionPosition &&
                    props.actionPosition === "header" &&
                    props.actions) || (
                    <InputsIteratorActions
                      arrayHelpers={props.arrayHelpers}
                      index={index}
                      disabledDownArrow={index === props.records.length - 1}
                      disabledUpArrow={index === 0}
                      addButton={props.addButton}
                      removeButton={props.removeButton}
                      moveUpButton={props.moveUpButton}
                      moveDownButton={props.moveDownButton}
                    />
                  )
                }
                title={props.title}
              >
                <Row justify="space-between" align="middle">
                  <Col span={20}>
                    {typeof props.children === "function"
                      ? props.children({
                          name: `${arrayName}.${index}`,
                        })
                      : React.Children.map(props.children, (child) =>
                          React.cloneElement(child, {
                            ...child.props,
                            name: `${arrayName}.${index}.${child.props.name}`,
                            value: record[child.props.name],
                          })
                        )}
                  </Col>
                  <Col
                    style={{ display: "flex", justifyContent: "flex-end" }}
                    span={4}
                  >
                    {props.actionPosition === "body" && (
                      <InputsIteratorActions
                        direction="vertical"
                        arrayHelpers={props.arrayHelpers}
                        index={index}
                        disabledDownArrow={index === props.records.length - 1}
                        disabledUpArrow={index === 0}
                        addButton={props.addButton}
                        removeButton={props.removeButton}
                        moveUpButton={props.moveUpButton}
                        moveDownButton={props.moveDownButton}
                      />
                    )}
                  </Col>
                </Row>
              </Card>
            );
          })}
    </Space>
  );
};

export default InputsIterator;
