import React from "react";

import Input from "antd/lib/input";
import { FormItemProps } from "antd/lib/form";

import CoreInput, { InputProps } from "./Input";

interface PasswordInputProps extends InputProps {
  placeholder?: string;
}

const PasswordInput: React.FC<PasswordInputProps & FormItemProps> = (props) => {
  return <CoreInput {...props} component={Input.Password}></CoreInput>;
};

export default PasswordInput;
