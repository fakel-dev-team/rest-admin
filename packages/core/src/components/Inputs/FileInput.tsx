import React from "react";

import Upload, { UploadProps } from "antd/lib/upload";
import { FormItemProps } from "antd/lib/form";

import CoreInput, { InputProps } from "./Input";

/**
 * https://ant.design/components/upload/#header
 * 
 * 
 * const props = {
      name: 'file',
      action: 'https://www.mocky.io/v2/5cc8019d300000980a055e76',
      headers: {
        authorization: 'authorization-text',
      },
      onChange(info) {
        if (info.file.status !== 'uploading') {
          console.log(info.file, info.fileList);
        }
        if (info.file.status === 'done') {
          message.success(`${info.file.name} file uploaded successfully`);
        } else if (info.file.status === 'error') {
          message.error(`${info.file.name} file upload failed.`);
        }
      },
    };
 * 
 * */

interface FileInputProps extends InputProps {
  button: JSX.Element;
}

const FileInput: React.FC<FileInputProps & FormItemProps & UploadProps> = (
  props
) => {
  return (
    <CoreInput {...props}>
      {(form, field, meta) => {
        return <Upload {...props}>{props.button}</Upload>;
      }}
    </CoreInput>
  );
};

export default FileInput;
