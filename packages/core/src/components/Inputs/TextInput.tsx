import React from "react";

import Input from "antd/lib/input";

import CoreInput, { InputProps } from "./Input";

import { AntFieldProps } from "../../@types";

interface InputFieldProps extends InputProps {}

const InputField: React.FC<InputFieldProps & AntFieldProps> = (props) => {
  return (
    <CoreInput
      {...props}
      style={{ width: "100%" }}
      component={Input}
    ></CoreInput>
  );
};

export default InputField;
