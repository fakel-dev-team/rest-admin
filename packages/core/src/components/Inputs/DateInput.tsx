import React from "react";

import { FormItemProps } from "antd/lib/form";
import DatePicker from "antd/lib/date-picker";

import CoreInput, { InputProps } from "./Input";

interface DateInputProps extends InputProps {}

const DateInput: React.FC<DateInputProps & FormItemProps> = (props) => {
  return <CoreInput {...props} component={DatePicker}></CoreInput>;
};

export default DateInput;
