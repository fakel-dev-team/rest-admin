import React, { useEffect, useState } from "react";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";

import get from "lodash.get";

export type ReferenceInputProps = {
  source: string;
  reference: string;
  label?: string;
  record?: any;
};

const ReferenceInput: React.FC<ReferenceInputProps> = (props) => {
  const { source, reference, record, children } = props;
  const [refRecord, setRefRecord] = useState(null);

  const dataProviderStore = useDataProviderStore();

  const refValue = get(record, source);

  const fetchRecord = async () => {
    const { data: _records } = await dataProviderStore.dataProvider.getOne(
      reference,
      {
        id: refValue,
      }
    );
    setRefRecord(_records);
  };

  useEffect(() => {
    fetchRecord();
  }, []);

  return (
    refRecord &&
    React.Children.map(children, (child: any) =>
      React.cloneElement(child, {
        ...child.props,
        record: refRecord,
        isLink: false,
        defaultValue: refRecord[child.props.name],
      })
    )
  );
};

export default ReferenceInput;
