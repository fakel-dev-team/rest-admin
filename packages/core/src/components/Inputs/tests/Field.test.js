"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_2 = require("@testing-library/react");
var Form_1 = require("../../Form");
var Input_1 = __importDefault(require("../Input"));
var antd_1 = require("antd");
var react_hooks_1 = require("@testing-library/react-hooks");
var renderWrapper = function () {
    var wrapper = function (_a) {
        var children = _a.children, initialValues = _a.initialValues;
        return (react_1.default.createElement(Form_1.Form, { handleSubmit: jest.fn(), initialValue: initialValues }, children));
    };
    return wrapper;
};
var TestInputWithChidren = function (props) {
    return react_1.default.createElement(Input_1.default, null, function (_, field) { return react_1.default.createElement("input", __assign({}, field, props)); });
};
var TestInputWithAntComponent = function (props) {
    return react_1.default.createElement(Input_1.default, __assign({}, props));
};
var renderInput = function (type) {
    var mockInitialValues = {
        firstName: "",
    };
    react_2.render(type === "component" ? (react_1.default.createElement(TestInputWithAntComponent, { component: antd_1.Input })) : (react_1.default.createElement(TestInputWithChidren, null)), {
        wrapper: renderWrapper(),
        initialProps: { initialValues: mockInitialValues },
    });
};
describe("<FormField>", function () {
    afterEach(function () {
        react_hooks_1.cleanup();
    });
    test("should render a <Input/> component with children input", function () {
        renderInput("children");
        var input = react_2.screen.getByRole("textbox");
        expect(input).toBeInTheDocument();
    });
    // test("should render a <Input/> component with Ant Component", () => {
    //   renderInput("component");
    //   const input = screen.getByRole("textbox");
    //   expect(input).toBeInTheDocument();
    // });
});
