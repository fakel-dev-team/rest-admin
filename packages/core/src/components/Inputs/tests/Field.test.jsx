import React from "react";

import { render, screen } from "@testing-library/react";
import { Form } from "../../Form";
import CoreInput from "../Input";

import { Input as AntInput } from "antd";
import { cleanup } from "@testing-library/react-hooks";

const renderWrapper = () => {
  const wrapper = ({ children, initialValues }) => (
    <Form handleSubmit={jest.fn()} initialValue={initialValues}>
      {children}
    </Form>
  );
  return wrapper;
};

const TestInputWithChidren = (props) => {
  return <CoreInput>{(_, field) => <input {...field} {...props} />}</CoreInput>;
};

const TestInputWithAntComponent = (props) => {
  return <CoreInput {...props} />;
};

const renderInput = (type) => {
  const mockInitialValues = {
    firstName: "",
  };

  render(
    type === "component" ? (
      <TestInputWithAntComponent component={AntInput} />
    ) : (
      <TestInputWithChidren />
    ),
    {
      wrapper: renderWrapper(),
      initialProps: { initialValues: mockInitialValues },
    }
  );
};

describe("<FormField>", () => {
  afterEach(() => {
    cleanup();
  });

  test("should render a <Input/> component with children input", () => {
    renderInput("children");

    const input = screen.getByRole("textbox");
    expect(input).toBeInTheDocument();
  });

  // test("should render a <Input/> component with Ant Component", () => {
  //   renderInput("component");

  //   const input = screen.getByRole("textbox");
  //   expect(input).toBeInTheDocument();
  // });
});
