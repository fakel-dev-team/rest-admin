import React, { useEffect, useState } from "react";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";

export type ReferenceManyInputProps = {
  name: string;
  reference: string;
  label?: string;
  record?: any;
};

const ReferenceManyInput: React.FC<ReferenceManyInputProps> = (props) => {
  const { reference, children } = props;
  const [refRecord, setRefRecord] = useState(null);

  const dataProviderStore = useDataProviderStore();

  const fetchRecord = async () => {
    if (!dataProviderStore.dataProvider.getMany) {
      throw new Error("getMany is not defined");
    }
    const { data: _records } = await dataProviderStore.dataProvider.getMany(
      reference
    );
    setRefRecord(_records);
  };

  useEffect(() => {
    fetchRecord();
  }, []);

  return (
    refRecord &&
    React.Children.map(children, (child: any) =>
      React.cloneElement(child, {
        ...child.props,
        ...props,
        record: refRecord,
        isLink: false,
        defaultValue: refRecord[child.props.name],
      })
    )
  );
};

export default ReferenceManyInput;
