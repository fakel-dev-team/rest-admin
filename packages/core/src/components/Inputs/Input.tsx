import React from "react";

import { Field, FieldConfig, FormikBag, FormikProps, FieldProps } from "formik";

import { useAntComponent } from "../../hooks/useAntComponent";
import Space from "antd/lib/space";
import Typography from "antd/lib/typography";
import Form from "antd/lib/form";

const { Text } = Typography;

export interface InputProps extends FieldConfig {
  component?: any;
  componentChildren?: (value: any) => any;
  children?: (form: FormikProps<any>, field: FieldProps, meta) => any;
}

const CoreInput: React.FC<InputProps & any> = (props) => {
  return (
    <Field name={props.name}>
      {({ form, field, meta }) => {
        return props.component ? (
          useAntComponent({
            Component: props.component,
            ComponentChildren: props.componentChildren,
            form,
            field,
            meta,
            props,
          })
        ) : (
          <Form.Item
            initialValue={props.defaultValue}
            validateStatus={meta.touched && meta.error ? "error" : "success"}
            {...props}
          >
            {props.children(form, field, meta)}
          </Form.Item>
        );
      }}
    </Field>
  );
};

export default CoreInput;
