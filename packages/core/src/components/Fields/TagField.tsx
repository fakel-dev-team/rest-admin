import React from "react";

import Tag from "antd/lib/tag";

import Field from "./Field";

import { FieldProps } from "../../@types";

type TagField = {
  color?: string;
  icon?: string;
  visible?: boolean;
};

const TagField: React.FC<FieldProps & TagField> = (props) => {
  const { source, record } = props;
  return (
    <Field source={source} record={record}>
      {(value) => (
        <Tag {...props} color="default">
          {value}
        </Tag>
      )}
    </Field>
  );
};

export default TagField;
