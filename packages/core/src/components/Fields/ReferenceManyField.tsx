import React, { useEffect, useState } from "react";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";

import { GET_MANY_IS_NOT_DEFINED } from "../../constants/errors";
import { LinkT } from "../../@types";

export type ReferenceManyFieldProps<RecordT = any> = {
  name: string;
  reference: string;
  link?: LinkT;
  record?: RecordT;
};

function ReferenceManyField<RecordT>({
  name,
  reference,
  link = "edit",
  children,
  record,
}: React.PropsWithChildren<ReferenceManyFieldProps<RecordT>>) {
  const [refRecord, setRefRecord] = useState([]);

  const dataProviderStore = useDataProviderStore();

  const fetchRecord = async () => {
    if (!dataProviderStore.dataProvider.getMany) {
      throw new Error(GET_MANY_IS_NOT_DEFINED);
    }

    const records = await dataProviderStore.dataProvider.getMany(reference, {
      id: record[name],
    });

    setRefRecord(records[name]);
  };

  useEffect(() => {
    fetchRecord();
  }, []);
  return (
    <>
      {refRecord &&
        React.cloneElement(React.Children.only(children as any), {
          records: refRecord,
          link,
        })}
    </>
  );
}

export default ReferenceManyField;
