import React from "react";

import Field from "./Field";

import Typography from 'antd/lib/typography'

import { FieldProps } from "../../@types/index";

const {Text} = Typography

type DateField = {
  format?: string
  locale?: string
};

const mapDateToFormat = (date: string, format?: string) => {
  return new Date(date).toLocaleDateString()
}

const DateField: React.FC<FieldProps & DateField> = (props) => {
  const { source, record } = props;
  return (
    <Field source={source} record={record}>
      {(value) => <Text>{mapDateToFormat(value)}</Text>}
    </Field>
  );
};

export default DateField;
