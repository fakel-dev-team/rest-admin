import React from "react";

import Field from "./Field";

import CheckOutlined from "@ant-design/icons/lib/icons/CheckOutlined";
import CloseOutlined from "@ant-design/icons/lib/icons/CloseOutlined";

import { FieldProps } from "../../@types/index";

type TagField = {
  color?: string;
  icon?: string;
  visible?: boolean;
};

const BooleanField: React.FC<FieldProps & TagField> = (props) => {
  const { source, record } = props;
  return (
    <Field source={source} record={record}>
      {(value) => (value ? <CheckOutlined /> : <CloseOutlined />)}
    </Field>
  );
};

export default BooleanField;
