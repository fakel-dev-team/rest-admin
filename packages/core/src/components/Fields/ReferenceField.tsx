import React, { useEffect, useState } from "react";
import type { FieldProps, LinkT } from "../../@types";

import Spin from "antd/lib/spin";
import LoadingOutlined from "@ant-design/icons/lib/icons/LoadingOutlined";

import { Link } from "react-router-dom";
import { useDataProviderStore } from "../../hooks/useDataProviderStore";

import get from "lodash.get";

export type ReferenceFieldProps = {
  reference: string;
  link: LinkT | false;
  children: (props?: any) => any;
};

export const createLink = (link: LinkT, reference: string, source: any) => {
  const baseUrl = `/${reference}`;

  if (link === "edit") {
    return `${baseUrl}/${source}`;
  }

  return `${baseUrl}/${link}`;
};

const ReferenceField: React.FC<FieldProps & ReferenceFieldProps> = ({
  source,
  reference,
  link,
  record,
  children,
}) => {
  const [refRecord, setRefRecord] = useState(null);
  const [loading, setLoading] = useState(false);

  const refValue = get(record, source);
  const dataProviderStore = useDataProviderStore();

  const isLink = !!link;

  const fetchRecord = async () => {
    const { data } = await dataProviderStore.dataProvider.getOne(reference, {
      id: refValue,
    });

    setRefRecord(data);
    setLoading(false);
  };

  useEffect(() => {
    setLoading(true);
    fetchRecord();
  }, []);

  return loading ? (
    <Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} />} />
  ) : (
    refRecord && (
      <>
        {React.Children.map(children, (child: any) => {
          return !isLink ? (
            React.cloneElement(child, {
              ...child.props,
              record: refRecord,
              isLink: false,
            })
          ) : (
            <Link to={createLink(link as LinkT, reference, name)}>
              {React.cloneElement(child, {
                ...child.props,
                record: refRecord,
                isLink: true,
              })}
            </Link>
          );
        })}
      </>
    )
  );
};

export default ReferenceField;
