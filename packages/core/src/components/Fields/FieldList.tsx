import React from "react";
import { LinkT } from "../../@types";
import { observer } from "mobx-react";

import Space from "antd/lib/space";

type FieldListPropsT = {
  link?: LinkT;
  records?: any[];
  direction?: "vertical" | "horizontal";
};

const FieldList: React.FC<FieldListPropsT> = observer((props) => {
  return (
    <Space direction={props.direction || "vertical"}>
      {props.records &&
        props.records.map((record) =>
          React.Children.map(props.children, (child: any) =>
            React.cloneElement(child, { ...props, record })
          )
        )}
    </Space>
  );
});

export default FieldList;
