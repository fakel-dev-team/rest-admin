import React from "react";

import { FieldProps } from "../../@types";

import Typography from "antd/lib/typography";

import Field from "./Field";

const { Text } = Typography;

type TextFieldProps = {
  style?: React.CSSProperties;
  isLink?: boolean;
};

const useLinkStyle = {
  color: "#1890ff",
};

const TextField: React.FC<FieldProps & TextFieldProps> = ({
  record,
  source,
  style,
  isLink = false,
  label,
}) => {
  return (
    <Field record={record} source={source}>
      {(value) => (
        <>
          {label && (
            <>
              <Text type="secondary">{label}</Text>
              <br />
            </>
          )}

          <Text style={Object.assign({}, style, isLink ? useLinkStyle : null)}>
            {value}
          </Text>
        </>
      )}
    </Field>
  );
};

export default TextField;
