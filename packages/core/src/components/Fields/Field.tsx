import React from "react";

import get from "lodash.get";

/**
 * @component Field
 * @props
 *  record - any (record from data source)
 *  source - string (name property in record)
 *  children - (value: any) => any
 *
 * @example
 * const record = {id: 1, name: "Name"}
 * const source = "name"
 * <Field source={source} record={record}>
      {(value) => (value ? <CheckOutlined /> : <CloseOutlined />)}
    </Field>
 * */

type FieldProps = {
  record: any;
  source: string;
  children: (value: any) => any;
};

const Field: React.FC<FieldProps> = ({ record, source, children }) => {
  const value = get(record, source);
  return children(value);
};

export default Field;
