import React from "react";

import Space from "antd/lib/space";
import Image from "antd/lib/image";
import Button from "antd/lib/button";

import { ImageT } from "../../stores/ImagesStore";

export type GallerySelecteImagesProps = {
  images: ImageT[];
  openModal: (isVisible: boolean) => void;
};

const renderImages = (images: ImageT[]) => {
  return (
    <Space size={[10, 10]} wrap>
      {images.map((image) => (
        <Image src={image.src} key={image.id} width={50} />
      ))}
    </Space>
  );
};

const renderSelectedImages = (
  images: ImageT[],
  openModal: (...any) => void
) => {
  if (images.length > 10) {
    return (
      <Space direction="vertical">
        {renderImages(images.slice(0, 10))}
        <Button onClick={openModal} type="ghost">
          Показать больше
        </Button>
      </Space>
    );
  }
  return renderImages(images);
};
export const GallerySelectedImages: React.FC<GallerySelecteImagesProps> = ({
  images,
  openModal,
}) => {
  return <>{images ? renderSelectedImages(images, openModal) : null}</>;
};
