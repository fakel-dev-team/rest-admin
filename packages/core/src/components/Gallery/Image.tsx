import React from "react";

import AntImage from "antd/lib/image";
import { ImageT } from "../../stores/ImagesStore";
type ImageProps = {
  image: ImageT;
  handleImageClick: (image: ImageT) => void;
};
export const Image: React.FC<ImageProps> = ({ image, handleImageClick }) => {
  return (
    <AntImage
      style={image.isSelected ? { border: "2px solid black" } : null}
      onClick={() => handleImageClick(image)}
      preview={false}
      width={100}
      src={image.src}
      key={image.id}
      alt={image.name || ""}
    />
  );
};
