import React, { useState } from "react";

import Modal from "antd/lib/modal";
import Space from "antd/lib/space";
import Button from "antd/lib/button";
import Divider from "antd/lib/divider";
import Pagination from "antd/lib/pagination";

import { Image } from "./Image";
import { UploadHandler } from "./Gallery";

import { GalleryViewFooter, OnOK } from "./GalleryFooter";
import { ImageT } from "../../stores/ImagesStore";
import { observer } from "mobx-react";
import { useImagesStore } from "../../hooks/useImagesStore";

export type GalleryViewProps = {
  uploadHandler: UploadHandler;
  isMultiple: boolean;
  images: ImageT[];
  totalPages: number;
  currentPage: number;
  perPage: number;
  onPerPageChange: (current: number, perPage: any) => void;
  onPageChange: (page: number) => void;
  onOk: OnOK;
  isVisible: boolean;
  setVisible: (isVisible: boolean) => void;
};

export const GalleryView: React.FC<GalleryViewProps> = observer(
  ({
    uploadHandler,
    isMultiple,
    images,
    currentPage,
    totalPages,
    perPage,
    onPageChange,
    onPerPageChange,
    isVisible,
    onOk,
    setVisible,
  }) => {
    if (!uploadHandler) {
      throw new Error("Define upload handler - function (file: RcFile) => any");
    }

    const imagesStore = useImagesStore();

    const handleImageSelect = (image: ImageT) => {
      imagesStore.setIsSelected(image, !image.isSelected);
    };
    return (
      <>
        <Modal
          width="90%"
          title="Галерея"
          visible={isVisible}
          onCancel={() => setVisible(false)}
          footer={
            <GalleryViewFooter
              onOk={onOk}
              setVisible={setVisible}
              isMultiple={isMultiple}
              uploadHandler={uploadHandler}
            />
          }
        >
          <Space size={10} wrap>
            {images.map((image) => (
              <Image handleImageClick={handleImageSelect} image={image} />
            ))}
          </Space>
          <Divider />
          <Pagination
            onChange={onPageChange}
            current={currentPage}
            total={totalPages}
            size="small"
            showSizeChanger
            pageSize={perPage}
            pageSizeOptions={["2", "10", "15", "20", "50", "100"]}
            onShowSizeChange={onPerPageChange}
          />
        </Modal>
      </>
    );
  }
);
