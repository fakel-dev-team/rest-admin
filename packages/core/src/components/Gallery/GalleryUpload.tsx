import React from "react";

import Button from "antd/lib/button";
import Upload from "antd/lib/upload";

import { UploadHandler } from "./Gallery";
import { observer } from "mobx-react";
import { useImagesStore } from "../../hooks/useImagesStore";

type GalleryUploadProps = {
  uploadHandler: UploadHandler;
  isMultiple?: boolean;
};

export const GalleryUpload: React.FC<GalleryUploadProps> = observer(
  ({ uploadHandler, isMultiple }) => {
    const imagesStore = useImagesStore();

    const customUploadRequest = async (file: File, isMultiple: boolean) => {
      const files: File[] = [];
      if (isMultiple) {
        files.push(file);
      }

      const image = await uploadHandler(isMultiple ? files : file);
      imagesStore.pushImage(image);
    };

    return (
      <Upload
        multiple={isMultiple || true}
        customRequest={({ file }) =>
          customUploadRequest(file, isMultiple || true)
        }
      >
        <Button type="ghost">Загрузить</Button>
      </Upload>
    );
  }
);
