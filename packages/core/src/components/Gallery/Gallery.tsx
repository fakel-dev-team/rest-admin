import React, { useState, useEffect } from "react";

import {
  DataProviderT,
  GetListResponse,
} from "@fakel-dev-team/rest-admin-simple-rest";

import { observer } from "mobx-react";

import { ImageT } from "../../stores/ImagesStore";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useImagesStore } from "../../hooks/useImagesStore";

import { GalleryView } from "./GalleryView";
import { OnOK } from "./GalleryFooter";

export type UploadHandler = (files: File | File[]) => any;
export type Pagination = {
  currentPage: number;
  perPage: number;
};

export type GetImagesFunction = (
  dataProvider: DataProviderT,
  pagination?: Pagination
) => GetListResponse;

export type GalleryProps = {
  resource?: string;
  getImages?: GetImagesFunction;
  defaultPerPage?: number;
  uploadHandler: UploadHandler;
  isMultiple?: boolean;
  buttonText?: string;
  onOk: OnOK;
  isVisible: boolean;
  setVisible: (isVisible: boolean) => void;
};

const getByResource = async (
  resource: string,
  dataProvider: DataProviderT,
  pagination: Pagination
) => {
  const { data } = await dataProvider.getList(resource, { pagination });
  return data;
};

export const Gallery: React.FC<GalleryProps> = observer(
  ({
    resource,
    getImages,
    defaultPerPage,
    uploadHandler,
    isMultiple,
    onOk,
    isVisible,
    setVisible,
  }) => {
    const [currentPage, setCurrentPage] = useState(1);
    const [perPage, setPerPage] = useState(defaultPerPage || 10);
    const imagesStore = useImagesStore();
    const { dataProvider } = useDataProviderStore();
    const onPageChange = (page) => {
      setCurrentPage(page);
    };

    const onPerPageChange = (_, perPage) => {
      setPerPage(perPage);
    };

    useEffect(() => {
      if (getImages) {
        const { data: images, total } = getImages(dataProvider, {
          perPage,
          currentPage,
        });
        imagesStore.setImages(images, total);
      } else if (resource) {
        async () => {
          const { data: images, total } = await getByResource(
            resource,
            dataProvider,
            {
              currentPage,
              perPage,
            }
          );
          imagesStore.setImages(images, total);
        };
      } else {
        throw new Error(
          'Define way to get images (example: <Resource name="image"/>, or function getImage(dataProvider, pagination?)'
        );
      }
    }, [perPage, currentPage]);
    return (
      <GalleryView
        onOk={onOk}
        setVisible={setVisible}
        isVisible={isVisible}
        isMultiple={isMultiple}
        uploadHandler={uploadHandler}
        currentPage={currentPage}
        perPage={perPage}
        onPerPageChange={onPerPageChange}
        onPageChange={onPageChange}
        totalPages={imagesStore.total}
        images={imagesStore.images}
      />
    );
  }
);
