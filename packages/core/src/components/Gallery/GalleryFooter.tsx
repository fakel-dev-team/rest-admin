import React from "react";

import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Button from "antd/lib/button";
import Space from "antd/lib/space";
import Checkbox from "antd/lib/checkbox";

import { UploadHandler } from "./Gallery";
import { GalleryUpload } from "./GalleryUpload";
import { ImageT } from "../../stores/ImagesStore";
import { observer } from "mobx-react";
import { useImagesStore } from "../../hooks/useImagesStore";

export type OnOK = (images: ImageT[]) => void;

type GalleryFooterProps = {
  uploadHandler: UploadHandler;
  setVisible: (visible: boolean) => void;
  isMultiple?: boolean;
  onOk: OnOK;
};

export const GalleryViewFooter: React.FC<GalleryFooterProps> = observer(
  ({ setVisible, uploadHandler, isMultiple, onOk }) => {
    const imagesStore = useImagesStore();

    const handleOk = () => {
      setVisible(false);
      onOk(imagesStore.images.filter((image) => image.isSelected));
    };

    const handleCancle = () => {
      imagesStore.clearSelected();
      setVisible(false);
    };

    const toggleSelectAll = () => {
      imagesStore.toggleSelectAll();
    };

    return (
      <Row justify="space-between">
        <Col style={{ textAlign: "left" }} span={12}>
          <Space size={[10, 0]} align="start">
            <GalleryUpload
              uploadHandler={uploadHandler}
              isMultiple={isMultiple}
            />
            <Checkbox onChange={toggleSelectAll}>Выделить всё</Checkbox>
          </Space>
        </Col>

        <Col span={12}>
          <Button onClick={handleCancle} type="primary" danger>
            Отмена
          </Button>
          <Button onClick={handleOk} type="primary">
            Ок
          </Button>
        </Col>
      </Row>
    );
  }
);
