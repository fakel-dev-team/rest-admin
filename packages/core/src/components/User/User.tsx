import React from "react";

import Avatar from "antd/lib/avatar";
import { useGetMe } from "../../hooks/useGetMe";
import { observer } from "mobx-react";

import UserOutlined from "@ant-design/icons/UserOutlined";

export type AvatarType = "text" | "icon" | "image";
export type AvatarTextFunction = (user: any) => string;

type UserProps = {
  avatarType?: AvatarType;
  avatarText?: string | AvatarTextFunction;
  avatarIcon?: React.ReactNode;
  avatarImageSrc?: string;
};

const renderAvatar = (
  avatarType: AvatarType,
  avatarIcon: React.ReactNode,
  avatarImageSrc: string,
  avatarText: string | AvatarTextFunction,
  user: any
) => {
  switch (avatarType) {
    case "icon":
      return <Avatar icon={avatarIcon} />;
    case "image":
      return <Avatar src={avatarImageSrc} />;
    case "text":
      return (
        <Avatar>
          {typeof avatarText === "string"
            ? avatarText
            : typeof avatarText === "function"
            ? avatarText(user)
            : null && console.error("Invalid avatar text type")}
        </Avatar>
      );
    default:
      return <Avatar icon={<UserOutlined />} />;
  }
};

export const User: React.FC<UserProps> = observer(
  ({ avatarIcon, avatarImageSrc, avatarText, avatarType }) => {
    const { user } = useGetMe();

    return renderAvatar(
      avatarType,
      avatarIcon,
      avatarImageSrc,
      avatarText,
      user
    );
  }
);
