import React from "react";

import Tabs from "antd/lib/tabs";
import Space from "antd/lib/space";
import Button from "antd/lib/button";

import { Form } from "./index";

/**
 * @example 
 * <TabbedForm
      {...createProps}
      initialValue={{ title: "", body: "", author: "" }}
    >
      <FormTab tab="Basic info">
        <TextInput label="Title" name="title" placeholder="Enter title" />
        <TextInput label="Body" name="body" placeholder="Enter body" />
      </FormTab>
      <FormTab tab="Info">
        <TextInput
          label="Author"
          name="author"
          placeholder="Enter author"
        />
      </FormTab>
    </TabbedForm>
 * 
 * */

type TabbedFormInterface = {
  initialValue: any;
  handleSubmit?: (...any) => any;
  defaultActiveKey?: string;
  buttonText?: string;
};

const TabbedForm: React.FC<TabbedFormInterface> = (props) => {
  if (!props.initialValue) {
    throw new Error("Define initial values!");
  }
  return (
    props.initialValue && (
      <Form
        handleSubmit={props.handleSubmit}
        initialValue={props.initialValue}
        {...props}
      >
        <Space direction="vertical">
          <Tabs defaultActiveKey={props.defaultActiveKey || "1"}>
            {React.Children.map(props.children, (child: any, index: number) =>
              React.cloneElement(child, { ...child.props, key: index })
            )}
          </Tabs>
          <Button type="primary" htmlType="submit">
            {props.buttonText || "Save"}
          </Button>
        </Space>
      </Form>
    )
  );
};

export default TabbedForm;
