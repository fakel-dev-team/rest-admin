import React from "react";

import { Form } from "./index";
import Space from "antd/lib/space";
import { Button } from "antd";

type SimpleFormInterface = {
  initialValue: any;
  handleSubmit?: (...any) => any;
  actions?: React.ReactNode;
  buttonText?: string;
};

const SimpleForm: React.FC<SimpleFormInterface> = (props) => {
  return props.initialValue ? (
    <Form
      handleSubmit={props.handleSubmit}
      initialValue={props.initialValue}
      {...props}
    >
      <Space direction="vertical">
        {props.children}
        {props.actions || (
          <Button type="primary" htmlType="submit">
            {props.buttonText || "Save"}
          </Button>
        )}
      </Space>
    </Form>
  ) : null;
};

export default SimpleForm;
