import React from "react";
import Form from "../Form";
import { TextInput } from "../../Inputs";
import { Button, notification } from "antd";

import {
  render,
  fireEvent,
  screen,
  waitFor,
  cleanup,
} from "@testing-library/react";

import { mockAdminStore } from "../../../mock/adminStore";
import StoreProvider from "../../StoreProvider";

const MockForm = ({ onSubmit }) => {
  return (
    <Form
      id="submitForm"
      initialValue={{ email: "", firstName: "" }}
      handleSubmit={onSubmit}
    >
      <TextInput placeholder="Email" id="email" name="email" label="Email" />
      <TextInput
        placeholder="First Name"
        id="firstName"
        name="firstName"
        label="First Name"
      />
      <Button id="button" name="submit" htmlType="submit">
        Send
      </Button>
    </Form>
  );
};

const renderWrapper = () => {
  const wrapper = ({ children, adminStore }) => (
    <StoreProvider adminStore={adminStore}>{children}</StoreProvider>
  );
  return wrapper;
};

const renderForm = (handleSubmit) => {
  const initialProps = {
    adminStore: mockAdminStore,
  };

  const { unmount, container, asFragment } = render(
    <MockForm onSubmit={handleSubmit} />,
    {
      wrapper: renderWrapper(),
      initialProps,
    }
  );

  return { unmount, container, asFragment };
};

describe("Form component", () => {
  let handleSubmit = null;

  beforeAll(() => {
    handleSubmit = jest.fn().mockImplementation(() => {
      notification.open({
        message: "Успешно отправлено",
      });
    });
  });

  afterEach(() => {
    cleanup();
  });

  test("should rendering Formik form", async () => {
    const mockValues = {
      email: "kapishdima@gmail.com",
      firstName: "Kapish Dima",
    };

    const { container, asFragment } = renderForm(handleSubmit);

    const email = container.querySelector("#email");
    const firstName = container.querySelector("#firstName");
    const button = container.querySelector("#button");

    expect(email).toBeInTheDocument();
    expect(firstName).toBeInTheDocument();
    expect(button).toBeInTheDocument();

    await waitFor(() =>
      fireEvent.change(email, { target: { value: mockValues.email } })
    );
    await waitFor(() =>
      fireEvent.change(firstName, { target: { value: mockValues.firstName } })
    );

    await waitFor(() => {
      fireEvent.click(button);
    });

    expect(email.value).toBe(mockValues.email);
    expect(firstName.value).toBe(mockValues.firstName);

    await waitFor(() => {
      expect(screen.getByText(/Успешно отправлено/i)).toBeInTheDocument();
    });
  });
});
