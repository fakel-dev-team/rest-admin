"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var Form_1 = __importDefault(require("../Form"));
var Inputs_1 = require("../../Inputs");
var antd_1 = require("antd");
var react_2 = require("@testing-library/react");
var adminStore_1 = require("../../../mock/adminStore");
var StoreProvider_1 = __importDefault(require("../../StoreProvider"));
var MockForm = function (_a) {
    var onSubmit = _a.onSubmit;
    return (react_1.default.createElement(Form_1.default, { id: "submitForm", initialValue: { email: "", firstName: "" }, handleSubmit: onSubmit },
        react_1.default.createElement(Inputs_1.TextInput, { placeholder: "Email", id: "email", name: "email", label: "Email" }),
        react_1.default.createElement(Inputs_1.TextInput, { placeholder: "First Name", id: "firstName", name: "firstName", label: "First Name" }),
        react_1.default.createElement(antd_1.Button, { id: "button", name: "submit", htmlType: "submit" }, "Send")));
};
var renderWrapper = function () {
    var wrapper = function (_a) {
        var children = _a.children, adminStore = _a.adminStore;
        return (react_1.default.createElement(StoreProvider_1.default, { adminStore: adminStore }, children));
    };
    return wrapper;
};
var renderForm = function (handleSubmit) {
    var initialProps = {
        adminStore: adminStore_1.mockAdminStore,
    };
    var _a = react_2.render(react_1.default.createElement(MockForm, { onSubmit: handleSubmit }), {
        wrapper: renderWrapper(),
        initialProps: initialProps,
    }), unmount = _a.unmount, container = _a.container, asFragment = _a.asFragment;
    return { unmount: unmount, container: container, asFragment: asFragment };
};
describe("Form component", function () {
    var handleSubmit = null;
    beforeAll(function () {
        handleSubmit = jest.fn().mockImplementation(function () {
            antd_1.notification.open({
                message: "Успешно отправлено",
            });
        });
    });
    afterEach(function () {
        react_2.cleanup();
    });
    test("should rendering Formik form", function () { return __awaiter(void 0, void 0, void 0, function () {
        var mockValues, _a, container, asFragment, email, firstName, button;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    mockValues = {
                        email: "kapishdima@gmail.com",
                        firstName: "Kapish Dima",
                    };
                    _a = renderForm(handleSubmit), container = _a.container, asFragment = _a.asFragment;
                    email = container.querySelector("#email");
                    firstName = container.querySelector("#firstName");
                    button = container.querySelector("#button");
                    expect(email).toBeInTheDocument();
                    expect(firstName).toBeInTheDocument();
                    expect(button).toBeInTheDocument();
                    return [4 /*yield*/, react_2.waitFor(function () {
                            return react_2.fireEvent.change(email, { target: { value: mockValues.email } });
                        })];
                case 1:
                    _b.sent();
                    return [4 /*yield*/, react_2.waitFor(function () {
                            return react_2.fireEvent.change(firstName, { target: { value: mockValues.firstName } });
                        })];
                case 2:
                    _b.sent();
                    return [4 /*yield*/, react_2.waitFor(function () {
                            react_2.fireEvent.click(button);
                        })];
                case 3:
                    _b.sent();
                    expect(email.value).toBe(mockValues.email);
                    expect(firstName.value).toBe(mockValues.firstName);
                    return [4 /*yield*/, react_2.waitFor(function () {
                            expect(react_2.screen.getByText(/Успешно отправлено/i)).toBeInTheDocument();
                        })];
                case 4:
                    _b.sent();
                    return [2 /*return*/];
            }
        });
    }); });
});
