import React from "react";

import {
  FormikErrors,
  Formik,
  FormikValues,
  FormikHelpers,
  // Form,
  FormikProps,
} from "formik";

import Form from 'antd/lib/form'

export interface AppFormProps {
  id?: string;
  name?: string;
  validate?: (
    values: FormikValues,
    props: any
  ) => FormikErrors<any> | Promise<any>;
  validationSchema?: any;
  initialValue?: any;
  handleSubmit?: (values: any, formikHelpers: FormikHelpers<any>) => void;
}

const AppForm: React.FC<AppFormProps> = (props) => {
  const {
    id,
    validate,
    initialValue,
    validationSchema,
    children,
    handleSubmit,
  } = props;

  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialValue}
      onSubmit={handleSubmit}
    >
      {(formikProps: FormikProps<any>) => {
        return (
          <Form id={id} layout="vertical" onFinish={formikProps.handleSubmit}>
            {children}
          </Form>
        );
      }}
    </Formik>
  );
};

export default AppForm;
