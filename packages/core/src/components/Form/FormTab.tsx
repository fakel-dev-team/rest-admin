import React from "react";

import Tabs, { TabPaneProps } from "antd/lib/tabs";

const { TabPane } = Tabs;

const FormTab: React.FC<TabPaneProps> = (props) => {
  return <TabPane {...props}>{props.children}</TabPane>;
};

export default FormTab;
