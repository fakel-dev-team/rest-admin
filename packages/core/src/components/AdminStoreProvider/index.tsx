import React from "react";
import { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { AdminStore } from "../../stores/AdminStore";

import StoreProvider from "../StoreProvider";

type AdminStoreProviderProps = {
  dataProvider: DataProviderT;
};

const AdminStoreProvider: React.FC<AdminStoreProviderProps> = ({
  dataProvider,
  children,
}) => {
  const adminStore = new AdminStore(dataProvider);

  return <StoreProvider adminStore={adminStore}>{children}</StoreProvider>;
};

export default AdminStoreProvider;
