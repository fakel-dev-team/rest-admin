import React, { useState } from "react";

import { useResourceStore } from "../../hooks/useResourceStore";
import { RedirectTo } from "../../@types";

type RedirectProps = {
  to: RedirectTo;
  children: (props?: any) => any;
};

const FormWithRedirect: React.FC<RedirectProps> = ({ to, children }) => {
  const resourceStore = useResourceStore();
  const [redirect, setRedirect] = useState(
    `/${resourceStore.currentResource}/${to}` || ""
  );

  const saveRedirect = () => {
    setRedirect(`/${resourceStore.currentResource}/${to}`);
  };

  return children({ redirect, saveRedirect });
};

export default FormWithRedirect;
