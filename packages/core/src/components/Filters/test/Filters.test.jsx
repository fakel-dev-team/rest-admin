import React from "react";

import { renderHook } from "@testing-library/react-hooks";

import { screen, wait, waitFor, cleanup } from "@testing-library/react";

import Filters from "../Filters";

import { TextInput } from "../../Inputs";
import StoreProvider from "../../StoreProvider";
import { useFiltersStore } from "../../../hooks/useFiltersStore";

import { customRender } from "../../../utils/customRender";

import { mockAdminStore } from "../../../mock/adminStore";
import userEvent from "@testing-library/user-event";

import { createMemoryHistory } from "history";

import { BrowserRouter } from "react-router-dom";
import { useListStore } from "../../../hooks/useListStore";

const renderWrapper = () => {
  const wrapper = ({ children }) => (
    <StoreProvider adminStore={mockAdminStore}>{children}</StoreProvider>
  );
  return wrapper;
};

describe("<Filters>", () => {
  let filtersGlobal = null;

  afterEach(() => {
    cleanup();

    if (filtersGlobal) {
      filtersGlobal = [];
    }
  });

  test("should be save filters to store", () => {
    customRender(
      <Filters>
        <TextInput name="title" placeholder="Title" />
        <TextInput name="body" placeholder="Body" />
      </Filters>
    );

    const { result } = renderHook(() => useFiltersStore(), {
      wrapper: renderWrapper(),
    });

    const filters = result.current._filters;
    filtersGlobal = filters;
    const titleFilter = filters.find((filter) => filter.source === "title");
    const bodyFilter = filters.find((filter) => filter.source === "body");

    expect(filters).toHaveLength(2);
    expect(titleFilter).toBeDefined();
    expect(bodyFilter).toBeDefined();
  });

  test("should be send request with filters ", () => {
    const history = createMemoryHistory();
    const { container } = customRender(
      <BrowserRouter history={history}>
        <Filters>
          <TextInput name="title" placeholder="Title" />
          <TextInput name="body" placeholder="Body" />
        </Filters>
      </BrowserRouter>
    );

    const { result: filterStore } = renderHook(() => useFiltersStore(), {
      wrapper: renderWrapper(),
    });

    const { result: listStore } = renderHook(() => useListStore(), {
      wrapper: renderWrapper(),
    });

    filtersGlobal = filterStore.current._filters;

    const button = container.querySelector("[type='button']");
    waitFor(() => {
      userEvent.click(button);
    });

    const menuItems = screen.getAllByRole("menuitem");

    waitFor(() => {
      userEvent.click(menuItems[0]);
    });

    const submit = container.querySelector('[type="submit"]');
    const inputTitle = Array.from(screen.getAllByPlaceholderText("Title"));

    console.log(submit);
    waitFor(() => {
      userEvent.type(inputTitle[0], "hello");
    });
  });
});
