"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_hooks_1 = require("@testing-library/react-hooks");
var react_2 = require("@testing-library/react");
var Filters_1 = __importDefault(require("../Filters"));
var Inputs_1 = require("../../Inputs");
var StoreProvider_1 = __importDefault(require("../../StoreProvider"));
var useFiltersStore_1 = require("../../../hooks/useFiltersStore");
var customRender_1 = require("../../../utils/customRender");
var adminStore_1 = require("../../../mock/adminStore");
var user_event_1 = __importDefault(require("@testing-library/user-event"));
var history_1 = require("history");
var react_router_dom_1 = require("react-router-dom");
var useListStore_1 = require("../../../hooks/useListStore");
var renderWrapper = function () {
    var wrapper = function (_a) {
        var children = _a.children;
        return (react_1.default.createElement(StoreProvider_1.default, { adminStore: adminStore_1.mockAdminStore }, children));
    };
    return wrapper;
};
describe("<Filters>", function () {
    var filtersGlobal = null;
    afterEach(function () {
        react_2.cleanup();
        if (filtersGlobal) {
            filtersGlobal = [];
        }
    });
    test("should be save filters to store", function () {
        customRender_1.customRender(react_1.default.createElement(Filters_1.default, null,
            react_1.default.createElement(Inputs_1.TextInput, { name: "title", placeholder: "Title" }),
            react_1.default.createElement(Inputs_1.TextInput, { name: "body", placeholder: "Body" })));
        var result = react_hooks_1.renderHook(function () { return useFiltersStore_1.useFiltersStore(); }, {
            wrapper: renderWrapper(),
        }).result;
        var filters = result.current._filters;
        filtersGlobal = filters;
        var titleFilter = filters.find(function (filter) { return filter.source === "title"; });
        var bodyFilter = filters.find(function (filter) { return filter.source === "body"; });
        expect(filters).toHaveLength(2);
        expect(titleFilter).toBeDefined();
        expect(bodyFilter).toBeDefined();
    });
    test("should be send request with filters ", function () {
        var history = history_1.createMemoryHistory();
        var container = customRender_1.customRender(react_1.default.createElement(react_router_dom_1.BrowserRouter, { history: history },
            react_1.default.createElement(Filters_1.default, null,
                react_1.default.createElement(Inputs_1.TextInput, { name: "title", placeholder: "Title" }),
                react_1.default.createElement(Inputs_1.TextInput, { name: "body", placeholder: "Body" })))).container;
        var filterStore = react_hooks_1.renderHook(function () { return useFiltersStore_1.useFiltersStore(); }, {
            wrapper: renderWrapper(),
        }).result;
        var listStore = react_hooks_1.renderHook(function () { return useListStore_1.useListStore(); }, {
            wrapper: renderWrapper(),
        }).result;
        filtersGlobal = filterStore.current._filters;
        var button = container.querySelector("[type='button']");
        react_2.waitFor(function () {
            user_event_1.default.click(button);
        });
        var menuItems = react_2.screen.getAllByRole("menuitem");
        react_2.waitFor(function () {
            user_event_1.default.click(menuItems[0]);
        });
        var submit = container.querySelector('[type="submit"]');
        var inputTitle = Array.from(react_2.screen.getAllByPlaceholderText("Title"));
        console.log(submit);
        react_2.waitFor(function () {
            user_event_1.default.type(inputTitle[0], "hello");
        });
    });
});
