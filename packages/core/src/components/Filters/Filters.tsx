import React, { useEffect, useState } from "react";

import Space from "antd/lib/space";
import Button from "antd/lib/button";
import Dropdown from "antd/lib/dropdown";

import PlusOutlined from "@ant-design/icons/lib/icons/PlusOutlined";

/**
 * @hook_stores - hooks for getting stores
 */
import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useFiltersStore } from "../../hooks/useFiltersStore";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useListStore } from "../../hooks/useListStore";

/**
 * @stores_types
 */
import type { Filter, FiltersStore } from "../../stores/FiltersStore";

import { Form } from "../Form";
import DisplayFilter from "./DisplayFilter";
import FiltersMenu from "./FiltersMenu";

import { observer } from "mobx-react";

type FiltersProps = {
  always_visible?: boolean;
  defaultFilters?: string[];
};

/**
 * @function getDefaultFilters
 * @params
 *  filterStore - type FilterStore
 *  defaultFilters - Array of string
 *
 * @description
 *  takes filters from filterStore and returns only those that are in the default filters
 * */

const getDefaultFilters = (
  filtersStore: FiltersStore,
  defaultFilters: string[]
) => {
  return defaultFilters && defaultFilters.length
    ? defaultFilters.map((defaultFilter) => {
        return filtersStore.getFilter(defaultFilter).DisplayFilterComponent;
      })
    : [];
};

/**
 * @component Filters
 * @props
 *  children - React.Nodes
 *  always_visible - boolean (if true - necessary default filters)
 *  defaultFilters - Array of string
 * 
 * @example
 *  <Filters>
 *    <TextInput name="title" placeholder="Title" />
      <TextInput name="body" placeholder="Body" />
 *  </Filters>
 * */

const Filters: React.FC<FiltersProps> = observer(
  ({ children, always_visible, defaultFilters }) => {
    const dataProviderStore = useDataProviderStore();
    const listStore = useListStore();
    const resourceStore = useResourceStore();
    const filtersStore = useFiltersStore();

    const [filters, setFilters] = useState<Filter[]>([]);
    const [displayFilters, setDisplayFilters] = useState([]);

    const handleSubmit = async (values) => {
      listStore.getData(
        dataProviderStore.dataProvider,
        resourceStore.currentResource,
        { filters: values }
      );

      const valuesKeys = Object.keys(values);

      valuesKeys.map((valueKey) =>
        filtersStore.setFilterValue(valueKey, values[valueKey])
      );
    };

    const _filters: Filter[] = React.Children.map(children, (child: any) => ({
      source: child.props.name,
      value: "",
      label: child.props.label || child.props.name,
      DisplayFilterComponent: child,
    }));

    const mapfiltersToInitialValue = () => {
      let initialValues = {};
      filtersStore.filters.forEach((filter) => {
        initialValues = { ...initialValues, ...{ [filter.source]: "" } };
      });
      return initialValues;
    };

    useEffect(() => {
      filtersStore.filters = _filters;
      if (always_visible) {
        setDisplayFilters(getDefaultFilters(filtersStore, defaultFilters));
      }
    }, []);

    useEffect(() => {
      if (always_visible) {
        setFilters(
          filtersStore.filters.filter(
            (filter) => !defaultFilters.includes(filter.source)
          )
        );
      } else {
        setFilters(filtersStore.filters);
      }
    }, [filtersStore.filters]);

    useEffect(() => {
      filtersStore.displayFilters = displayFilters;
    }, [displayFilters]);

    return (
      <Space>
        <Form
          handleSubmit={handleSubmit}
          initialValue={mapfiltersToInitialValue()}
        >
          {displayFilters && displayFilters.length ? (
            <>
              <Space>
                {displayFilters.map((displayFilter, index) => (
                  <DisplayFilter
                    key={`displayFilter-${index}`}
                    displayFilter={displayFilter}
                    setDisplayFilters={setDisplayFilters}
                    setFilters={setFilters}
                    filtersStore={filtersStore}
                  />
                ))}
              </Space>
              <Button
                style={{ marginLeft: 10 }}
                type="primary"
                htmlType="submit"
              >
                Filter
              </Button>
            </>
          ) : null}
        </Form>

        <Dropdown
          trigger={["click"]}
          overlay={
            <FiltersMenu
              setDisplayFilters={setDisplayFilters}
              setFilters={setFilters}
              filters={filters}
            />
          }
        >
          <Button>
            Add Filter <PlusOutlined />
          </Button>
        </Dropdown>
      </Space>
    );
  }
);

export default Filters;
