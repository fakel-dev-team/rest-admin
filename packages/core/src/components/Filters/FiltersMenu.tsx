import React from "react";
import { Filter } from "../../stores";

import Menu from "antd/lib/menu";

import { observer } from "mobx-react";

/**
 * @component FiltersMenu
 * @props
 *  setDisplayFilters - (displayFilters) => void
 *  setFilters - (filters) => void
 *  filters - Array of Filter
 * 
 * @example
 *  <FiltersMenu
      setDisplayFilters={setDisplayFilters}
      setFilters={setFilters}
      filters={filters}
    />
 * */

type FiltersMenuProps = {
  setDisplayFilters: (displayFilters: any) => any;
  setFilters: (filters: any) => any;
  filters: Filter[];
};

const FiltersMenu: React.FC<FiltersMenuProps> = observer(
  ({ setDisplayFilters, setFilters, filters }) => {
    const handleMenuClick = (filter: Filter) => {
      const displayFilter = filter.DisplayFilterComponent;

      setDisplayFilters((displayFilters) => {
        const isExist = displayFilters.find((dF) => dF === displayFilter);
        if (!isExist) {
          return [...displayFilters, displayFilter];
        }
        return displayFilters;
      });
      setFilters((filters) => filters.filter((_filter) => _filter !== filter));
    };
    return (
      <Menu>
        {filters.map((filter, index) => (
          <Menu.Item onClick={() => handleMenuClick(filter)} key={index}>
            {filter.label}
          </Menu.Item>
        ))}
      </Menu>
    );
  }
);

export default FiltersMenu;
