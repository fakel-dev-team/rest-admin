import React from "react";

import Col from "antd/lib/col";
import Button from "antd/lib/button";

import CloseOutlined from "@ant-design/icons/lib/icons/CloseOutlined";
import { FiltersStore } from "../../stores";

/**
 * @component DisplayFilter
 * @props
 *  displayFilter - React.Node
 *  setDisplayFilter - @function (displayFilter) => void
 *  setFilters - @function (filters) => void
 *
 * @example
 *  <Filters>
 *    const [displayFilters, setDisplayFilters] = useState([])
 *    const [filters, setFilters] = useState([])
 *
 *    return (
 *      displayFilters.map(displayFilter =>
 *        <DisplayFilter
 *          displayFilter={displayFilter}
 *          filters={filters}
 *          setFilters={setFilters}
 *          setDisplayFilters={setDisplayFilters}
 *        />)
 *    )
 *  </Filters>
 *
 * */

type DisplayFiltersProps = {
  displayFilter: any;
  setDisplayFilters: (displayFilters: any) => any;
  setFilters: (filters: any) => any;
  filtersStore: FiltersStore;
};
const DisplayFilter: React.FC<DisplayFiltersProps> = ({
  displayFilter,
  setDisplayFilters,
  setFilters,
  filtersStore,
}) => {
  const handleDeleteButton = (displayFilter, source: string) => {
    setDisplayFilters((displayFilters) =>
      displayFilters.filter(
        (_displayFilter) => _displayFilter !== displayFilter
      )
    );

    setFilters((filters) => {
      const isExist = filters.find((filter) => filter.source === source);

      if (!isExist) {
        return [...filters, filtersStore.getFilter(source)];
      }

      return filters;
    });
  };

  return (
    <Col
      span="8"
      style={{
        display: "flex",
        alignItems: "center",
      }}
    >
      <Button
        onClick={() =>
          handleDeleteButton(displayFilter, displayFilter.props.name)
        }
        size="small"
        type="ghost"
        shape="circle"
        style={{ marginRight: 10 }}
        icon={<CloseOutlined style={{ fontSize: 12 }} />}
      />
      {displayFilter}
    </Col>
  );
};

export default DisplayFilter;
