import React from "react";

import CreateView from "./CreateView";
import { RedirectTo } from "../../@types";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useNotification } from "../../hooks/useNotication";

import { useHistory } from "react-router-dom";

import { FormikHelpers } from "formik";

/**
 * @component Create
 * @props
 *  title? - string (Title for create form)
 *  children - (props: any) => any
 *  redirect? - "list" | "show" | "create" | "edit"
 * 
 * @example
 * <Create redirect="list">
      {(createProps) => {
        return (
          <SimpleForm
            {...createProps}
            initialValue={{
              username: "",
              email: "",
            }}
          >
           <TextInput label="Username" name="username" />
            <TextInput
              label="Email"
              name="email"
              placeholder="Enter email"
            />
          </SimpleForm>
        );
      }}
    </Create>
 * */

type CreateProps = {
  title?: string;
  children: (props?: any) => any;
  redirect?: RedirectTo;
  saveRedirect?: (...any) => void;
  onSuccessfulSubmit?: () => void;
  onFailedSubmit?: (error?: any) => void;
  handleSubmit?: (value: any, actions: any) => any;
};

const Create: React.FC<CreateProps> = (props) => {
  const { title, children, redirect } = props;

  const dataProviderStore = useDataProviderStore();
  const resourceStore = useResourceStore();
  const history = useHistory();

  const successfulNotification = useNotification({
    message: "Успешно отправлено",
  });
  const failedNotification = useNotification({
    type: "error",
    message: "Ошибка",
  });

  const handleSubmit = async (values: any, actions: FormikHelpers<any>) => {
    try {
      const response = await dataProviderStore.dataProvider.create(
        resourceStore.currentResource,
        values
      );

      props.onSuccessfulSubmit
        ? props.onSuccessfulSubmit()
        : successfulNotification();
      history.push(`/${resourceStore.currentResource}/${props.redirect}`);
    } catch (error) {
      console.log(error);
      props.onFailedSubmit ? props.onFailedSubmit() : failedNotification();
    }
  };

  return (
    <CreateView title={title}>
      {children({ handleSubmit: props.handleSubmit || handleSubmit })}
    </CreateView>
  );
};

export default Create;
