import React from "react";

import Space from "antd/lib/space";
import Typography from "antd/lib/typography";
import Row from "antd/lib/row";

const { Title } = Typography;

type CreateViewProps = {
  actions?: JSX.Element;
  title?: string;
};

const CreateView: React.FC<CreateViewProps> = ({
  actions,
  title,
  children,
}) => {
  const defaultTitle = "Create form";

  return (
    <Space direction="vertical">
      <Row justify="space-between" align="middle">
        <Title level={4}>{title || defaultTitle}</Title>
        {actions}
      </Row>
      {children}
    </Space>
  );
};

export default CreateView;
