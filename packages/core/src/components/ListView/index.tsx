import React from "react";

import { useHistory } from "react-router-dom";

import { Table } from "antd";
import { ColumnType, ColumnProps } from "antd/lib/table";
import { ResourceT } from "../../stores/ResorceStore";

type ListViewProps<DataSourceT> = {
  dataSource: any;
  columns: any;
  resource: ResourceT;
};

function ListView<DataSourceT>(props: ListViewProps<DataSourceT>) {
  const { dataSource, columns, resource } = props;
  const history = useHistory();

  const onRow = (record, rowIndex) => {
    return {
      onClick: (event) => {
        history.push(`/${resource.name}/edit/${record.id}`);
      },
    };
  };

  return (
    <Table
      onRow={onRow}
      rowSelection={{ type: "checkbox" }}
      dataSource={dataSource}
      columns={columns}
    />
  );
}

export default ListView;
