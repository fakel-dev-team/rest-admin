import React, { useEffect, useState } from "react";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useShowStore } from "../../hooks/useShowStore";

import { useParams } from "react-router-dom";
import { observer } from "mobx-react";

import LoadingOutlined from "@ant-design/icons/lib/icons/LoadingOutlined";
import Row from "antd/lib/row";
import Typography from "antd/lib/typography";
import Space from "antd/lib/space";
import Spin from "antd/lib/spin";

import ShowActions from "./ShowActions";

const { Title } = Typography;

type ShowProps = {
  title?: string | ((record: any) => JSX.Element);
  actions?: JSX.Element;
};

const Show: React.FC<ShowProps> = observer(({ title, actions, children }) => {
  const dataProviderStore = useDataProviderStore();
  const resourceStore = useResourceStore();
  const showStore = useShowStore();

  const [loading, setLoading] = useState(false);

  const { id } = useParams<any>();

  useEffect(() => {
    setLoading(true);
    if (resourceStore.currentResource) {
      showStore.getData(
        dataProviderStore.dataProvider,
        resourceStore.currentResource,
        { id }
      );
      setLoading(false);
    }
  }, [resourceStore.currentResource]);

  return loading ? (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Spin indicator={<LoadingOutlined style={{ fontSize: 24 }} />} />
    </div>
  ) : (
    showStore.data && (
      <>
        <Row justify="space-between">
          {typeof title === "function" ? (
            title(showStore.data)
          ) : (
            <Title level={3}>{title}</Title>
          )}
          <ShowActions id={id}>{actions}</ShowActions>
        </Row>
        <Space direction="vertical">
          {React.Children.map(children, (child) => {
            return React.cloneElement(child as any, {
              records: [showStore.data],
            });
          })}
        </Space>
      </>
    )
  );
});

export default Show;
