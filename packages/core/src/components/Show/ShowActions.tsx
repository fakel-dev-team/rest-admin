import React from "react";

import Space from "antd/lib/space";
import Button from "antd/lib/button";

import { useResourceStore } from "../../hooks/useResourceStore";
import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useHistory } from "react-router-dom";

type ShowActionsProps = {
  id: string;
};

const ShowActions: React.FC<ShowActionsProps> = ({ id, children }) => {
  const dataProviderStore = useDataProviderStore();
  const resourceStore = useResourceStore();
  const history = useHistory();

  const handleDelete = async () => {
    const { data } = await dataProviderStore.dataProvider.delete(
      resourceStore.currentResource,
      {
        id,
      }
    );
  };

  const handleEdit = async () => {
    history.push(`/${resourceStore.currentResource}/edit/${id}`);
  };

  return (
    <Space>
      {children || (
        <>
          <Button onClick={handleEdit} type="primary">
            Edit
          </Button>
          <Button onClick={handleDelete} type="primary" danger>
            Delete
          </Button>
        </>
      )}
    </Space>
  );
};

export default ShowActions;
