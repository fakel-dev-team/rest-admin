import React from "react";

import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Button from "antd/lib/button";
import message from "antd/lib/message";

import CloseOutlined from "@ant-design/icons/lib/icons/CloseOutlined";
import DeleteOutlined from "@ant-design/icons/lib/icons/DeleteOutlined";

import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useListStore } from "../../hooks/useListStore";
import { useResourceStore } from "../../hooks/useResourceStore";

type ListSelectionProps = {
  selectedRows: number[];
  setSelectedRows: (...any) => any;
  actions?: React.ReactNode;
  dataSource: any;
};

const ListSelection: React.FC<ListSelectionProps> = ({
  selectedRows,
  actions,
  setSelectedRows,
  dataSource,
}) => {
  const dataProviderStore = useDataProviderStore();
  const resourceStore = useResourceStore();
  const listStore = useListStore();

  const onClose = () => {
    setSelectedRows([]);
  };

  const onDelete = async () => {
    const ids = selectedRows.map((id) => dataSource[id].id);
    console.log(dataSource);
    listStore.deleteRecords(
      dataProviderStore.dataProvider,
      resourceStore.currentResource,
      ids
    );

    message.success("Упешно удалено!");

    setSelectedRows([]);
  };

  return selectedRows ? (
    <Row
      align="middle"
      justify="space-between"
      style={{
        backgroundColor: "#fff",
        width: "100%",
        padding: 10,
      }}
    >
      <Col>
        <Button onClick={onClose} icon={<CloseOutlined />} />
      </Col>

      <Col>Selected {selectedRows.length} items</Col>

      <Col>
        {actions || (
          <Button onClick={onDelete} type="primary" danger>
            Delete <DeleteOutlined />
          </Button>
        )}
      </Col>
    </Row>
  ) : null;
};

export default ListSelection;
