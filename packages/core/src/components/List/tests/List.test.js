"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var react_1 = __importDefault(require("react"));
var react_2 = require("@testing-library/react");
var Fields_1 = require("../../Fields");
var index_1 = require("../index");
var customRender_1 = require("../../../utils/customRender");
var mockColumns = [
    {
        title: "ID",
        source: "id",
        Field: Fields_1.TextField,
    },
];
describe("<List>", function () {
    afterAll(function () {
        react_2.cleanup();
        jest.restoreAllMocks();
    });
    test("should be render table with data", function () {
        var container = customRender_1.customRender(react_1.default.createElement(index_1.List, { columns: mockColumns })).container;
        var table = container.querySelector("table");
        var tbody = table.querySelector("tbody");
        expect(table).toBeInTheDocument();
        expect(tbody).toBeInTheDocument();
    });
    test("should be render table with data, filtes, actions", function () {
        var Filters = function () { return react_1.default.createElement("div", { id: "filters" }, "Filters"); };
        var Actions = function () { return react_1.default.createElement("div", { id: "actions" }, "Actions"); };
        var container = customRender_1.customRender(react_1.default.createElement(index_1.List, { columns: mockColumns, filters: react_1.default.createElement(Filters, null), actions: react_1.default.createElement(Actions, null) })).container;
        var table = container.querySelector("table");
        var tbody = table.querySelector("tbody");
        var filters = container.querySelector("#filters");
        var actions = container.querySelector("#actions");
        expect(table).toBeInTheDocument();
        expect(tbody).toBeInTheDocument();
        expect(filters).toBeInTheDocument();
        expect(actions).toBeInTheDocument();
    });
});
