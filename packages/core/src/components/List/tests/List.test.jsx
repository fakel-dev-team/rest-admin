import React from "react";

import { cleanup } from "@testing-library/react";

import { TextField } from "../../Fields";
import { List } from "../index";

import { customRender } from "../../../utils/customRender";

const mockColumns = [
  {
    title: "ID",
    source: "id",
    Field: TextField,
  },
];

describe("<List>", () => {
  afterAll(() => {
    cleanup();
    jest.restoreAllMocks();
  });

  test("should be render table with data", () => {
    const { container } = customRender(<List columns={mockColumns} />);
    const table = container.querySelector("table");
    const tbody = table.querySelector("tbody");

    expect(table).toBeInTheDocument();
    expect(tbody).toBeInTheDocument();
  });

  test("should be render table with data, filtes, actions", () => {
    const Filters = () => <div id="filters">Filters</div>;
    const Actions = () => <div id="actions">Actions</div>;

    const { container } = customRender(
      <List columns={mockColumns} filters={<Filters />} actions={<Actions />} />
    );
    const table = container.querySelector("table");
    const tbody = table.querySelector("tbody");

    const filters = container.querySelector("#filters");
    const actions = container.querySelector("#actions");

    expect(table).toBeInTheDocument();
    expect(tbody).toBeInTheDocument();
    expect(filters).toBeInTheDocument();
    expect(actions).toBeInTheDocument();
  });
});
