import React, { useState } from "react";

import { useHistory } from "react-router-dom";

import Table from "antd/lib/table";

import { ListSelection } from "./index";
import { observer } from "mobx-react";
import { ListStore } from "../../stores";

import { useDataProviderStore } from "../../hooks";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useListStore } from "../../hooks/useListStore";

type ListViewProps = {
  dataSource: any;
  ids: number[];
  columns: any;
  resource: string;
  perPage?: number;
  page?: number;
  listStore: ListStore;
  loading?: boolean;
  view?: "edit" | "show";
};

const ListView: React.FC<ListViewProps> = observer((props) => {
  const { dataSource, columns, resource, perPage, page, loading, view } = props;

  const dataProviderStore = useDataProviderStore();
  const listStore = useListStore();
  const resourceStore = useResourceStore();

  const history = useHistory();

  const [selectedRows, setSelectedRows] = useState([]);

  const onSelect = (selectedRowsIds) => {
    setSelectedRows([...selectedRowsIds]);
  };

  const onRow = (record) => {
    if (!record.id) {
      console.warn("WARNING: Record must have an id parameter");
    }
    return {
      onClick: () => {
        history.push(`/${resource}/${view ? view : "edit"}/${record.id}`);
      },
    };
  };

  const onPageChange = (page: number, pageSize: number) => {
    listStore.getData(
      dataProviderStore.dataProvider,
      resourceStore.currentResource,
      { page, pageSize }
    );
  };

  return (
    <>
      {selectedRows && selectedRows.length ? (
        <ListSelection
          dataSource={dataSource}
          setSelectedRows={setSelectedRows}
          selectedRows={selectedRows}
        />
      ) : null}
      <Table
        loading={loading}
        style={{ minWidth: "80vw" }}
        size="middle"
        onRow={onRow}
        rowSelection={{
          type: "checkbox",
          selectedRowKeys: selectedRows,
          onChange: onSelect,
        }}
        dataSource={dataSource}
        columns={columns}
        pagination={{
          showSizeChanger: true,
          total: listStore.total,
          defaultPageSize: perPage,
          defaultCurrent: page,
          onChange: onPageChange,
        }}
      />
    </>
  );
});

export default ListView;
