import React from "react";
import Space from "antd/lib/space";

import { CreateButton } from "../Buttons";
import { useLogin } from "../../hooks/useLogin";
import { Button } from "antd";

const DefaulListAction: React.FC = (props) => {
  const { logout } = useLogin();

  return (
    <Space>
      <CreateButton />
    </Space>
  );
};

export default DefaulListAction;
