import React, { useEffect } from "react";

import { ListView, DefaultListActions } from "./index";

import { observer } from "mobx-react";
import { toJS } from "mobx";

import Space from "antd/lib/space";
import Col from "antd/lib/col";
import Row from "antd/lib/row";

import { useListStore } from "../../hooks/useListStore";
import { useResourceStore } from "../../hooks/useResourceStore";
import { useDataProviderStore } from "../../hooks/useDataProviderStore";
import { useFiltersStore } from "../../hooks/useFiltersStore";

import { ColumnT } from "../../@types";

import { mapToAntColumn } from "../../utils/List";
import { useHistory } from "react-router-dom";

type ListProps = {
  columns: ColumnT[];
  title?: string;
  actions?: JSX.Element;
  filters?: JSX.Element;
  perPage?: number;
  view?: "edit" | "show";
};

const List: React.FC<ListProps> = observer(
  ({ columns, filters, actions, perPage, view }) => {
    const dataProviderStore = useDataProviderStore();
    const resourceStore = useResourceStore();
    const listStore = useListStore();
    const filtersStore = useFiltersStore();

    const tableColumns = mapToAntColumn(columns);
    useEffect(() => {
      listStore.getData(
        dataProviderStore.dataProvider,
        resourceStore.currentResource
      );
    }, [resourceStore.currentResource, filtersStore.displayFilters]);

    return (
      <Space direction="vertical">
        <Row justify="space-between" align="top">
          <Col span="4">{actions || <DefaultListActions />} </Col>
          <Col
            style={{ display: "flex", justifyContent: "flex-end" }}
            span="20"
          >
            {filters}
          </Col>
        </Row>
        <ListView
          loading={toJS(listStore.loading)}
          view={view}
          listStore={listStore}
          dataSource={toJS(listStore.dataSource)}
          ids={listStore.ids}
          perPage={perPage || 20}
          resource={resourceStore.currentResource}
          columns={tableColumns}
        />
      </Space>
    );
  }
);

export default List;
