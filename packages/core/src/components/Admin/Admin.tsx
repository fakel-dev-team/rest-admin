import React from 'react';
import { DataProviderT } from '@fakel-dev-team/rest-admin-simple-rest';
import { AuthProvider } from '@fakel-dev-team/rest-admin-simple-auth';

import { HashRouter as Router } from 'react-router-dom';

import { AdminStoreProvider, AdminRouter } from './index';
import AdminLayout from './AdminLayout';

/**
 * @component Admin
 * @props
 *  dataProvider - DataProviderT
 *
 * @example
 *  <Admin dataProvider={dataProvider}>
      <Resource
        name="posts"
        edit={PostsEdit}
        create={PostsCreate}
        list={PostView}
        show={PostShow}
        options={{ label: "Posts", icon: <BookOutlined /> }}
      />
      ...
    </Admin>
 * */

type AdminPropsT = {
  dataProvider: DataProviderT;
  authProvider?: AuthProvider;
  logoTitle?: string;
  userView?: React.ReactNode;
  defaultResource?: string;
};

const Admin: React.FC<AdminPropsT> = ({
  dataProvider,
  children,
  authProvider,
  userView,
  logoTitle,
  defaultResource,
}) => {
  return (
    <Router hashType="slash">
      <AdminStoreProvider
        authProvider={authProvider}
        dataProvider={dataProvider}
      >
        {children}
        <AdminRouter />
      </AdminStoreProvider>
    </Router>
  );
};

export default Admin;
