import { observer } from "mobx-react";
import React, { useEffect, useState } from "react";

import { Route, Switch, useLocation } from "react-router-dom";
import { useResourceStore } from "../../hooks";

import { LoginPage } from "../../pages";
import { ResourceT } from "../../stores";
import { ResourceProtectedRoute } from "../Resource";
import AdminLayout from "./AdminLayout";
import { useCheckAuth } from "../../hooks/useCheckAuth";

type DefaultViewFunction = (resources: ResourceT[]) => React.ReactNode;
type DefaultView = React.ComponentType | DefaultViewFunction;

type AdminRouterProps = {
  customRoutes?: any;
  defaultView?: DefaultView;
};

const createLoginRoute = () => {
  return <Route exact path="/login" component={LoginPage} />;
};

const createRootRoute = (defaultView, resources: ResourceT[]) => {
  return (
    <Route
      exact
      path="/"
      render={() => {
        if (defaultView) {
          if (typeof defaultView === "function") {
            return defaultView(resources);
          } else {
            return React.createElement(defaultView);
          }
        } else {
          return <AdminLayout />;
        }
      }}
    />
  );
};

const createResourceRoutes = (resource: ResourceT) => {
  return (
    <AdminLayout>
      <Switch>
        <ResourceProtectedRoute
          key={`/${resource.name}/list`}
          exact
          path={`/${resource.name}/list`}
          component={resource.list}
        />
        <ResourceProtectedRoute
          exact
          key={`/${resource.name}/create`}
          path={`/${resource.name}/create`}
          component={resource.create}
        />
        <ResourceProtectedRoute
          exact
          key={`/${resource.name}/edit`}
          path={`/${resource.name}/edit/:id`}
          component={resource.edit}
        />
        <ResourceProtectedRoute
          exact
          key={`/${resource.name}/show`}
          path={`/${resource.name}/show/:id`}
          component={resource.show}
        />
      </Switch>
    </AdminLayout>
  );
};

const AdminRouter: React.FC<AdminRouterProps> = observer(
  ({ customRoutes, defaultView }) => {
    useCheckAuth();
    const resourceStore = useResourceStore();

    const [currentResource, setCurrentResource] = useState<ResourceT>(null);
    const location = useLocation();

    useEffect(() => {
      resourceStore.setCurrentResource(location.pathname.split("/", 2)[1]);
    }, [location.pathname]);

    useEffect(() => {
      if (resourceStore.resources && resourceStore.resources.length)
        setCurrentResource(
          resourceStore.getCurrentResource() || resourceStore.resources[0]
        );
    }, [resourceStore.currentResource]);

    return (
      <Switch key="main-switch">
        {createLoginRoute()}
        {createRootRoute(defaultView, resourceStore.resources)}
        {currentResource ? createResourceRoutes(currentResource) : null}
        {customRoutes}
      </Switch>
    );
  }
);

export default AdminRouter;
