import React from "react";

import Layout from "antd/lib/layout";
import Typography from "antd/lib/typography";
import Row from "antd/lib/row";
import Col from "antd/lib/col";
import Space from "antd/lib/space";

import { Menu } from "../index";

import { ErrorBoundary } from "react-error-boundary";
import ErrorFallback from "../ErrorFallback/ErrorFallback";
import { observer } from "mobx-react";
import { useGetMe } from "../../hooks/useGetMe";
import { User } from "../User/User";
import { LogoutButton } from "../Buttons";
import { useAuthStore } from "../../hooks/useAuthStore";
import { useLogin } from "../../hooks/useLogin";

const { Header, Content, Sider } = Layout;
const { Title, Link: AntLink } = Typography;

export type AdminLayoutProps = {
  logoTitle?: string;
  userView?: React.ReactNode;
};

const AdminLayout: React.FC<AdminLayoutProps> = observer(
  ({ children, logoTitle, userView }) => {
    // const { user } = useGetMe();
    // console.log(user);
    const authStore = useAuthStore();

    return (
      <Layout style={{ minHeight: "100vh" }}>
        <Header>
          <Row justify="space-between" align="middle">
            <Col span={6}>
              <AntLink href="/">
                <Title
                  className="header__logo"
                  level={3}
                  style={{ color: "#fff", marginBottom: 0 }}
                >
                  {logoTitle || "Admin"}
                </Title>
              </AntLink>
            </Col>
            {/* {user && (
              <Col style={{ textAlign: "right" }} span={8}>
                <Space>
                  {userView || <User />}
                  <LogoutButton />
                </Space>
              </Col>
            )} */}
          </Row>
        </Header>
        <Layout>
          <Sider width={200} className="site-layout-background">
            <Menu />
          </Sider>
          <Layout style={{ padding: "20px" }}>
            <ErrorBoundary
              fallbackRender={({ error, resetErrorBoundary }) => (
                <ErrorFallback
                  message={error.message}
                  resetErrorBoundary={resetErrorBoundary}
                />
              )}
            >
              <Content className="site-layout-background">{children}</Content>
            </ErrorBoundary>
          </Layout>
        </Layout>
      </Layout>
    );
  }
);

export default AdminLayout;
