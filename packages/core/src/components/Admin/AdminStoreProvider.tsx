import React from "react";
import { DataProviderT } from "@fakel-dev-team/rest-admin-simple-rest";
import { AdminStore } from "../../stores/AdminStore";

import StoreProvider from "../StoreProvider";
import { AuthProvider } from "@fakel-dev-team/rest-admin-simple-auth";

type AdminStoreProviderProps = {
  dataProvider: DataProviderT;
  authProvider?: AuthProvider;
};

const AdminStoreProvider: React.FC<AdminStoreProviderProps> = ({
  dataProvider,
  authProvider,
  children,
}) => {
  const adminStore = new AdminStore(dataProvider, authProvider);
  return <StoreProvider adminStore={adminStore}>{children}</StoreProvider>;
};

export default AdminStoreProvider;
