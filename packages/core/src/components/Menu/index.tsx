import React from "react";

import Menu from "antd/lib/menu";

import { observer } from "mobx-react";

import { Link } from "react-router-dom";

import { useResourceStore } from "../../hooks/useResourceStore";
import { ResourceT } from "../../stores";

const isDefinedOptions = (resource: ResourceT) => !!resource.options;

const getRenderView = (resource: ResourceT) => {
  if (isDefinedOptions(resource)) {
    return resource.options.renderViewOnMenuClicked || "list";
  }

  return "list";
};

const getLinkUrl = (resource: ResourceT) => {
  const DEFAULT_URL = `/${resource.name}/${getRenderView(resource)}`;
  if (isDefinedOptions(resource)) {
    return resource.options.linkUrl || DEFAULT_URL;
  }

  return DEFAULT_URL;
};

const getLabel = (resource: ResourceT) => {
  if (isDefinedOptions(resource)) {
    return resource.options.label || resource.name;
  }

  return resource.name;
};

const AppMenu: React.FC = observer(() => {
  const resourceStore = useResourceStore();
  const resources = resourceStore.resources;

  return resources && resources.length ? (
    <Menu mode="inline" style={{ height: "100%", borderRight: 0 }}>
      {resources.map((resource, index) => {
        return (
          <Menu.Item
            icon={resource.options ? resource.options.icon : null}
            key={index}
          >
            <Link to={getLinkUrl(resource)}>{getLabel(resource)}</Link>
          </Menu.Item>
        );
      })}
    </Menu>
  ) : null;
});

export default AppMenu;
