import React, { createContext } from "react";
import { AdminStore } from "../../stores/AdminStore";

type StoreProviderProps = {
  adminStore: AdminStore;
};

export const AdminContext = createContext<StoreProviderProps>({
  adminStore: null,
});

const StoreProvider: React.FC<StoreProviderProps> = ({
  adminStore,
  children,
}) => {
  return (
    <AdminContext.Provider value={{ adminStore }}>
      {children}
    </AdminContext.Provider>
  );
};
export default StoreProvider;
