import React, { useEffect, useState } from "react";
import { ResourceStore, ResourceT } from "../../stores/ResorceStore";

import { Switch, Route } from "react-router-dom";
import { observer } from "mobx-react";

type ResourceRouterProps = {
  resourceStore: ResourceStore;
  resourceName: string;
};

const ResourceRouter: React.FC<ResourceRouterProps> = observer(
  ({ resourceStore, resourceName }) => {
    const [resource, setResources] = useState<ResourceT>(null);

    useEffect(() => {
      setResources(resourceStore.getResource(resourceName));
    }, [resource]);

    return resource ? (
      <Switch key={resource.name}>
        <Route
          exact
          path={`/${resource.name}/create`}
          render={(props) => {
            return <resource.create name={resource.name} {...props} />;
          }}
        />
        <Route
          exact
          path={`/${resource.name}/edit/:id`}
          render={(props) => {
            return <resource.edit name={resource.name} {...props} />;
          }}
        />
        <Route
          exact
          path={`/${resource.name}/list`}
          render={(props) => {
            return <resource.list name={resource.name} {...props} />;
          }}
        />
        <Route
          exact
          path={`/${resource.name}/show`}
          render={(props) => {
            return <resource.show name={resource.name} {...props} />;
          }}
        />
      </Switch>
    ) : null;
  }
);

export default ResourceRouter;
