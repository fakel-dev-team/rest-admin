import React from "react";

import Result from "antd/lib/result";
import Button from "antd/lib/button";

type ErrorFallbackProps = {
  message?: string;
  description?: string;
  resetErrorBoundary?: any;
};

const ErrorFallback: React.FC<ErrorFallbackProps> = ({
  message,
  description,
  resetErrorBoundary,
}) => {
  return (
    <div
      style={{
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
      }}
    >
      <Result
        status="error"
        title="Something went wrong:"
        subTitle={message}
        extra={[
          <Button key="reset" onClick={resetErrorBoundary}>
            Reset
          </Button>,
        ]}
      />
    </div>
  );
};

export default ErrorFallback;
