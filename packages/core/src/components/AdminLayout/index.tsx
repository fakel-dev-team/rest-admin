import React from "react";

import { Layout, Typography } from "antd";

import { Menu } from "../index";

import "./style.scss";

const { Header, Content, Sider } = Layout;
const { Title } = Typography;

type AdminLayoutProps = {};

const AdminLayout: React.FC<AdminLayoutProps> = ({ children }) => {
  return (
    <Layout style={{ minHeight: "100vh" }}>
      <Header className="header">
        <Title
          className="header__logo"
          level={3}
          style={{ color: "#fff", marginBottom: 0 }}
        >
          Admin
        </Title>
      </Header>
      <Layout>
        <Sider width={200} className="site-layout-background">
          <Menu />
        </Sider>
        <Layout style={{ padding: "20px" }}>
          <Content className="site-layout-background">{children}</Content>
        </Layout>
      </Layout>
    </Layout>
  );
};

export default AdminLayout;
