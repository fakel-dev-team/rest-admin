import axios from "axios";
import { DataProviderT, DataProviderResponse } from "./types";

type RestCreateParamsT = {
  apiBaseUrl: string;
};

const mapToDataWithId = (data) => {
  const dataWithId = data.map((el) => ({ ...el, id: el._id }));
  return dataWithId;
};

export const createSimpleRest = (params: RestCreateParamsT): DataProviderT => {
  const { apiBaseUrl } = params;

  const APIClient = axios.create({
    baseURL: apiBaseUrl,
  });

  return {
    getMany: async (resource, params = {}) => {
      const { search, searchBy } = params;
      const { data } = await APIClient.get(`/${resource}`);
      return { data: data.data, errors: data.errors };
    },
    getList: async (resource, params = {}) => {
      if (params.filters) {
        const { data } = await APIClient.get(`/${resource}/filters`, {
          params: params.filters,
        });
        console.log(mapToDataWithId(data.data));
        return {
          data: mapToDataWithId(data.data),
          errors: data.errors,
          total: data.total,
        };
      }

      const { data } = await APIClient.get(`/${resource}`, { params });
      return { data: data.data, errors: null, total: data.total };
    },
    getOne: async (resource, params = {}) => {
      const { id } = params;
      const { data } = await APIClient.get(`/${resource}/${id}`);
      return { data: data.data, errors: data.errors };
    },
    update: async (resource, payload, params = {}) => {
      const { id } = params;
      const { data } = await APIClient.put(`/${resource}/${id}`, payload);
      return { data, errors: data.errors };
    },
    create: async (resource, payload, params = {}) => {
      const { data } = await APIClient.post(`/${resource}`, payload);
      return { data, errors: data.errors };
    },
    delete: async (resource, params = {}) => {
      const { id } = params;
      const { data } = await APIClient.delete(`/${resource}/${id}`);
      return { data, errors: data.errors };
    },
  };
};
