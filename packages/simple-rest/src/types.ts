export type DataProviderT = {
  getMany?: (
    resource,
    params?
  ) => DataProviderResponse | Promise<DataProviderResponse>;
  getList: (resource, params?) => GetListResponse | Promise<GetListResponse>;
  getOne: (
    resource,
    params?
  ) => DataProviderResponse | Promise<DataProviderResponse>;
  create: (
    resource,
    payload,
    params?
  ) => DataProviderResponse | Promise<DataProviderResponse>;
  update: (
    resource,
    payload,
    params?
  ) => DataProviderResponse | Promise<DataProviderResponse>;
  delete: (
    resource,
    params?
  ) => DataProviderResponse | Promise<DataProviderResponse>;
};

export type DataProviderResponse<Data = any> = {
  errors: any;
  data: Data;
};

export interface GetListResponse<Data = any>
  extends DataProviderResponse<Data> {
  total: number;
}
