import axios from "axios";

export const createAuthProvider = (options) => {
  axios.defaults.withCredentials = true;

  const APIClient =
    options.client ||
    axios.create({
      baseURL: options.baseURL,
      withCredentials: true,
    });

  return {
    login: async ({ username, password }) => {
      localStorage.setItem("isAuth", "true");
      localStorage.setItem("user", JSON.stringify({ username, password }));
    },
    logout: async () => {
      localStorage.removeItem("user");
      localStorage.removeItem("isAuth");
    },
    checkAuth: async () => {
      return JSON.parse(localStorage.getItem("isAuth"));
    },
    checkError: async (error) => {
      const status = error.status;
      if (status === 401 || status === 403) {
        localStorage.removeItem("token");
        return Promise.reject();
      }
      return Promise.resolve();
    },
    getMe: async () => {
      return JSON.parse(localStorage.getItem("user"));
    },
    getPermissions: async () => {
      const role = localStorage.getItem("permissions");
      return role ? Promise.resolve(role) : Promise.reject();
    },
  };
};
