export type LoginPayload = {
  username: string;
  password: string;
};

export type AuthProvider = {
  login: (payload: LoginPayload) => Promise<void>;
  logout: () => Promise<void>;
  getMe: () => Promise<any>;
  getPermissions: () => Promise<any>;
  checkAuth: () => Promise<boolean>;
  checkError: (error) => Promise<void>;
};

export type createAuthProviderOption = {
  baseURL: string;
  client?: any;
};
