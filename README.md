# Getting Started
`make install` - install all dependency

## Build packages
`make build` - build all packages \
`make build-core` - build package @fakel-dev-team/rest-admin-core \
`make build-simple-rest` - build package @fakel-dev-team/rest-simple-rest
`make build-simple-rest` - build package @fakel-dev-team/rest-simple-rest 

## Watch packages
`make watch` - watch all packages \
`make watch-core` - watch package @fakel-dev-team/rest-admin-core \
`make watch-simple-rest` - watch packages @fakel-dev-team/rest-simple-rest
`make watch-simple-rest` - watch packages @fakel-dev-team/rest-simple-rest 

## Run examples
`make run-simple` - run simple example 


