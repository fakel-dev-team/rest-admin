
install: 
	yarn

run-simple:
	yarn run-simple

build-simple:
	cd examples/simple && yarn build

build-core:
	cd packages/core && yarn build

build-simple-rest:
	cd packages/simple-rest && yarn build

build-simple-auth:
	cd packages/simple-auth && yarn build

watch-core:
	cd packages/core && yarn -s watch

watch-simple-rest:
	cd packages/simple-rest && yarn -s watch

watch-simple-auth:
	cd packages/simple-auth && yarn -s watch

watch: watch-core watch-simple-rest watch-simple-auth
build: build-core build-simple-rest build-simple-auth

publish-core: 
	make build-core && cd packages/core && npm publish

publish-simple-rest: 
	make build-simple-rest && cd packages/simple-rest && npm publish

publish-simple-auth: 
	make build-simple-auth && cd packages/simple-auth && npm publish

publish-all: publish-core publish-simple-rest publish-simple-auth