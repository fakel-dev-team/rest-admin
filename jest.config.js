module.exports = {
  presets: ["@babel/preset-env", "@babel/preset-react"],
  setupFilesAfterEnv: ["./src/setupTests.js"],
};
